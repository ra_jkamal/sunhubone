﻿namespace SunshareCommon
{
    public static class CallStatuses
    {
        public const int DontCall = 1;
        public const int CallBack = 2;
        public const int ClosedWon = 3;
        public const int ClosedLost = 4;
        public const int Incoming = 5;
        public const int NoAnswer = 6;
        public const int LeftVoicemail = 7;
        public const int NotInsterested = 8;
        public const int Ineligible = 10;
    }
}
