﻿namespace SunshareCommon
{
    public static class ContractOptions
    {
        public const int SendInfo = 1;
        public const int GenerateContract = 2;
        public const int SaveLead = 3;
        public const int Request = 4;
        public const int Clone = 5;
    }
}
