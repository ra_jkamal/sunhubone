﻿using System;

namespace SunshareCommon
{
    public static class DateTimeExtensions
    {
        public static DateTime StartOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            var diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }
            return dt.AddDays(-1 * diff).Date;
        }

        public static string StartOfWeekAsShortDateString(this DateTime dt, DayOfWeek startOfWeek)
        {
            return dt.StartOfWeek(startOfWeek).ToShortDateString();
        }

        public static bool IsToday(this DateTime? dt)
        {
            return dt.HasValue && DateTime.Today.Equals(dt);
        }

        private static DateTime EndOfWeek(this DateTime dt, DayOfWeek startOfWeek)
        {
            var diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }
            return dt.AddDays(1 * diff).Date;
        }

        public static string EndOfWeekAsShortDateString(this DateTime dt, DayOfWeek startOfWeek)
        {
            return dt.EndOfWeek(startOfWeek).ToShortDateString();
        }

        public static bool TimeBetween(this DateTime? dt, DateTime start, DateTime end)
        {
            // convert datetime to a TimeSpan
            if (dt == null) return false;
            // see if start comes before end
            if (start < end)
                return start.Date <= ((DateTime)dt).Date && ((DateTime)dt).Date <= end;
            // start is after end, so do the inverse comparison
            return !(end < ((DateTime)dt).Date && ((DateTime)dt).Date < start);
        }
    }
}
