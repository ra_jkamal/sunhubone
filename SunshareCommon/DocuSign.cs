﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using DocuSign.eSign.Api;
using DocuSign.eSign.Client;
using DocuSign.eSign.Model;
using Configuration = DocuSign.eSign.Client.Configuration;

namespace SunshareCommon
{
    public class CoreRecipes
    {
        // Integrator Key (aka API key) is needed to authenticate your API calls.  This is an application-wide key
        private static readonly string IntegratorKey = ConfigurationManager.AppSettings["integratorKey"]; 
        private readonly string _accountId;
        public CoreRecipes(string userName, string password)
        {
            ConfigureApiClient(ConfigurationManager.AppSettings["isDemo"] == "false"
                ? "https://na2.docusign.net/restapi"
                : "https://demo.docusign.net/restapi");
            _accountId = LoginApi(userName, password);
        }

        public EnvelopeSummary RequestSignatureOnDocument(string cusEmail,
            string cusName, IEnumerable<string> filePaths, string repEmail, IEnumerable<string> fileNames, string utilityAccountNo = null, bool isCustomer = false)
        {
            var envDef = new EnvelopeDefinition
            {
                EmailSubject = Messages.EmailSendAgreement,
                Documents = new List<Document>()
            };
            int[] documentId = { 1 };
            foreach (var doc in filePaths.Select(File.ReadAllBytes).Select(fileBytes => new Document
            {
                DocumentId = Convert.ToString(documentId[0]),
                DocumentBase64 = Convert.ToBase64String(fileBytes),
                Name = $"{fileNames.ToList()[documentId[0] - 1]}.pdf"
            }))
            {
                documentId[0] = documentId[0] + 1;
                envDef.Documents.Add(doc);
            }
            Text accountNoOne = null;
            Text accountNoTwo = null;
            if (string.IsNullOrEmpty(utilityAccountNo))
            {
                accountNoOne = new Text
                {
                    DocumentId = "1",
                    PageNumber = "2",
                    XPosition = "170",
                    YPosition = "315",
                    Required = "true",
                    Locked = "false",
                    ValidationPattern = "[0-9]+",
                    Font = "Arial",
                    FontSize = "5",
                    DisableAutoSize = "true",
                    Width = 152,
                    ValidationMessage = "Please enter valid account Number"
                };
                accountNoTwo = new Text
                {
                    DocumentId = "2",
                    PageNumber = "2",
                    XPosition = "345",
                    YPosition = "645",
                    Required = "true",
                    Locked = "false",
                    ValidationPattern = "[0-9]+",
                    Font = "Arial",
                    FontSize = "5",
                    DisableAutoSize = "true",
                    Width = 152,
                    ValidationMessage = "Please enter valid account Number"
                };
            }
            var signHereOne = new SignHere
            {
                DocumentId = "1",
                PageNumber = "9",
                RecipientId = "2",
                XPosition = "350",
                YPosition = "420"
            };
            var signHereTwo = new SignHere
            {
                DocumentId = "2",
                PageNumber = "2",
                RecipientId = "2",
                XPosition = "240",
                YPosition = "667"
            };
            var signHereThree = new SignHere
            {
                DocumentId = "3",
                PageNumber = "3",
                RecipientId = "2",
                XPosition = "170",
                YPosition = "620"
            };

            if (isCustomer)
            {
                var rep = new CarbonCopy
                {
                    Email = repEmail,
                    Name = "Sunshare User",
                    RecipientId = "2"
                };
                var cus = new Signer
                {
                    Email = cusEmail,
                    UserId = "1",
                    RecipientId = "1",
                    Name = cusName,
                    Tabs = new Tabs { SignHereTabs = new List<SignHere>(), TextTabs = new List<Text>() }
                };
                if (accountNoOne != null & accountNoTwo != null)
                {
                    cus.Tabs.TextTabs.Add(accountNoOne);
                    cus.Tabs.TextTabs.Add(accountNoTwo);
                }

                cus.Tabs.SignHereTabs.Add(signHereOne);
                cus.Tabs.SignHereTabs.Add(signHereTwo);
                cus.Tabs.SignHereTabs.Add(signHereThree);

                envDef.Recipients = new Recipients { CarbonCopies = new List<CarbonCopy> { rep }, Signers = new List<Signer> { cus } };
            }
            else
            {
                var inPerson = new InPersonSigner
                {
                    HostEmail = repEmail,
                    UserId = "1",
                    RecipientId = "2",
                    HostName = "Andrew Subash",
                    SignerEmail = cusEmail,
                    SignerName = cusName,
                    RoutingOrder = "1",
                    Tabs = new Tabs { SignHereTabs = new List<SignHere>(), TextTabs = new List<Text>() }
                };
                if (accountNoOne != null & accountNoTwo != null)
                {
                    inPerson.Tabs.TextTabs.Add(accountNoOne);
                    inPerson.Tabs.TextTabs.Add(accountNoTwo);
                }
                inPerson.Tabs.SignHereTabs.Add(signHereOne);
                inPerson.Tabs.SignHereTabs.Add(signHereTwo);
                inPerson.Tabs.SignHereTabs.Add(signHereThree);

                envDef.Recipients = new Recipients { InPersonSigners = new List<InPersonSigner> { inPerson } };
            }
            var emailFilePath = HostingEnvironment.MapPath("~/FileServer/Upload/ContractEmailTemplate.html");
            if (emailFilePath != null)
            {
                var mailTemplate = File.ReadAllText(emailFilePath).Replace("{0}", cusName);
                envDef.EmailBlurb = mailTemplate;
            }
            envDef.Status = "sent";
            var envelopesApi = new EnvelopesApi();
            var envelopeSummary = envelopesApi.CreateEnvelope(_accountId, envDef);
            return envelopeSummary;
        }
        public Envelope GetEnvelopeInformation(string envelopeId)
        {
            try
            {
                var envelopesApi = new EnvelopesApi();
                var envInfo = envelopesApi.GetEnvelope(_accountId, envelopeId);
                return envInfo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void VoidContract(string envelopeId)
        {
            var envelopesApi = new EnvelopesApi();
            var envelope = envelopesApi.GetEnvelope(_accountId, envelopeId);
            envelope.Status = "voided";
            envelope.VoidedReason = "Contract regenerated";
            envelope.PurgeState = null;
            envelopesApi.Update(_accountId, envelopeId, envelope);
        }
        private static void ConfigureApiClient(string basePath)
        {
            var apiClient = new ApiClient(basePath);
            Configuration.Default.ApiClient = apiClient;
        }
        private static string LoginApi(string userName, string password)
        {
            var authHeader = "{\"Username\":\"" + userName + "\", \"Password\":\"" + password +
                             "\", \"IntegratorKey\":\"" + IntegratorKey + "\"}";
            Configuration.Default.DefaultHeader.Remove("X-DocuSign-Authentication");
            Configuration.Default.AddDefaultHeader("X-DocuSign-Authentication", authHeader);
            var authApi = new AuthenticationApi();
            var loginInfo = authApi.Login();
            var accountId =
                (from loginAcct in loginInfo.LoginAccounts
                 where loginAcct.IsDefault == "true"
                 select loginAcct.AccountId).FirstOrDefault() ??
                loginInfo.LoginAccounts[0].AccountId;
            return accountId;
            //return "32325491";
        }
    }
}




