﻿namespace SunshareCommon
{
    public static class DocuSignEnvelopeStatus
    {
        public const string Signed = "signed";
        public const string Completed = "completed";
        public const string Declined = "declined";
    }
}
