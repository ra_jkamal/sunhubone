﻿using System.Collections.Generic;
using SunshareCommon.com.echosign.secure;


namespace SunshareCommon
{
    public class DocumentCreationInfo
    {
        public List<FileInfo> fileInfos { get; set; }
        public string name { get; set; }
        public List<RecipientInfo> recipientSetInfos { get; set; }
        public SignatureType signatureType { get; set; }
        public SignatureFlow signatureFlow { get; set; }
    }
}