﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SunshareCommon.com.echosign.secure;

namespace SunshareCommon
{
    public class EchoAgreementStatus
    {
        public AgreementStatus? Status { get; set; }
        public DateTime UpdateDateTime { get; set; }
    }
}
