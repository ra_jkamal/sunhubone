﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using SunshareCommon.com.echosign.secure;

namespace SunshareCommon
{
    public class EchoSign
    {
        private static EchoSignDocumentService22 Sign { get; set; }
        private readonly string _echoSignApiKey = ConfigurationManager.AppSettings["echoApiKey"];
        private readonly string _echoServiceUrl = ConfigurationManager.AppSettings["echoUrl"];
        public EchoSign()
        {
            Sign=new EchoSignDocumentService22();
            Sign.Url = _echoServiceUrl;
            Sign.Url = GetBaseUris(_echoSignApiKey, _echoServiceUrl);
        }
/*
        private const string UsageText =
            @"Usage:
echosoap.exe <Service URL> <API key> <function> [parameters]
  
where the function is one of:
test
createFormFieldLayerTemplate [<filename>] ('testtemplate.pdf' will be used if <filename> is not provided)
send <filename> <recipient_email>  <recipient_set_email_separated by commas>
sendWithFormFieldLayerTemplate <filename> <libraryTemplateKey> <recipient_email> <two_recipient_set_email_separated by commas>
info <documentKey>
latest <documentKey> <filename>
version <versionKey> <filename>

test                            will run basic tests to make sure you can communicate with the web service
createFormFieldLayerTemplate    will create a form field layer template, and returns a libraryTemplateKey
send                            will create a new agreement in the EchoSign system, and returns a documentKey
sendWithFormFieldLayerTemplate  will create a new agreement with the specified form field layer template applied
info                            returns the current status and all the history events for a given documentKey
latest                          saves the latest version of the document as a PDF with the given filename
version                         saves a specific version of the document (referenced by the history events)
";
*/


        private static string GetVersion(string data)
        {
            return Regex.Replace(data, @"[^\d]", "");
        }

        public static string GetBaseUris(string apiKey, string url)
        {
            string baseUri;
            var uri = new Uri(url);
            var echoSignDocumentServiceVersion = GetVersion(uri.LocalPath);
            var version = int.Parse(echoSignDocumentServiceVersion);
            if (version < 22)
            {
                baseUri = url;
            }
            else
            {
                var result = Sign.getBaseUris(apiKey);
                var baseUris = result.apiBaseUri;
                var baseUriHost = new Uri(baseUris);
                baseUri = baseUriHost.Scheme + "://" + baseUriHost.Host + uri.LocalPath;
            }
            return baseUri;
        }

        

        public EchoAgreementStatus GetDocumentInfo(string apiKey, string documentKey)
        {
            var info = Sign.getDocumentInfo(apiKey, documentKey);
            

            //Console.WriteLine("Document history: ");
            //foreach (DocumentHistoryEvent docEvent in info.events)
            //{
            ////    Console.WriteLine(
            ////        "\t" +
            //        docEvent.description 
                    
            ////        " on " +
            //        docEvent +
            ////        (docEvent.documentVersionKey == null
            ////            ? ""
            ////            : (" (versionKey: " + docEvent.documentVersionKey + ")")
            ////        )
            ////    );
            //}
            return new EchoAgreementStatus
            {
                Status = info.status,
                UpdateDateTime = info.expiration
            };
        }

        private static FileStream GetPdfFile(string filePath)
        {
            return File.OpenRead(filePath);
        }

        public string CreateLibraryTemplate(string apiKey, string filename, LibraryTemplateType type)
        {
            var templateFile = File.OpenRead(filename);
            var buf = new byte[templateFile.Length];
            filename = Path.GetFileName(templateFile.Name);
            var fileInfos = new com.echosign.secure.FileInfo[1];
            templateFile.Read(buf, 0, (int) templateFile.Length);
            fileInfos[0] = new com.echosign.secure.FileInfo()
            {
                fileName = filename,
                file = buf
            }; 
            var libraryTemplateTypes = new LibraryTemplateType?[1];
            {
                libraryTemplateTypes[0] = LibraryTemplateType.FORM_FIELD_LAYER;
                var libraryInfo = new LibraryDocumentCreationInfo()
                {
                    name = filename,
                    fileInfos = fileInfos,
                    signatureType = SignatureType.ESIGN,
                    signatureFlow = SignatureFlow.SENDER_SIGNATURE_NOT_REQUIRED,
                    librarySharingMode = LibrarySharingMode.USER,
                    libraryTemplateTypes = libraryTemplateTypes
                };
                return Sign.createLibraryDocument(apiKey, null, libraryInfo).documentKey;
                //Console.WriteLine("Template key is: " + result.documentKey);
            }
        }

        public string SendDocument(string apiKey, List<string> filePathsList,List<string> fileNames, string formFieldLayerTemplateKey,string repemail, string recipient, string recipientSet)
        {
            var fileInfos = new com.echosign.secure.FileInfo[filePathsList.Count];
            for (int count = 0; count < filePathsList.Count; count++)
            {
                var file = GetPdfFile(filePathsList[count]);
                var buf = new byte[file.Length];
                file.Read(buf, 0, (int)file.Length);
                fileInfos[count] = new com.echosign.secure.FileInfo()
                {
                    fileName = fileNames[count],
                    file = buf,
                    mimeType = "application/pdf"
                };
            }
            //TODO: move to config
            SenderInfo senderInfo = new SenderInfo
            {
                email = "andrew.s@infinitelinked.com",
                password = "loveline@07"
            };
            var recipientInfo = new RecipientInfo[1];

            var recipientEmail = new RecipientInfo
            {
                email = recipient,
                role = RecipientRole.SIGNER,
                roleSpecified = true
            };
            recipientInfo[0] = recipientEmail;
            
            var carbonCopies = (!string.IsNullOrEmpty(repemail)) ? new string[1] {repemail} : new string[0] {};

            var documentInfo = new com.echosign.secure.DocumentCreationInfo()
            {
                fileInfos = fileInfos,
                signatureTypeSpecified = true,
                signatureType = SignatureType.ESIGN,
                signatureFlowSpecified = true,
                signatureFlow = SignatureFlow.SENDER_SIGNATURE_NOT_REQUIRED,
                recipients = recipientInfo,
                name = "Sunshare Agreement",
                ccs = carbonCopies
            };
            if (string.IsNullOrEmpty(formFieldLayerTemplateKey))
                return Sign.sendDocument(apiKey, senderInfo, documentInfo)[0].documentKey;
            var formFieldLayerTemplates = new com.echosign.secure.FileInfo[1];
            formFieldLayerTemplates[0] = new com.echosign.secure.FileInfo()
            {
                documentKey = formFieldLayerTemplateKey
            };
            documentInfo.formFieldLayerTemplates = formFieldLayerTemplates;
            return Sign.sendDocument(apiKey, senderInfo, documentInfo)[0].documentKey;
            //Console.WriteLine("Document key is: " + documentKeys[0].documentKey);
        }

        public static void GetDocument(string apiKey, string documentKey, string versionKey, string fileName)
        {
            var data = (documentKey != null)
                ? Sign.getLatestDocument(apiKey, documentKey)
                : Sign.getDocumentByVersion(apiKey, versionKey);
            var file = File.OpenWrite(fileName);
            file.Write(data, 0, data.Length);
        }

    }
}
