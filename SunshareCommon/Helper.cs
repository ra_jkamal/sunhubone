﻿using System;
using System.IO;
using System.ServiceModel;
using System.Web.Hosting;

namespace SunshareCommon

{

    public class Helper
    {
        public static void LogError(Exception exception)
        {

            // use Path.Combine to combine 2 strings to a path
            
            string dir = Path.Combine(HostingEnvironment.MapPath("~/Error/"));

            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
                File.WriteAllText(Path.Combine(dir, "log.txt"), "Message :" + exception.Message + "<br/>" + Environment.NewLine + "StackTrace :" + exception.StackTrace + "" + Environment.NewLine + "Date :" + DateTime.Now);
                string New = Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine;
                File.AppendAllText(HostingEnvironment.MapPath("~/Error/log.txt"), New);
            }
            else
            {
                File.AppendAllText(HostingEnvironment.MapPath("~/Error/log.txt"), "Message :" + exception.Message + "<br/>" + Environment.NewLine + "StackTrace :" + exception.StackTrace + "" + Environment.NewLine + "Date :" + DateTime.Now);
                string New = Environment.NewLine + "-----------------------------------------------------------------------------" + Environment.NewLine;
                File.AppendAllText(HostingEnvironment.MapPath("~/Error/log.txt"), New);
            }


        }

        public static FaultException ConvertToSoapFault(Exception ex)
        {
            return new FaultException(ex.Message);
        }
    }
}
