﻿using System.Linq;
using System.Security.Principal;
using SunshareDatabase;

namespace SunshareCommon
{
    public static class IdentityExtensions
    {

        public static Efficiency GetEfficientPercentage(this IIdentity identity, string userId)
        {
            //return "1";
            //var claim = ((System.Security.Claims.ClaimsIdentity)identity).FindFirst("EfficientPercentage");
            using (var context = new SunshareDatabaseContext())
            {
                context.Database.Connection.Open();
                var createdLead = context.Leads.Count(c => c.AspNetUsersId.ToString() == userId);
                var convertedLead = context.Leads.Count(c => c.AspNetUsersId.ToString() == userId & context.LeadLeadStatusMappings.FirstOrDefault(l => l.LeadId == c.LeadId & l.IsCurrent).LeadStatusId == LeadContractStatus.ContractSigned);
                // Test for null to avoid issues during local testing
                return new Efficiency { CreatedLead = createdLead, ConvertedLead = convertedLead };
            }

        }
        public static string GetDisplayName(this IIdentity identity, string userId)
        {
            using (var context = new SunshareDatabaseContext())
            {
                var user = context.Users.FirstOrDefault(u => u.Id.ToString() == userId);
                return (user!=null)? $"{user.FirstName} {user.LastName}" : string.Empty;
            }
        }
        public static string GetPartnerImageUrl(this IIdentity identity, string userId)
        {
            using (var context = new SunshareDatabaseContext())
            {
                var user = context.Users.FirstOrDefault(u => u.Id.ToString() == userId);
                //var partner = context.Partners.FirstOrDefault(p => p.PartnerId == user.PartnerId);
                return (user != null ? user.ImageUrl : string.Empty);
            }
        }
    }
    public class Efficiency
    {
        public int CreatedLead { private get; set; }
        public int ConvertedLead { private get; set; }
        public string EfficiencyPercentage
        {
            get
            {
                if (CreatedLead == 0 || ConvertedLead == 0)
                {
                    return string.Empty;
                }
                return
                    $"{((decimal) (ConvertedLead/(float) CreatedLead)*100):0.00}";
            }
        }
    }

}
