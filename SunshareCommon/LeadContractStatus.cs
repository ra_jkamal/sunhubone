﻿namespace SunshareCommon
{
    public static class LeadContractStatus
    {
        public const int ContractSigned = 2;
        public const int ContractSent = 1;
        public const int ContractDeclined = 3;
        public const int CotractInformationSent = 4;
        public const int LeadCreated=5;
        public const int New = 0;
        public const int RefusedLead = 7;
    }
}
