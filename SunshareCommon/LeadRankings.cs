﻿namespace SunshareCommon
{
    public static class LeadRankings
    {
        public static int Cold = 1;
        public static int Standard = 2;
        public static int Warm = 3;
        public static int Hot = 4;
        public static int Fire = 5;
        //public static int NoRanking = 5;
    }
}
