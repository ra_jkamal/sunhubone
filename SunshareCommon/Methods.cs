﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.Hosting;
using iTextSharp.text.pdf;
using SunshareCommon.com.echosign.secure;
using SunshareDatabase;

namespace SunshareCommon
{
    public class Methods
    {
        private readonly SunshareDatabaseContext _context = new SunshareDatabaseContext();
        private readonly CoreRecipes _docuSign;
        private EchoSign _echoSign;

        private readonly string _docuSignUserName = ConfigurationManager.AppSettings["docusignUserName"];
            //"isaac@infinitelinked.com"; 
        private readonly string _echoSignApiKey = ConfigurationManager.AppSettings["echoApiKey"];
        readonly string _docuSignPassword = ConfigurationManager.AppSettings["docusignUserPassword"]; // "sunshare2016";

        public Methods()
        {
            _docuSign = new CoreRecipes(_docuSignUserName, _docuSignPassword);
            
        }

        public ContractReturn ContractReturn(Lead userLead, Address userAddress, AspNetUsers rep,
            AddressUserMapping mapping = null, bool isD2D = false)
        {
            var returnId=string.Empty;
            var documentName =
                $"pdf_SunShare_TEXT_{userLead.LeadId}_{userLead.FirstName}_{userLead.LastName}_{userAddress?.Zipcode}_{DateTime.Now.Month}{DateTime.Now.Day}{DateTime.Now.Year}_{String.Format("{0}{1}{2}", DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second)}";
            var documentPath = GenerateDocument(documentName, userLead, userAddress);
            if (rep == null) return new ContractReturn {ReturnMsg = "Something went wrong", ReturnCode = 401};
            var lead = _context.Leads.FirstOrDefault(l => l.IsActive & l.LeadId == userLead.LeadId);
            if (!isD2D)
            {
                _echoSign = new EchoSign();
                var trancientId = _echoSign.SendDocument(_echoSignApiKey,
                    documentPath.DocumentPaths, documentPath.DocumentNames, string.Empty,
                     rep.Email, userLead.PrimaryEmail,
                    $"{rep.Email}");
                if (lead != null)
                {
                    returnId = trancientId;
                    lead.TrancientId = returnId;
                    lead.AllowDuplicateContract = false;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(userLead.EnvelopeId))
                    _docuSign.VoidContract(userLead.EnvelopeId);
                var envelopesummary = _docuSign.RequestSignatureOnDocument(userLead.PrimaryEmail,
                    $"{userLead.FirstName} {userLead.LastName}", documentPath.DocumentPaths, rep.Email,
                    documentPath.DocumentNames,
                    userLead.UtilityAccountNo);
                if (lead != null)
                {
                    returnId = envelopesummary.EnvelopeId;
                    lead.EnvelopeId = returnId;
                    lead.AllowDuplicateContract = false;
                }
            }
            var currentLeadLeadStatusMapping =
                _context.LeadLeadStatusMappings.FirstOrDefault(l => l.IsCurrent & l.LeadId == userLead.LeadId);
            if (currentLeadLeadStatusMapping != null) currentLeadLeadStatusMapping.IsCurrent = false;
            _context.LeadLeadStatusMappings.Add(new LeadLeadStatusMapping()
            {
                IsCurrent = true,
                LeadId = userLead.LeadId,
                LeadStatusId = LeadContractStatus.ContractSent,
                UpdatedDate = DateTime.Today
            });
            _context.SaveChanges();
            return new ContractReturn
            {
                ReturnMsg = Messages.Success,
                ReturnCode = 200,
                //EnvelopeId = envelopesummary.EnvelopeId,
                EnvelopeId= returnId,
                LogId = Convert.ToString(mapping?.AddressUserMappingId ?? 0)
            };
        }

        public ContractReturn SendInformation(Lead userLead)
        {
            var filePath = HostingEnvironment.MapPath("~/FileServer/Upload/SendInformationTemplate.html");
            if (filePath == null)
                return new ContractReturn {ReturnMsg = Messages.ErrorTemplateMapping, ReturnCode = 401};
            var mailTemplate = File.ReadAllText(filePath)
                .Replace("{0}", userLead.FirstName)
                .Replace("{1}", userLead.LastName);
            SendEmail(userLead.PrimaryEmail, Convert.ToString(mailTemplate), Messages.EmailSendInformation, true);
            return new ContractReturn {ReturnMsg = Messages.Success, ReturnCode = 200};
        }

        public static bool SendEmail(string toEmail, string message, string subject, bool isHtml = false)
        {
            try
            {
                var mail = new MailMessage();
                var smtpServer = new SmtpClient("smtp.gmail.com");
                mail.From = new MailAddress("telesalessupport@sunsharecorp.com");
                mail.To.Add(toEmail);
                mail.Subject = subject;
                mail.Body = message;
                mail.IsBodyHtml = isHtml;
                smtpServer.Port = 587;
                smtpServer.Timeout = 10000;
                smtpServer.UseDefaultCredentials = false;
                smtpServer.Credentials = new NetworkCredential("telesalessupport@sunsharecorp.com", "SunShare1!");
                smtpServer.EnableSsl = true;
                smtpServer.Send(mail);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public LogReturn SendInfo(Lead userLead)
        {
            var filePath = HostingEnvironment.MapPath("~/FileServer/Upload/SendInformationTemplate.html");
            if (filePath == null) return new LogReturn {ReturnMsg = Messages.ErrorTemplateMapping, ReturnCode = 401};

            var mailTemplate = File.ReadAllText(filePath)
                .Replace("{0}", userLead.FirstName)
                .Replace("{1}", userLead.LastName);
            SendEmail(userLead.PrimaryEmail, Convert.ToString(mailTemplate), Messages.EmailSendInformation, true);
            return new LogReturn {ReturnMsg = Messages.Success, ReturnCode = 200};
        }

        public void UpdateDocuSignStatus(ICollection<Lead> leads)
        {
            foreach (var lead in leads)
            {
                var i = (lead.ChannelId == LeadChannels.D2D) ? UpdateLeadStatusD2D(lead) : UpdateLeadStatus(lead);
            }
        }

        public int UpdateLeadDocuSignStatus(Lead lead)
        {
            var status = lead.ChannelId == LeadChannels.D2D ? UpdateLeadStatusD2D(lead) : UpdateLeadStatus(lead);
            return status;
        }

        private int UpdateLeadStatus(Lead lead)
        {
            _echoSign = new EchoSign();
            var localLead = _context.Leads.FirstOrDefault(l => l.LeadId == lead.LeadId);
            if (localLead == null) return 0;
            {
                var leadLeadStatusMapping = localLead.LeadLeadStatusMappings.FirstOrDefault(l => l.IsCurrent);
                if (leadLeadStatusMapping == null ||
                    leadLeadStatusMapping.LeadStatusId != LeadContractStatus.ContractSent)
                    return leadLeadStatusMapping?.LeadStatusId ?? 0;
                if (string.IsNullOrEmpty(localLead.TrancientId)) return leadLeadStatusMapping.LeadStatusId;
                //var result = _docuSign.GetEnvelopeInformation(lead.EnvelopeId);
                var result = _echoSign.GetDocumentInfo(_echoSignApiKey, localLead.TrancientId);
                switch (result.Status)
                {
                    //case DocuSignEnvelopeStatus.Completed:
                    //case DocuSignEnvelopeStatus.Signed:
                    case AgreementStatus.SIGNED_IN_ADOBE_ACROBAT:
                    case AgreementStatus.SIGNED:
                    case AgreementStatus.SIGNED_IN_ADOBE_READER:
                        localLead.LeadLeadStatusMappings.Add(new LeadLeadStatusMapping
                        {
                            LeadStatusId = LeadContractStatus.ContractSigned,
                            IsCurrent = true,
                            //UpdatedDate = Convert.ToDateTime(result.UpdateDateTime)
                            UpdatedDate=DateTime.Now
                        });
                        leadLeadStatusMapping.IsCurrent = false;
                        _context.SaveChanges();
                        return LeadContractStatus.ContractSigned;
                    case AgreementStatus.ABORTED:
                    case AgreementStatus.EXPIRED:
                    case AgreementStatus.ARCHIVED:
                        localLead.LeadLeadStatusMappings.Add(new LeadLeadStatusMapping
                        {
                            LeadStatusId = LeadContractStatus.ContractDeclined,
                            IsCurrent = true,
                            UpdatedDate = DateTime.Now
                            //UpdatedDate = Convert.ToDateTime(result.UpdateDateTime)
                        });
                        leadLeadStatusMapping.IsCurrent = false;
                        _context.SaveChanges();
                        return LeadContractStatus.ContractDeclined;
                }
                return leadLeadStatusMapping.LeadStatusId;
                //if (string.IsNullOrEmpty(lead.EnvelopeId)) return leadLeadStatusMapping.LeadStatusId;
            }
        }

        private int UpdateLeadStatusD2D(Lead lead)
        {
            var localLead = _context.Leads.FirstOrDefault(l => l.LeadId == lead.LeadId);
            if (localLead == null) return 0;
            {
                var leadLeadStatusMapping = localLead.LeadLeadStatusMappings.FirstOrDefault(l => l.IsCurrent);
                if (leadLeadStatusMapping == null ||
                    leadLeadStatusMapping.LeadStatusId != LeadContractStatus.ContractSent)
                    return leadLeadStatusMapping?.LeadStatusId ?? 0;
                if (string.IsNullOrEmpty(localLead.EnvelopeId)) return leadLeadStatusMapping.LeadStatusId;
                var result = _docuSign.GetEnvelopeInformation(lead.EnvelopeId);
                switch (result.Status)
                {
                    case DocuSignEnvelopeStatus.Completed:
                    case DocuSignEnvelopeStatus.Signed:
                        localLead.LeadLeadStatusMappings.Add(new LeadLeadStatusMapping
                        {
                            LeadStatusId = LeadContractStatus.ContractSigned,
                            IsCurrent = true,
                            //UpdatedDate = Convert.ToDateTime(result.),
                            UpdatedDate = DateTime.Now
                        });
                        leadLeadStatusMapping.IsCurrent = false;
                        _context.SaveChanges();
                        return LeadContractStatus.ContractSigned;
                    case DocuSignEnvelopeStatus.Declined:
                        localLead.LeadLeadStatusMappings.Add(new LeadLeadStatusMapping
                        {
                            LeadStatusId = LeadContractStatus.ContractDeclined,
                            IsCurrent = true,
                            UpdatedDate = DateTime.Now
                            //UpdatedDate = Convert.ToDateTime(result.UpdateDateTime)
                        });
                        leadLeadStatusMapping.IsCurrent = false;
                        _context.SaveChanges();
                        return LeadContractStatus.ContractDeclined;
                }
                return leadLeadStatusMapping.LeadStatusId;
                //if (string.IsNullOrEmpty(lead.EnvelopeId)) return leadLeadStatusMapping.LeadStatusId;
            }
        }

        #region Private Methods

        private static DocumentDetail GenerateDocument(string filename, Lead customer, Address customerAddress)
        {
            var resiltDetails = new DocumentDetail();

            var contractReader = new PdfReader(HostingEnvironment.MapPath("~/FileServer/Upload/ContractTemplate.pdf"));
            var contractStamper = new PdfStamper(contractReader,
                new FileStream(
                    HostingEnvironment.MapPath("~/FileServer/" + filename.Replace("TEXT", "MN_SSA_Form") + ".pdf"),
                    FileMode.Append));
            contractStamper.AcroFields.SetField("Customer Name", $"{customer.FirstName} {customer.LastName}");
            contractStamper.AcroFields.SetField("Generated on", $"{DateTime.Today: MMM d, yyyy}");
            contractStamper.AcroFields.SetField("Mailing Address 1", customerAddress.AddressInformation);
            contractStamper.AcroFields.SetField("Mailing Address 2",
                $"{customerAddress.City}, {customerAddress.State}-{customerAddress.Zipcode}");
            contractStamper.AcroFields.SetField("Email Address", customer.PrimaryEmail);
            contractStamper.AcroFields.SetField("Street", customerAddress.AddressInformation);
            contractStamper.AcroFields.SetField("City", customerAddress.City);
            contractStamper.AcroFields.SetField("County", customerAddress.County);
            contractStamper.AcroFields.SetField("ssa", customer.SubscriptionPercentage.ToString());
            contractStamper.AcroFields.SetField("Up to", Convert.ToString(customer.SubscriptionPercentage));
            contractStamper.AcroFields.SetField("Date", DateTime.Today.ToString("MM/dd/yyyy"));
            foreach (var de in contractStamper.AcroFields.Fields)
            {
                contractStamper.AcroFields.SetFieldProperty(de.Key, "setfflags", PdfFormField.FF_READ_ONLY, null);
            }
            //SetFieldsReadOnly(contractReader, contractStamper);
            contractStamper.AcroFields.SetField("Xcel Account Number", customer.UtilityAccountNo);
            contractStamper.Close();
            contractReader.Close();
            resiltDetails.DocumentNames.Add(filename.Replace("TEXT", "MN_Contract_Form"));
            resiltDetails.DocumentPaths.Add(
                HostingEnvironment.MapPath("~/FileServer/" + filename.Replace("TEXT", "MN_SSA_Form") + ".pdf"));
            var consentReader = new PdfReader(HostingEnvironment.MapPath("~/FileServer/Upload/MN Consent Form.pdf"));
            var consentStamper = new PdfStamper(consentReader,
                new FileStream(
                    HostingEnvironment.MapPath("~/FileServer/" + filename.Replace("TEXT", "MN_Consent_Form") + ".pdf"),
                    FileMode.Append));
            consentStamper.AcroFields.SetField("UITILITY ACCOUNT NUMBER",customer.UtilityAccountNo);
            consentStamper.AcroFields.SetField("FULL SERVICE ADDRESS",
                $"{customerAddress.AddressInformation}, {customerAddress.City}, {customerAddress.State}-{customerAddress.Zipcode}");
            consentStamper.AcroFields.SetField("Customer Name", $"{customer.FirstName} {customer.LastName}");
            consentStamper.AcroFields.SetField("mm", DateTime.Now.Month.ToString());
            consentStamper.AcroFields.SetField("dd", DateTime.Now.Day.ToString());
            consentStamper.AcroFields.SetField("yyyy", DateTime.Now.Year.ToString());
           
            //SetFieldsReadOnly(contractReader, contractStamper);
            foreach (var de in consentStamper.AcroFields.Fields)
            {
                consentStamper.AcroFields.SetFieldProperty(de.Key, "setfflags", PdfFormField.FF_READ_ONLY, null);
            }
            consentStamper.AcroFields.SetField("Xcel Account Number", customer.UtilityAccountNo);
            consentStamper.AcroFields.SetFieldProperty("Xcel Account Number", "setfflags", PdfFormField.FF_EDIT, null);
            consentStamper.AcroFields.SetFieldProperty("Xcel Account Number", "setfflags", PdfFormField.FF_REQUIRED, null);
           
            consentStamper.Close();
            consentReader.Close();
            resiltDetails.DocumentNames.Add(filename.Replace("TEXT", "MN_Consent_Form"));
            resiltDetails.DocumentPaths.Add(
                HostingEnvironment.MapPath("~/FileServer/" + filename.Replace("TEXT", "MN_Consent_Form") + ".pdf"));
            var blankReader = new PdfReader(HostingEnvironment.MapPath("~/FileServer/Upload/MN BLANK SAA.pdf"));
            var blankStamper = new PdfStamper(blankReader,
                new FileStream(
                    HostingEnvironment.MapPath("~/FileServer/" + filename.Replace("TEXT", "MN_Blank_Form") + ".pdf"),
                    FileMode.Append));
            blankStamper.AcroFields.SetField("SubscribersName", $"{customer.FirstName} {customer.LastName}");
            blankStamper.AcroFields.SetField("Email Address", customer.PrimaryEmail);
            blankStamper.AcroFields.SetField("UITILITY ACCOUNT NUMBER", customer.UtilityAccountNo);
            blankStamper.AcroFields.SetField("SubscriberName", $"{customer.FirstName} {customer.LastName}");
            blankStamper.AcroFields.SetField("SubscriberDate", DateTime.Today.ToString("MM/dd/yyyy"));
            SetFieldsReadOnly(contractReader, contractStamper);
            foreach (var de in blankStamper.AcroFields.Fields)
            {
                blankStamper.AcroFields.SetFieldProperty(de.Key, "setfflags", PdfFormField.FF_READ_ONLY, null);
            }
            blankStamper.Close();
            blankReader.Close();
            resiltDetails.DocumentNames.Add(filename.Replace("TEXT", "MN_Blank_Form"));
            resiltDetails.DocumentPaths.Add(
                HostingEnvironment.MapPath("~/FileServer/" + filename.Replace("TEXT", "MN_Blank_Form") + ".pdf"));
            return resiltDetails;
        }

        private static void SetFieldsReadOnly(PdfReader pdfReader, PdfStamper formFields)
        {
            foreach (var de in pdfReader.AcroFields.Fields)
            {
                formFields.AcroFields.SetFieldProperty(de.Key,"sunshareflag",PdfFormField.FF_READ_ONLY,null);
            }
        }

        private class DocumentDetail
        {
            public DocumentDetail()
            {
                DocumentPaths = new List<string>();
                DocumentNames = new List<string>();
            }

            public List<string> DocumentPaths { get; }
            public List<string> DocumentNames { get; }
        }

        #endregion

        public void SendInformation(int leadId)
        {
            SendInformation(_context.Leads.FirstOrDefault(l => l.IsActive & l.LeadId == leadId));
        }
    }
}
