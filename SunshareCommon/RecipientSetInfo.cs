﻿using System.Collections.Generic;

namespace SunshareCommon
{
    public class RecipientSetInfo
    {
        public List<RecipientSetMemberInfo> recipientSetMemberInfos { get; set; }
        public string recipientSetRole { get; set; }
    }
}