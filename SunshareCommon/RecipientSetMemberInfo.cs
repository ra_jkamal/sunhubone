﻿namespace SunshareCommon
{
    public class RecipientSetMemberInfo
    {
        public string email { get; set; }
        public string fax { get; set; }
    }
}