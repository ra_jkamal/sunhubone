﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;

namespace SunshareCommon
{
    #region retunClasses

    [DataContract]

    public class BaseReturn
    {
        [DataMember]
        public int ReturnCode { get; set; }
        [DataMember]
        public string ReturnMsg { get; set; }
    }

    public class FileUploadReturn : BaseReturn
    {
        [DataMember]
        public string FileName;
        [DataMember]
        public string Url;
        [DataMember]
        public int UserId;
    }

    [DataContract]
    public class FileUploadMessage
    {
        [DataMember(Order = 1)]
        public Stream FileByteStream;
        [DataMember]
        public string FileName;
        [DataMember]
        public int UserId;
    }



    [DataContract]
    public class BaseReturnWithLogId : BaseReturn
    {
        [DataMember]
        public string LogId { get; set; }
    }

    [DataContract]
    public class ContractReturn : BaseReturnWithLogId
    {
        [DataMember]
        public string EnvelopeId { get; set; }
    }

    [DataContract]
    public class DeleteLogReturn : BaseReturn
    {

    }

    [DataContract]
    public class PasswordReturn : BaseReturn
    {
    }

    [DataContract]
    public class AnalysisReturn : BaseReturn
    {
        public AnalysisReturn()
        {
            WeeklyData = new List<UserAnalysis>();
        }
        [DataMember]
        public string WeekPeriod { get; set; }
        [DataMember]
        public ICollection<UserAnalysis> WeeklyData { get; set; }
        [DataMember]
        public int TodayAppointment { get; set; }
        [DataMember]
        public int TodatOutstandingContract { get; set; }
        [DataMember]
        public int FireLeads { get; set; }
    }

    [DataContract]
    public class UserAnalysis
    {
        [DataMember]
        public string UserName { get; set; }
        [DataMember]
        public int Knocks { get; set; }
    }

    [DataContract]
    public class LogReturn : BaseReturnWithLogId
    {

    }

    [DataContract]
    public class SalesRepReturn : BaseReturn
    {
        [DataMember]
        public ICollection<LeadInfo> TodayAppointments { get; set; }
        [DataMember]
        public ICollection<LeadInfo> TodayKnock { get; set; }
        [DataMember]
        public ICollection<LeadInfo> OutstandingLeads { get; set; }
        [DataMember]
        public ICollection<LeadInfo> HotLeads { get; set; }
        [DataMember]
        public ICollection<LeadInfo> AssociatedLeads { get; set; }
    }

    [DataContract]
    public class LeadInfo
    {
        [DataMember]
        public string Lattitude { get; set; }

        [DataMember]
        public string Longitude { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string Zip { get; set; }

        [DataMember]
        public string AppointmentDate { get; set; }

        [DataMember]
        public string AppointmentTime { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string Mobile { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string VisitStatus { get; set; }

        [DataMember]
        public string Rank { get; set; }

        [DataMember]
        public string Notes { get; set; }

        [DataMember]
        public string LogId { get; set; }

        [DataMember]
        public string LeadContractStatus { get; set; }

        [DataMember]
        public string ContractStatusUpdateDate { get; set; }

        [DataMember]
        public string ContractStatusUpdateTime { get; set; }

        [DataMember]
        public string DateVisited { get; set; }

        [DataMember]
        public string EnvelopeId { get; set; }
        [DataMember]
        public string TranscientId { get; set; }
    }

    [DataContract]
    public class UserDetails
    {
        [DataMember]
        public int ReturnCode { get; set; }
        [DataMember]
        public string ReturnMsg { get; set; }
        [DataMember]
        public int SaleRepId { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string ProfilePic { get; set; }
        [DataMember]
        public string PartnerLogoPic { get; set; }
    }
    #endregion
}
