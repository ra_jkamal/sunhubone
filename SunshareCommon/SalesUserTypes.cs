﻿namespace SunshareCommon
{
    public static class SalesUserTypes
    {
        public static int D2D = 1;
        public static int TeleSales = 2;
        public static int Website = 3;
    }
}
