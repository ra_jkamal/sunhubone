﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using SunshareCommon;
using SunshareDatabase;
using System.Web;
using System.Web.Hosting;
using System.Web.Security;
using System.ServiceModel.Activation;

namespace SunshareD2DServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class D2DService : ID2DService
    {
        private readonly SunshareDatabaseContext _context = new SunshareDatabaseContext();
        
        //TODO:Check is this required here?
        private readonly UserDetails _userDetail = new UserDetails
        {
            ReturnCode = 401,
            ReturnMsg = "No data",
            PartnerLogoPic = string.Empty,
            ProfilePic = string.Empty
        };
        private readonly PasswordReturn _passwordReturn = new PasswordReturn { ReturnCode = 401, ReturnMsg = "No Data" };

        private readonly Methods _methods = new Methods();
        public UserDetails ValidateLogin(string emailId, string password, string partnerId)
        {
            var allUsers = _context.Users;
            if (!string.IsNullOrEmpty(emailId) && !string.IsNullOrEmpty(password) && !string.IsNullOrEmpty(partnerId))
            {
                var partner = _context.Partners.FirstOrDefault(p => p.PartnerNumber == partnerId);
                if (partner != null)
                {
                    if (!allUsers.Any()) return _userDetail;
                    if (!allUsers.Any(u => u.Email == emailId & u.IsActive))
                    {
                        _userDetail.ReturnMsg = Messages.ValidEmailId;
                        return _userDetail;
                    }
                    var user = allUsers.FirstOrDefault(u => u.Email == emailId);
                    if (user != null)
                    {
                        if (user.PasswordHash != password)
                        {
                            _userDetail.ReturnMsg = Messages.ValidPassword;
                            return _userDetail;
                        }
                        if (user.PartnerId == partner.PartnerId)
                            return new UserDetails
                            {
                                Name = $"{user.FirstName} {user.LastName}",
                                ProfilePic = string.IsNullOrEmpty(user.ImageUrl) ? string.Empty : user.ImageUrl,
                                PartnerLogoPic = VirtualPathUtility.ToAbsolute($"~/FileServer/{partner.PartnerLogoUrl}"),
                                ReturnCode = 200,
                                ReturnMsg = Messages.ValidationSuccess,
                                SaleRepId = user.Id
                            };
                        _userDetail.ReturnMsg = Messages.ValidPartnerId;
                        return _userDetail;
                    }
                }
                else
                {
                    _userDetail.ReturnMsg = Messages.ValidPartnerId;
                    return _userDetail;
                }
            }
            _userDetail.ReturnMsg = Messages.ValidEmailandPassword;
            return _userDetail;
        }

        public PasswordReturn ForgotPassword(string emailId)
        {
            var allUsers = _context.Users;
            if (!string.IsNullOrEmpty(emailId))
            {
                if (!allUsers.Any()) return _passwordReturn;
                if (!allUsers.Any(u => u.Email == emailId & u.IsActive))
                {
                    _passwordReturn.ReturnMsg = Messages.ValidEmailId;
                    return _passwordReturn;
                }
                var user = allUsers.FirstOrDefault(u => u.Email == emailId & u.IsActive);
                if (user == null) return _passwordReturn;
                user.PasswordHash = Membership.GeneratePassword(6, 0);
                _context.SaveChanges();
                PasswordReturn forgotPassword;
                return SetPasswordResetTemplateAndEmail(emailId, user, out forgotPassword) ? forgotPassword : _passwordReturn;
            }
            _passwordReturn.ReturnMsg = Messages.ValidEmailId;
            return _passwordReturn;
        }

        public PasswordReturn ChangePassword(string emailId, string oldPassword, string newPassword)
        {
            var allUsers = _context.Users;
            if (!string.IsNullOrEmpty(emailId) && !string.IsNullOrEmpty(oldPassword) && !string.IsNullOrEmpty(newPassword))
            {
                if (!allUsers.Any()) return _passwordReturn;
                if (allUsers.Any(u => u.Email == emailId & u.IsActive))
                {
                    var user = allUsers.FirstOrDefault(u => u.Email == emailId & u.IsActive);
                    if (user != null)
                    {
                        if (user.PasswordHash != oldPassword)
                        {
                            _passwordReturn.ReturnMsg = Messages.ValidPassword;
                            return _passwordReturn;
                        }
                        user.PasswordHash = newPassword;
                        _context.SaveChanges();
                        _passwordReturn.ReturnCode = 200;
                        _passwordReturn.ReturnMsg = Messages.PasswordChangedSuccess;
                        return _passwordReturn;
                    }
                }
            }
            _passwordReturn.ReturnMsg = Messages.ValidEmailId;
            return _passwordReturn;
        }

        public SalesRepReturn SalesRepData(int salesRepId)
        {
            var salesRepReturn = new SalesRepReturn { ReturnMsg = "Enter valid representative Id", ReturnCode = 401 };
            if (salesRepId == 0) return salesRepReturn;
            var rep = _context.Users.FirstOrDefault(u => u.Id == salesRepId & u.IsActive);
            if (rep != null)
            {
                var userLeads = rep.Leads.Where(l => l.IsActive).ToList();
                //_methods.UpdateDocuSignStatus(userLeads);
                salesRepReturn.AssociatedLeads = MapLeads(userLeads, rep);
                _context.SaveChanges();
            }
            salesRepReturn.ReturnCode = 200;
            salesRepReturn.ReturnMsg = Messages.Success;
            return salesRepReturn;
        }

        public AnalysisReturn GetRepAnalysis(int salesRepId)
        {
            var analysis = new AnalysisReturn { ReturnMsg = "Enter valid representative Id", ReturnCode = 401 };
            if (salesRepId == 0) return analysis;
            var rep = _context.Users.FirstOrDefault(u => u.Id == salesRepId & u.IsActive);
            if (rep != null)
            {
                var leads = rep.Leads.Where(l => l.IsActive).ToList();
                //_methods.UpdateDocuSignStatus(leads);
                var userAppointments = rep.AddressUserMappings.Where(a => a.VisitStatusId == LeadVisitStatus.Appointment & a.IsActive).ToList();
                analysis.FireLeads = leads.Count(l => l.LeadRankingId == LeadRankings.Fire);
                var count = leads.Select(lead => lead.LeadLeadStatusMappings.FirstOrDefault(l => l.IsCurrent)).Count(leadLeadStatusMapping => leadLeadStatusMapping?.LeadStatusId == LeadContractStatus.ContractSent);
                analysis.TodatOutstandingContract = count;
                analysis.WeekPeriod =
                    $"{DateTime.Now.StartOfWeekAsShortDateString(DayOfWeek.Monday)} to {DateTime.Now.ToShortDateString()}";
                analysis.TodayAppointment = userAppointments.Count(u => u.AppointmentDate.IsToday());
                analysis.WeeklyData = GetWeeklyData(DateTime.Now.StartOfWeek(DayOfWeek.Monday), DateTime.Now);
                analysis.ReturnMsg = Messages.Success;
                analysis.ReturnCode = 200;
                return analysis;
            }
            analysis.ReturnMsg = Messages.FailureNoRepFound;
            analysis.ReturnCode = 401;
            return analysis;
        }

        public DeleteLogReturn DeleteVisit(int logId)
        {
            var mapping = _context.AddressUserMappings.FirstOrDefault(a => a.AddressUserMappingId == logId & a.IsActive);
            if (mapping == null) return new DeleteLogReturn { ReturnMsg = Messages.RecordNotFound, ReturnCode = 401 };
            {
                var userAddress = mapping.Address;
                var userLead = _context.Leads.FirstOrDefault(
                    s => (s.ServiceAddressId == userAddress.AddressId || s.BillingAddressId == userAddress.AddressId) & s.IsActive);
                using (var transactionContext = _context.Database.BeginTransaction())
                {
                    try
                    {
                        mapping.IsActive = false;
                        if (userAddress != null) userAddress.IsActive = false;
                        if (userLead != null) userLead.IsActive = false;
                        _context.SaveChanges();
                        transactionContext.Commit();
                        return new DeleteLogReturn { ReturnMsg = Messages.Success, ReturnCode = 200 };
                    }
                    catch (Exception)
                    {
                        transactionContext.Rollback();
                        return new DeleteLogReturn { ReturnMsg = Messages.ErrorDeletingRecord, ReturnCode = 401 };
                    }
                }
            }
        }

        public FileUploadReturn UploadFile(int userId)
        {
            var filename = $"{Guid.NewGuid()}.jpeg";
            var result = new FileUploadReturn()
            {
                FileName = filename,
                UserId = userId,
                ReturnCode = 400,
                ReturnMsg = "File upload failed",
                Url = string.Empty
            };
            try
            {
                if (HttpContext.Current.Request.Files.Count == 0)
                    throw new InvalidOperationException("no files been received.");
                var image = HttpContext.Current.Request.Files[0].InputStream;
                var path = HostingEnvironment.MapPath("~/FileServer/");
                if (!string.IsNullOrEmpty(path))
                {
                    var filePath = Path.Combine(path, filename);
                    using (var writer = new FileStream(filePath, FileMode.Create))
                    {
                        int readCount;
                        var buffer = new byte[8192];
                        while ((readCount = image.Read(buffer, 0, buffer.Length)) != 0)
                        {
                            writer.Write(buffer, 0, readCount);
                        }
                    }
                }
                if (userId == 0)
                {
                    userId = Convert.ToInt32(HttpContext.Current.Request.Files[0].FileName.Split('.')[0]);
                }
                var user = _context.Users.FirstOrDefault(l => l.Id == userId);
                if (user != null) user.ImageUrl = VirtualPathUtility.ToAbsolute($"~/FileServer/{filename}");
                _context.SaveChanges();
                result.ReturnCode = 200;
                result.ReturnMsg = "File Upload Success";
                result.Url = VirtualPathUtility.ToAbsolute($"~/FileServer/{filename}");
                result.UserId = userId;
                return result;
            }
            catch (Exception exception)
            {
                Helper.LogError(exception);
                return result;
            }
        }

        public LogReturn LogVisits(int salesRepId, float lat, float longi, string cusAddress, string cusState,
            string cusCity, string cusZip,
            string cusAppointmentDate, string cusAppointmentTime, string cusFirstName, string cusLastName,
            string cusMobile,
            int visitStatusId, int rank, string notes, string cusEmailId, int logId, string cusSubsription, float dlat,
            float dlongi, bool isSendInfo, bool isContract)
        {
            var allAddressUserMapping = _context.AddressUserMappings;
            var allLeads =_context.Leads;
            //Initialize default return
            var logreturn = new BaseReturnWithLogId
            {
                ReturnMsg = "Error saving data",
                ReturnCode = 401,
                LogId = string.Empty
            };
            var mapping = logId != 0
                ? allAddressUserMapping.FirstOrDefault(a => a.AddressUserMappingId == logId & a.IsActive)
                : null;
            //Initialize default return
            //Case 1: New entry
            if (logId == 0)
            {
                //Check Email Already Exist
                if (allLeads.Any(l => l.PrimaryEmail == cusEmailId || l.SecondaryEmail == cusEmailId) &&
                    !string.IsNullOrEmpty(cusEmailId))
                    return new LogReturn { ReturnCode = 401, ReturnMsg = "Please enter different email." };                
                    //Check Address Already Exist
                    if (_context.Address.Any(a => a.Longitude.Equals(longi) & a.Lattitude.Equals(lat) & a.IsActive))
                        return new LogReturn { ReturnCode = 401, ReturnMsg = "Address already present" };
                    //SaveLog here
                    BaseReturnWithLogId logVisits;
                    var saveresult = SaveLog(salesRepId, lat, longi, cusAddress, cusCity, cusZip, cusFirstName,
                        cusLastName, cusMobile, visitStatusId, rank, notes, cusEmailId, cusState,
                        logreturn, out logVisits, dlat, dlongi, LeadAddressSources.D2D, isSendInfo, cusSubsription,
                        cusAppointmentDate, cusAppointmentTime);
                    //Get Saved Lead
                    var loggedId = Convert.ToInt32(logVisits.LogId);
                    var currentMapping =
                        allAddressUserMapping.FirstOrDefault(m => m.AddressUserMappingId == loggedId);
                    var lead =
                       allLeads.FirstOrDefault(
                            l =>
                                l.ServiceAddressId == currentMapping.AddressId ||
                                l.BillingAddressId == currentMapping.AddressId);
                    if (!saveresult)
                        return new LogReturn
                        {
                            ReturnMsg = logreturn.ReturnMsg,
                            ReturnCode = logreturn.ReturnCode,
                            LogId = logVisits.LogId
                        };
                    if (!isSendInfo)
                        return new LogReturn
                        {
                            ReturnMsg = logreturn.ReturnMsg,
                            ReturnCode = logreturn.ReturnCode,
                            LogId = logreturn.LogId
                        };
                if (!string.IsNullOrEmpty(cusEmailId))
                {
                    _methods.SendInfo(lead);
                    SetLeadStatus(lead);
                }
                else
                    return new LogReturn
                    {
                        ReturnCode = 401,
                        ReturnMsg = "Please enter a valid email",
                        LogId = logVisits.LogId
                    };
                    return new LogReturn
                    {
                        ReturnMsg = logreturn.ReturnMsg,
                        ReturnCode = logreturn.ReturnCode,
                        LogId = logreturn.LogId
                    };                
            }
            //Case 2:
            //Get AddressMapping
            // var mapping = allAddressUserMapping.FirstOrDefault(a => a.AddressUserMappingId == logId);
            //Get Lead Info
            var exsitingLead =
               allLeads.FirstOrDefault(
                    l => l.ServiceAddressId == mapping.AddressId || l.BillingAddressId == mapping.AddressId);
            //Check Email Already Exist
            if (allLeads.Any(
                l =>
                    (l.PrimaryEmail == cusEmailId || l.SecondaryEmail == cusEmailId) &&
                    l.LeadId != exsitingLead.LeadId) && !string.IsNullOrEmpty(cusEmailId))
                return new LogReturn { ReturnCode = 401, ReturnMsg = "Please enter different email." };
            UpdateLog(salesRepId, lat, longi, cusAddress, cusCity, cusZip, cusFirstName, cusLastName, cusMobile,
                visitStatusId, rank, notes, cusEmailId, logId, cusState, dlat, dlongi, isSendInfo,
                cusSubsription, cusAppointmentTime, cusAppointmentDate);
            if (!isSendInfo)
                return new LogReturn
                {
                    ReturnCode = 200,
                    ReturnMsg = "Updated successfully",
                    LogId = logId.ToString()
                };
            if (!string.IsNullOrEmpty(cusEmailId))
            {
                _methods.SendInfo(exsitingLead);
                SetLeadStatus(exsitingLead);
            }
            else
                return new LogReturn
                {
                    ReturnCode = 401,
                    ReturnMsg = "Please enter a valid email",
                    LogId = logId.ToString()
                };
            return new LogReturn { ReturnCode = 200, ReturnMsg = "Updated successfully", LogId = logId.ToString() };
        }

        public ContractReturn GenerateContract(int logId, bool isContract, int salesRepId, float lat, float longi, string cusAddress, string cusCounty, string cusState, string cusCity, string cusZip, string cusFirstName, string cusLastName, string cusMobile, int visitStatusId, int rank, string notes, string cusEmailId, string cusDob, string cusUtilityAccountNo, string cusSubsription, float dlat, float dlongi)
        {
            var rep = _context.Users.FirstOrDefault(u => u.Id == salesRepId & u.IsActive);
            if (rep == null) return new ContractReturn { ReturnMsg = Messages.FailureNoRepFound, ReturnCode = 401 };
            var contractReturn = new BaseReturnWithLogId() { ReturnMsg = "Error saving data", ReturnCode = 401, LogId = string.Empty };
            if (salesRepId == 0 || lat.Equals(0) || longi.Equals(0) || visitStatusId == 0)
                return new ContractReturn { ReturnMsg = Messages.ErrorProvideRequiredValues, ReturnCode = 401 };
            if (logId != 0)
            {
                UpdateLog(salesRepId, lat, longi, cusAddress, cusCity, cusZip, cusFirstName, cusLastName, cusMobile, visitStatusId, rank, notes, cusEmailId, logId, cusState, dlat, dlongi, false, cusSubsription, null, null, cusDob, cusUtilityAccountNo, cusCounty, true);
            }
            else
            {
                if (_context.Leads.Any(l => l.PrimaryEmail == cusEmailId || l.SecondaryEmail == cusEmailId) &&
                    !string.IsNullOrEmpty(cusEmailId))
                    return new ContractReturn { ReturnCode = 401, ReturnMsg = "Please enter different email." };
                if (_context.Address.Any(a => a.Longitude.Equals(longi) & a.Lattitude.Equals(lat) & a.IsActive))
                    return new ContractReturn { ReturnCode = 401, ReturnMsg = "Address Already Present" };
                BaseReturnWithLogId logVisits;
                contractReturn = SaveLog(salesRepId, lat, longi, cusAddress, cusCity, cusZip, cusFirstName, cusLastName, cusMobile, visitStatusId, rank, notes, cusEmailId, cusState,
                    contractReturn, out logVisits, dlat, dlongi, LeadAddressSources.D2D, false, cusSubsription, null, null, cusDob, cusUtilityAccountNo, cusCounty)
                    ? new ContractReturn { ReturnMsg = logVisits.ReturnMsg, ReturnCode = logVisits.ReturnCode, LogId = logVisits.LogId } : new ContractReturn { ReturnMsg = contractReturn.ReturnMsg, ReturnCode = contractReturn.ReturnCode, LogId = contractReturn.LogId };
                logId = string.IsNullOrEmpty(contractReturn.LogId) ? 0 : Convert.ToInt32(contractReturn.LogId);
            }
            if (logId == 0) return new ContractReturn { ReturnMsg = "Error Saving data/ No data Found", ReturnCode = 401, LogId = "0" };
            var mapping = rep.AddressUserMappings.FirstOrDefault(a => a.AddressUserMappingId == logId & a.IsActive);
            if (mapping == null) return new ContractReturn { ReturnMsg = "Contract already sent / signed", ReturnCode = 401 };
            if (_context.Leads.Any(l => l.IsActive & l.PrimaryEmail == cusEmailId & l.TrancientId != null))
                return new ContractReturn { ReturnMsg = "Contract already sent / signed", ReturnCode = 401 };
            var userLead = rep.Leads.FirstOrDefault(
                s => s.ServiceAddressId == mapping.AddressId & s.IsActive & s.PrimaryEmail == cusEmailId & string.IsNullOrEmpty(s.TrancientId));
            if (userLead == null) return new ContractReturn { ReturnMsg = "Something went wrong.", ReturnCode = 401 };
            var userAddress = userLead.LeadServiceAddress;
            return isContract ? _methods.ContractReturn(userLead, userAddress, rep, mapping,true) : _methods.SendInformation(userLead);
        }
        
        private void SetLeadStatus(Lead lead)
        {
            var currentLeadStatus = lead.LeadLeadStatusMappings.FirstOrDefault(l => l.IsCurrent);
            if (currentLeadStatus != null)
            {
                currentLeadStatus.IsCurrent = false;
            }
            lead.LeadLeadStatusMappings.Add(new LeadLeadStatusMapping()
            {
                LeadStatusId = LeadContractStatus.CotractInformationSent,
                LeadId = lead.LeadId,
                IsCurrent = true,
                UpdatedDate = DateTime.Now
            });
            _context.SaveChanges();
        }

        private bool SetPasswordResetTemplateAndEmail(string emailId, AspNetUsers user, out PasswordReturn forgotPassword)
        {
            var filePath = HostingEnvironment.MapPath("~/FileServer/Upload/PasswordResetTemplate.html");
            if (filePath == null)
            {
                forgotPassword = _passwordReturn;
                return true;
            }
            var mailTemplate = File.ReadAllText(filePath)
                .Replace("{0}", user.Email)
                .Replace("{1}", user.PasswordHash); //TODO: if encryption implemented then, we must decrypt here and send email
            if (Methods.SendEmail(emailId, Convert.ToString(mailTemplate), Messages.EmailSubjectPasswordReset,
                true))
            {
                _passwordReturn.ReturnCode = 200;
                _passwordReturn.ReturnMsg = Messages.PasswordResetSuccess;
                forgotPassword = _passwordReturn;
            }
            else
            {
                _passwordReturn.ReturnCode = 401;
                _passwordReturn.ReturnMsg = Messages.MailSendingError;
                forgotPassword = _passwordReturn;
            }
            return false;
        }
       
        private List<UserAnalysis> GetWeeklyData(DateTime start, DateTime end)
        {
            var userAnalysis = new List<UserAnalysis>();
            foreach (var user in _context.Users.Where(u => u.UserTypeId == SalesUserTypes.D2D & u.IsActive))
            {
                var userLeads = user.AddressUserMappings.Where(a => a.UserId == user.Id & a.IsActive).ToList();
                var count =
                    userLeads.Select(
                        userAddress =>
                            user.Leads.FirstOrDefault(
                                l =>
                                    l.IsActive &
                                    (l.ServiceAddressId == userAddress.AddressId ||
                                     l.BillingAddressId == userAddress.AddressId)))
                        .Where(userLead => userLead != null)
                        .Count(
                            userLead =>
                                userLead.LeadLeadStatusMappings.Where(
                                    lm => lm.IsCurrent & lm.LeadStatusId == LeadContractStatus.ContractSigned)
                                    .ToList()
                                    .Any(a => a.DateModified.TimeBetween(start, end)));
                userAnalysis.Add(new UserAnalysis { UserName = $"{user.FirstName}", Knocks = count });
            }
            return userAnalysis;
        }

        private static ICollection<LeadInfo> MapLeads(IEnumerable<Lead> userLeads, AspNetUsers rep)
        {
            return (from lead in userLeads
                    let leadAddress = lead.LeadServiceAddress ?? lead.LeadBillingAddress
                    let leadRank = lead.LeadRanking
                    let leadLeadStatusMapping = lead.LeadLeadStatusMappings.FirstOrDefault(l => l.IsCurrent)
                    where leadLeadStatusMapping != null
                    let leadContractStatus = leadLeadStatusMapping.LeadStatus
                    let userAddressMapping = rep.AddressUserMappings.FirstOrDefault(a => a.AddressId == leadAddress.AddressId & a.IsActive)
                    let visitStatus = userAddressMapping?.VisitStatus
                    select new LeadInfo
                    {
                        Address = string.IsNullOrEmpty(leadAddress.AddressInformation) ? string.Empty : leadAddress.AddressInformation,
                        FirstName = string.IsNullOrEmpty(lead.FirstName) ? string.Empty : lead.FirstName,
                        LastName = string.IsNullOrEmpty(lead.LastName) ? string.Empty : lead.LastName,
                        Email = string.IsNullOrEmpty(lead.PrimaryEmail) ? string.Empty : lead.PrimaryEmail,
                        City = string.IsNullOrEmpty(leadAddress.City) ? string.Empty : leadAddress.City,
                        Lattitude = Convert.ToString(leadAddress.Lattitude, CultureInfo.InvariantCulture),
                        Longitude = Convert.ToString(leadAddress.Longitude, CultureInfo.InvariantCulture),
                        Mobile = string.IsNullOrEmpty(lead.MobileNo) ? string.Empty : lead.WorkPhoneNo,
                        State = string.IsNullOrEmpty(leadAddress.State) ? string.Empty : leadAddress.State,
                        Zip = string.IsNullOrEmpty(leadAddress.Zipcode) ? string.Empty : leadAddress.Zipcode,
                        AppointmentDate = (userAddressMapping != null) ? userAddressMapping.AppointmentDate?.ToString("MM/dd/yyyy") ?? string.Empty : string.Empty,
                        Notes = (userAddressMapping != null) ? string.IsNullOrEmpty(userAddressMapping.Comments) ? string.Empty : userAddressMapping.Comments : string.Empty,
                        VisitStatus = visitStatus != null ? Convert.ToString(visitStatus.VisitName) : string.Empty,
                        LogId = (userAddressMapping != null) ? Convert.ToString(userAddressMapping.AddressUserMappingId) : string.Empty,
                        AppointmentTime = (userAddressMapping != null) ? userAddressMapping.AppointmentTime.CompareTo(TimeSpan.Zero) != 0 ? Convert.ToString(userAddressMapping.AppointmentTime) : string.Empty : string.Empty,
                        Rank = leadRank != null ? string.IsNullOrEmpty(leadRank.RankingName) ? string.Empty : leadRank.RankingName : string.Empty,
                        LeadContractStatus = leadContractStatus != null ? leadContractStatus.StatusName : string.Empty,
                        ContractStatusUpdateDate = leadLeadStatusMapping?.UpdatedDate.ToString("MM/dd/yyyy") ?? string.Empty,
                        ContractStatusUpdateTime = leadLeadStatusMapping?.UpdatedDate.ToString("HH:mm:ss") ?? string.Empty,
                        DateVisited = (userAddressMapping != null) ? userAddressMapping.DateVisited?.ToString("MM/dd/yyyy") ?? string.Empty : string.Empty,
                        EnvelopeId = string.IsNullOrEmpty(lead.EnvelopeId) ? string.Empty : lead.EnvelopeId,
                        TranscientId = !string.IsNullOrEmpty(lead.TrancientId) ? lead.TrancientId : string.Empty,
                       
                    }).ToList();
        }

        private
           bool SaveLog(int salesRepId, float lat, float longi, string cusAddress, string cusCity, string cusZip, string cusFirstName, string cusLastName, string cusMobile,
           int visitStatusId, int rank, string notes, string cusEmailId, string cusState, BaseReturnWithLogId logreturn, out BaseReturnWithLogId logVisits, float dlat, float dlongi, int addressSourceId, bool isSendInfo, string cusSubsription = null, string cusAppointmentDate = null, string cusAppointmentTime = null, string cusDob = null, string cusUtilityAccountNo = null, string cusCounty = null)
        {
            var rep = _context.Users.FirstOrDefault(u => u.IsActive && u.Id == salesRepId);
            var name = $"{rep.FirstName} {rep.LastName}";
            if (rep != null)
            {
                var lead = _context.Leads.Add(new Lead
                {
                    FirstName = cusFirstName,
                    LastName = cusLastName,
                    WorkPhoneNo = cusMobile,
                    AspNetUsersId = salesRepId,
                    PrimaryEmail = cusEmailId,
                    CampaignId = 2,
                    //CreatedDate = DateTime.Today,
                    //LeadStatusId = LeadContractStatus.LeadCreated,
                    IsActive = true,
                    LeadSourceId = 1,
                    ChannelId = LeadChannels.D2D,
                    UserCreated=name
                });
                if (rank != 0)
                    lead.LeadRankingId = rank;
                if (!string.IsNullOrEmpty(cusDob))
                    lead.DateOfBirth = cusDob;
                if (!string.IsNullOrEmpty(cusUtilityAccountNo))
                    lead.UtilityAccountNo = cusUtilityAccountNo;
                if (!string.IsNullOrEmpty(cusSubsription))
                    lead.SubscriptionPercentage = Convert.ToInt32(cusSubsription);
                lead.LeadServiceAddress = new Address
                {
                    AddressInformation = cusAddress,
                    City = cusCity,
                    Zipcode = cusZip,
                    State = cusState ?? string.Empty,
                    Longitude = longi,
                    Lattitude = lat,
                    AddressSourceId = addressSourceId, //LeadAddressSources.D2D,
                    County = cusCounty ?? string.Empty,
                    IsActive = true,
                    DeviceLong = dlongi,
                    DeviceLat = dlat,
                    UserCreated=name
                };
                var currentLeadStatus = lead.LeadLeadStatusMappings.FirstOrDefault(l => l.IsCurrent);
                if (currentLeadStatus != null)
                {
                    currentLeadStatus.IsCurrent = false;
                }
                lead.LeadLeadStatusMappings.Add(new LeadLeadStatusMapping()
                {
                    LeadStatusId = LeadContractStatus.LeadCreated,
                    LeadId = lead.LeadId,
                    IsCurrent = true,
                    UpdatedDate = DateTime.Now,
                    UserCreated=name
                });
                _context.SaveChanges();
                lead.LeadRep.AddressUserMappings.Add(new AddressUserMapping()
                {
                    AddressId = lead.ServiceAddressId,
                    VisitStatusId = visitStatusId,
                    AppointmentDate =
                        !string.IsNullOrEmpty(cusAppointmentDate)
                            ? Convert.ToDateTime(cusAppointmentDate)
                            : (DateTime?)null,
                    AppointmentTime =
                        !string.IsNullOrEmpty(cusAppointmentTime) ? TimeSpan.Parse(cusAppointmentTime) : TimeSpan.Zero,
                    DateVisited = DateTime.Today,
                    IsActive = true,
                    UserId = salesRepId,
                    Comments = notes,
                    UserCreated=name
                });
                _context.SaveChanges();
                if (visitStatusId == LeadVisitStatus.Appointment & !isSendInfo)
                {
                    var filePath = HostingEnvironment.MapPath("~/FileServer/Upload/SendAppoinmentTemplate.html");
                    if (filePath != null)
                    {
                        var mailTemplate =
                            File.ReadAllText(filePath)
                                .Replace("{0}", lead.FirstName).Replace("{3}", $"{rep.FirstName} {rep.LastName}")
                                .Replace("{4}", rep.Email).Replace("{5}", rep.PhoneNumber.ToString())
                                .Replace("{1}", lead.LastName)
                                .Replace("{2}",
                                    $"{Convert.ToDateTime(cusAppointmentDate).Add(TimeSpan.Parse(cusAppointmentTime)):MMM dd, yyyy}")
                                .Replace("{7}", $"{rep.FirstName}")
                                .Replace("{6}",
                                    String.Format("{0:HH:mm tt}",
                                        Convert.ToDateTime(cusAppointmentDate).Add(TimeSpan.Parse(cusAppointmentTime)))
                                        .Replace("AM", "").Replace("PM", ""));
                        Methods.SendEmail(lead.PrimaryEmail, mailTemplate, Messages.EmailAppointmentSubject, true);
                    }
                }

                var addressUserMapping = rep.AddressUserMappings.FirstOrDefault(m => m.AddressId == lead.ServiceAddressId);
                if (addressUserMapping != null)
                {
                    logreturn.LogId =
                        Convert.ToString(addressUserMapping.AddressUserMappingId);
                    logreturn.ReturnMsg = Messages.Success;
                    logreturn.ReturnCode = 200;
                    logVisits = logreturn;
                    return true;
                }
            }
            logreturn.ReturnMsg = Messages.FailureNoRepFound;
            logreturn.LogId = "0";
            logreturn.ReturnCode = 400;
            logVisits = logreturn;
            return false;
        }
        private void UpdateLog(int salesRepId, float lat, float longi, string cusAddress, string cusCity, string cusZip, string cusFirstName, string cusLastName, string cusMobile, int visitStatusId, int rank, string notes, string cusEmailId, int logId, string cusState, float dlat, float dlongi, bool isSendInfo, string cusSubsription = null, string cusAppointmentTime = null, string cusAppointmentDate = null, string cusDob = null, string cusUtilityAccountNo = null, string cusCounty = null, bool isContract = false)
        {
            var rep = _context.Users.FirstOrDefault(u => u.IsActive && u.Id == salesRepId);
            if (rep == null) return;
            var name = $"{rep.FirstName} {rep.LastName}";
            var allRepLead = rep.Leads;
            var mapping = rep.AddressUserMappings.FirstOrDefault(a => a.AddressUserMappingId == logId & a.IsActive);
            var userAddress = mapping?.Address;
            if (userAddress == null) return;
            var isModified = !allRepLead.Any(
                s => (s.FirstName == cusFirstName && s.LastName == cusLastName && s.PrimaryEmail == cusEmailId));
            var userLead = allRepLead.FirstOrDefault(
                s => (s.ServiceAddressId == userAddress.AddressId || s.BillingAddressId == userAddress.AddressId) & s.IsActive);
            if (userLead == null) return;
            var allLeadRepAddressMapping = userLead.LeadRep.AddressUserMappings;
            if (!isModified & !isContract)
                isModified = !allLeadRepAddressMapping.Any(a => a.IsActive & a.AddressId == userLead.ServiceAddressId & (!a.AppointmentDate.HasValue || (a.AppointmentDate.Value.CompareTo(Convert.ToDateTime(cusAppointmentDate)) == 0)) & a.AppointmentTime.CompareTo(string.IsNullOrEmpty(cusAppointmentTime) ? TimeSpan.Zero : TimeSpan.Parse(cusAppointmentTime)) == 0);
            SetLeadInformation(salesRepId, cusFirstName, cusLastName, cusMobile, rank, cusEmailId, cusSubsription, userLead, cusDob, cusUtilityAccountNo,name);
            SetAddressInformation(lat, longi, cusAddress, cusCity, cusZip, cusState, dlat, dlongi, userLead, cusCounty,name);
            SetAddressUserMappingInfo(salesRepId, visitStatusId, notes, allLeadRepAddressMapping, userLead, cusAppointmentTime, cusAppointmentDate,name);
            _context.SaveChanges();
            CheckForAppointmentAndProceed(visitStatusId, isSendInfo, isModified, userLead, rep, cusAppointmentTime, cusAppointmentDate, isContract);
        }

        private static void CheckForAppointmentAndProceed(int visitStatusId, bool isSendInfo, bool isModified, Lead userLead,
            AspNetUsers rep, string cusAppointmentTime, string cusAppointmentDate, bool isContract)
        {
            if (!(visitStatusId == LeadVisitStatus.Appointment & !isSendInfo & isModified & !isContract)) return;
            var filePath = HostingEnvironment.MapPath("~/FileServer/Upload/SendAppoinmentTemplate.html");
            if (filePath == null) return;
            var mailTemplate =
                File.ReadAllText(filePath)
                    .Replace("{0}", userLead.FirstName).Replace("{3}", $"{rep.FirstName} {rep.LastName}")
                    .Replace("{4}", rep.Email).Replace("{5}", rep.PhoneNumber.ToString())
                    .Replace("{1}", userLead.LastName)
                    .Replace("{2}",
                        $"{Convert.ToDateTime(cusAppointmentDate).Add(TimeSpan.Parse(cusAppointmentTime)):MMM dd, yyyy}")
                    .Replace("{7}", $"{rep.FirstName}")
                    .Replace("{6}",
                        $"{Convert.ToDateTime(cusAppointmentDate).Add(TimeSpan.Parse(cusAppointmentTime)):HH:mm tt}"
                            .Replace("AM", "").Replace("PM", ""));
            Methods.SendEmail(userLead.PrimaryEmail, mailTemplate, Messages.EmailAppointmentSubject, true);
        }

        private static void SetAddressUserMappingInfo(int salesRepId, int visitStatusId, string notes,
            IEnumerable<AddressUserMapping> allLeadRepAddressMapping, Lead userLead, string cusAppointmentTime, string cusAppointmentDate,string name)
        {
            var addressUserMapping = allLeadRepAddressMapping.FirstOrDefault(a => a.IsActive);
            if (addressUserMapping == null) return;
            addressUserMapping.AddressId = userLead.ServiceAddressId;
            addressUserMapping.VisitStatusId = visitStatusId;
            addressUserMapping.AppointmentDate =
                !string.IsNullOrEmpty(cusAppointmentDate)
                    ? Convert.ToDateTime(cusAppointmentDate)
                    : (DateTime?) null;
            addressUserMapping.AppointmentTime =
                !string.IsNullOrEmpty(cusAppointmentTime) ? TimeSpan.Parse(cusAppointmentTime) : TimeSpan.Zero;
            addressUserMapping.DateVisited = DateTime.Today;
            addressUserMapping.IsActive = true;
            addressUserMapping.UserId = salesRepId;
            addressUserMapping.Comments = notes;
            addressUserMapping.UserModified = name;
        }

        private static void SetAddressInformation(float lat, float longi, string cusAddress, string cusCity, string cusZip,
            string cusState, float dlat, float dlongi, Lead userLead, string cusCounty,string name)
        {
            var address = userLead.LeadServiceAddress;
            address.AddressInformation = cusAddress;
            address.City = cusCity;
            address.Zipcode = cusZip;
            address.State = cusState ?? string.Empty;
            address.Longitude = longi;
            address.Lattitude = lat;
            address.County = cusCounty ?? string.Empty;
            address.IsActive = true;
            address.DeviceLong = dlongi;
            address.DeviceLat = dlat;
            address.UserModified = name;
        }

        private static void SetLeadInformation(int salesRepId, string cusFirstName, string cusLastName, string cusMobile,
            int rank, string cusEmailId, string cusSubsription, Lead userLead, string cusDob, string cusUtilityAccountNo,string name)
        {
            userLead.FirstName = cusFirstName;
            userLead.LastName = cusLastName;
            userLead.MobileNo = cusMobile;
            userLead.AspNetUsersId = salesRepId;
            userLead.PrimaryEmail = cusEmailId;
            userLead.CampaignId = 2;
            userLead.IsActive = true;
            userLead.LeadSourceId = 1;
            userLead.ChannelId = LeadChannels.D2D;
            userLead.UserModified = name;
            if (rank != 0)
                userLead.LeadRankingId = rank;
            if (!string.IsNullOrEmpty(cusDob))
                userLead.DateOfBirth = cusDob;
            if (!string.IsNullOrEmpty(cusUtilityAccountNo))
                userLead.UtilityAccountNo = cusUtilityAccountNo;
            if (!string.IsNullOrEmpty(cusSubsription))
                userLead.SubscriptionPercentage = Convert.ToInt32(cusSubsription);
        }
    }
}
