﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class Address : BaseEntity
    {
        public Address()
        {
            
        }
        public int AddressId { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string AppartmentNo { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(400)]
        public string AddressInformation { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string State { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string City { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(25)]
        public string Zipcode { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string County { get; set; }
        public float Lattitude { get; set; }
        public float Longitude { get; set; }
        public int AddressSourceId { get; set; }
        public bool IsActive { get; set; }
        public float DeviceLat { get; set; }
        public float DeviceLong { get; set; }
    }
}