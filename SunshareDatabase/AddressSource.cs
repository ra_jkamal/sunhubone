﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class AddressSource : BaseEntity
    {
        public AddressSource()
        {
            Address=new List<Address>();
        }
        public int AddressSourceId { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string AddressSourceName { get; set; }
        protected virtual ICollection<Address> Address { get; set; }
    }
}