﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class AddressUserMapping : BaseEntity
    {
        public AddressUserMapping()
        {
        }
        public int AddressUserMappingId { get; set; }
        public int AddressId { get; set; }
        public int? UserId { get; set; }
        public DateTime? DateToVisit { get; set; }
        public DateTime? DateVisited { get; set; }
        public int VisitStatusId { get; set; }
        public DateTime? AppointmentDate { get; set; }
        public TimeSpan AppointmentTime { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(250)]
        public string Comments { get; set; }
        public bool IsActive { get; set; }
        [ForeignKey("AddressId")]
        public virtual Address Address { get; set; }
        [ForeignKey("UserId")]
        public virtual AspNetUsers User { get; set; }
        [ForeignKey("VisitStatusId")]
        public virtual VisitStatus VisitStatus { get; set; }
    }
}