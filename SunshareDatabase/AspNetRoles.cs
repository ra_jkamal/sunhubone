﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class AspNetRoles : BaseEntity
    {
        public AspNetRoles()
        {
            UserInRoles=new List<AspNetUserRoles>();
        }
        public int Id { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(100)]
        public string Name { get; set; }

        private ICollection<AspNetUserRoles> UserInRoles { get; set; }
    }
}
