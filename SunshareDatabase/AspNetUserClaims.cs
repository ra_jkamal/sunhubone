﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class AspNetUserClaims : BaseEntity
    {
        public AspNetUserClaims()
        { }
        public int Id { get; set; }
        public int UserId { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string ClaimType { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string ClaimValue { get; set; }
        public AspNetUsers User { get; set; }
    }
}
