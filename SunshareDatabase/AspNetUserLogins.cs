﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class AspNetUserLogins : BaseEntity
    {
        public AspNetUserLogins()
        { }
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string LoginProvider { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string ProviderKey { get; set; }
        [Key]
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual AspNetUsers User { get; set; }
    }
}
