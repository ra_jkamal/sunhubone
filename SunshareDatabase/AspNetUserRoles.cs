﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class AspNetUserRoles : BaseEntity
    {
        [Key,Column(Order =0)]
        [Required]
        public int UserId { get; set; }
        [Key,Column(Order =1)]
        [Required]
        public int RoleId { get; set; }
        [ForeignKey("RoleId")]
        public AspNetRoles UserRole { get; set; }
        [ForeignKey("UserId")]
        public AspNetUsers User { get; set; }
    }
}
