﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class AspNetUsers : BaseEntity
    {
        public AspNetUsers()
        {
            AssignHistory=new List<LeadUserMappingHistory>();
            Leads=new List<Lead>();
            UserRoles=new List<AspNetUserRoles>();
            AddressUserMappings=new List<AddressUserMapping>();
        }
        [Key]
        public int Id { get; set; }
        public int? UserParentId { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public long? PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public DateTime? LockoutEndDateUtc { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PartnerId { get; set; }
        public int UserTypeId { get; set; }
        public bool IsActive { get; set; }
        public string ImageUrl { get; set; }
        public string DeviceName { get; set; }
        public string DeviceSerialNo { get; set;}
        //public UserType UserType { get; set; }
        public Partner Partner { get; set; }
        private ICollection<AspNetUserRoles> UserRoles { get; set; }
        [ForeignKey("UserParentId")]
        public AspNetUsers User { get; set; }
        public virtual ICollection<Lead> Leads { get; set; }
        [ForeignKey("UserTypeId")]
        public virtual UserType Type { get; set; }

        protected virtual ICollection<LeadUserMappingHistory> AssignHistory { get; set; }

        public virtual ICollection<AddressUserMapping> AddressUserMappings { get; set; }
    }
}
