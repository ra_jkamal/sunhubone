﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class Audit : BaseEntity
    {
        public Audit()
        {
        }
        public enum AuditActions {Insert=1,Update,Delete}
        public int AuditId { get; set; }
        public DateTime RivisionStamp { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(100)]
        public string TableName { get; set; }
        public AuditActions Actions { get; set; }
        [Column(TypeName = "xml")]
        public string OldData { get; set; }
        [Column(TypeName = "xml")]
        public string NewData { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(400)]
        public string ChangedColumns { get; set; }
    }
}