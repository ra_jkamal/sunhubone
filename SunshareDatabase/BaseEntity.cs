﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class BaseEntity
    {
        public BaseEntity()
        {
        }
        public DateTime? DateCreated { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(100)]
        public string UserCreated { get; set; }
        public DateTime? DateModified { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(100)]
        public string UserModified { get; set; }
    }
}
