﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class CallAppointment : BaseEntity
    {
        public CallAppointment()
        { }
        [Key]
        public int CallAppointmentId { get; set; }
        public int CallLogId { get; set; }
        public DateTime DateToCall { get; set; }
        public DateTime? CallDate { get; set; }
        [ForeignKey("CallLogId")]
        public virtual CallLog CallLog { get; set; }
    }
}
