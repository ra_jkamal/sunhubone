﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class CallLog : BaseEntity
    {
        public CallLog()
        {
        }
        [Key]
        public int CallLogId { get; set; }
        public int? CallAppointmentId { get; set; }
        public int LeadId { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(400)]
        public string CallComment { get; set; }
        public bool IsActive { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(100)]
        public string CreatedBy { get; set; }
        public bool? IsDuplicateCall { get; set; }
        public int CallStatusId { get; set; }
        public virtual Lead Lead { get; set; }
        public string BoundStatusId { get; set; }
        //public virtual CallLog Call { get; set; }
        [ForeignKey("CallAppointmentId")]
        public virtual CallAppointment Appointment { get; set; }
        public virtual CallStatus CallStatus { get; set; }
    }
}