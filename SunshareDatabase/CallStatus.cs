﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class CallStatus : BaseEntity
    {
        public CallStatus()
        {
            CallLogs=new List<CallLog>();
        }
        public int CallStatusId { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(70)]
        public string CallStatusName { get; set; }
        public bool IsActive { get; set; }
        private ICollection<CallLog> CallLogs { get; set; }
    }
}
