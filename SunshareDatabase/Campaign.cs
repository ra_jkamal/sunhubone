﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class Campaign : BaseEntity
    {
        public Campaign()
        {
            //Channel=new Channel();
            LeadSources=new List<LeadSource>();
        }
        public int CampaignId { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(200)]
        public string CampaignName { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string AmountSpent { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(250)]
        public string Description { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(150)]
        public string TypeOfCampaign { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string Incentive { get; set; }
        public bool IsActive { get; set; }
        //public int ChannelId { get; set; }
        //public virtual ICollection<Lead> Leads { get; set; }
        //public virtual Channel Channel { get; set; }
        public virtual ICollection<LeadSource> LeadSources { get; set; } 
    }
}