﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class Channel : BaseEntity
    {
        public Channel()
        {
            //Campaigns=new List<Campaign>();
        }
        [Key]
        public int ChannelId { get; set; }
        public int? ParentChannelId { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string ChannelName { get; set; }
        public bool IsActive { get; set; }
        public bool ShowOnAdd { get; set; }
        public virtual ICollection<Lead> Leads { get; set; }

        //protected virtual ICollection<Campaign> Campaigns { get; set; }
    }
}