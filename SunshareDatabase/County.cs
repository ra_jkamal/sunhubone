﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class County : BaseEntity
    {
        public County()
        {
            Leads=new List<Lead>();
        }
        public int CountyId { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(100)]
        public string CountyName { get; set; }
        public bool IsActive { get; set; }
        public int StateId { get; set; }
        [ForeignKey("StateId")]
        public virtual State CountyState { get; set; }

        protected virtual ICollection<Lead> Leads { get; set; }
    }
}
