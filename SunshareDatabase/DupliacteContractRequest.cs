﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System;

namespace SunshareDatabase
{
    public class DupliacteContractRequest : BaseEntity
    {
        public DupliacteContractRequest()
        {
            
        }
        public int DupliacteContractRequestId { get; set; }
        public int LeadId { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(250)]
        public string Reason { get; set; }
        public bool IsDecided { get; set; }
        public bool IsApproved { get; set; }
        [ForeignKey("LeadId")]
        public virtual Lead Lead { get; set; }
        public int ReqRepId { get; set; }
        public int ApproveId { get; set; }
    }
}
