﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunshareDatabase
{
    public class LandingPage : BaseEntity
    {
        public int LandingPageId { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(400)]
        public string LandingPageName { get; set; }
        public bool IsActive { get; set; }

        public virtual ICollection<Lead> Leads { get; set; }
    }
}
