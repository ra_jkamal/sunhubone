﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class Lead : BaseEntity
    {
  
        public int LeadId { get; set; }
        //public string LeadName { get; set;} 
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string PrimaryEmail { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string SecondaryEmail { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string FirstName { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(100)]
        public string LastName { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
       
        public string DateOfBirth { get; set; }
        [Column(TypeName = "VARCHAR")]
        [MaxLength]
        //[StringLength(255)]
        public string UtilityAccountNo { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
       
        public string UtilityCompanyName { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(25)]
        public string HomePhoneNo { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(25)]
        public string WorkPhoneNo { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(25)]
        public string MobileNo { get; set; }
        public int ServiceAddressId { get; set; }
        public int BillingAddressId { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(25)]
        public string RefferedBy { get; set; }
        public int SubscriptionPercentage { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(25)]
        public string SSAType { get; set; }
        public int? AspNetUsersId { get; set; }
        public int? ChannelId { get; set; }
        public int CampaignId { get; set; }
        public int LeadRankingId { get; set; }
        public int? LandingPageId { get; set; }
        public int? LeadSourceId { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string EnvelopeId { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(150)]
        public string TrancientId { get; set; }
        public bool IsActive { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(250)]
        public string InActiveReason { get; set; }
        [Column(TypeName ="VARCHAR")]
        [StringLength(250)]
        public string ReasonToClose { get; set; }
        public bool IsAssigned { get; set; }
        public bool AllowDuplicateContract { get; set; }
        public bool IsDuplicateLead { get; set; }
        [ForeignKey("CampaignId")]
        public virtual tbl_m_CampaignDetails_CM LeadCampaign { get; set; }
        [ForeignKey("ChannelId")]
        public virtual Channel LeadChannel { get; set; }
        public virtual State LeadState { get; set; }
        public virtual County LeadCounty { get; set; }
        [ForeignKey("LandingPageId")]
        public virtual LandingPage LandingPage { get; set; }
        public virtual ICollection<CallLog> CallLogs { get; set; } = new List<CallLog>();

        [ForeignKey("ServiceAddressId")]
        public virtual Address LeadServiceAddress { get; set; }
        [ForeignKey("BillingAddressId")]
        public virtual Address LeadBillingAddress { get; set; }
        [ForeignKey("LeadRankingId")]
        public virtual LeadRanking LeadRanking { get; set; }
        public virtual ICollection<LeadLeadStatusMapping> LeadLeadStatusMappings { get; set; } = new List<LeadLeadStatusMapping>();

        [ForeignKey("AspNetUsersId")]
        public virtual AspNetUsers LeadRep { get; set; } 
        public virtual ICollection<LeadUserMappingHistory> AssignHistory { get; set; } = new List<LeadUserMappingHistory>();
        public virtual ICollection<DupliacteContractRequest> DupliacteContractRequests { get; set; } = new List<DupliacteContractRequest>();

        // public virtual LeadStatus LeadStatus { get; set; }
    }
}