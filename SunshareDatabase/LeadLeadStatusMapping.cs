﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
   public class LeadLeadStatusMapping : BaseEntity
    {
       
        [ForeignKey("LeadId")]
        public virtual Lead Lead { get; set; }
        public virtual LeadStatus LeadStatus {get;set;}
        public int LeadLeadStatusMappingId { get; set; }
        public int LeadId { get; set; }
        public int LeadStatusId { get; set; }
        public DateTime UpdatedDate { get; set; }
        public bool IsCurrent { get; set; }
    }
}
