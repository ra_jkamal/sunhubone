﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class LeadSource
    {
        public LeadSource()
        {
            Leads=new List<Lead>();
            Campaigns=new List<Campaign>();
            LeadSourceSubCategories=new List<LeadSourceSubCategory>();
        }
        public int LeadSourceId { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(70)]
        public  string LeadSourceName { get; set; }
        public bool IsActive { get; set; }
        public virtual ICollection<Lead> Leads { get; set; }
        public virtual ICollection<Campaign> Campaigns { get; set; } 
        public virtual ICollection<LeadSourceSubCategory> LeadSourceSubCategories { get; set; } 
    }
}
