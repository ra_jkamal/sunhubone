﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class LeadSourceSubCategory : BaseEntity
    {
        public LeadSourceSubCategory()
        {
            
        }
        public int LeadSourceSubCategoryId { get; set; }
        public int LeadSourceId { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(150)]
        public string LeadSourceSubCategoryName { get; set; }
        public bool IsActive { get; set; }
        [ForeignKey("LeadSourceId")]
        public virtual LeadSource LeadSource { get; set; }
    }
}
