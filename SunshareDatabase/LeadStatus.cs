﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class LeadStatus : BaseEntity
    {
        public LeadStatus()
        {
            Leads=new List<Lead>();
        }
        public int LeadStatusId { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(70)]
        public string StatusName { get; set; }
        public bool IsActive { get; set; }
        protected virtual ICollection<Lead> Leads { get; set; }
    }
}