﻿using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class LeadUserMappingHistory : BaseEntity
    {
        public LeadUserMappingHistory()
        {
        }
        public int LeadUserMappingHistoryId { get; set; }
        public int LeadId { get; set; }
        public int? AspNetUserId { get; set; }
        [ForeignKey("LeadId")]
        public virtual Lead mappedLead { get; set; }
        [ForeignKey("AspNetUserId")]
        public virtual AspNetUsers mappedUser {get;set;}
    }
}
