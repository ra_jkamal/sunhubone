namespace SunshareDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CallLogFix : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.Address",
            //    c => new
            //        {
            //            AddressId = c.Int(nullable: false, identity: true),
            //            AppartmentNo = c.String(maxLength: 50, unicode: false),
            //            AddressInformation = c.String(maxLength: 400, unicode: false),
            //            State = c.String(maxLength: 50, unicode: false),
            //            City = c.String(maxLength: 50, unicode: false),
            //            Zipcode = c.String(maxLength: 25, unicode: false),
            //            County = c.String(maxLength: 50, unicode: false),
            //            Lattitude = c.Single(nullable: false),
            //            Longitude = c.Single(nullable: false),
            //            AddressSourceId = c.Int(nullable: false),
            //            IsActive = c.Boolean(nullable: false),
            //            DeviceLat = c.Single(nullable: false),
            //            DeviceLong = c.Single(nullable: false),
            //            DateCreated = c.DateTime(),
            //            UserCreated = c.String(maxLength: 100, unicode: false),
            //            DateModified = c.DateTime(),
            //            UserModified = c.String(maxLength: 100, unicode: false),
            //        })
            //    .PrimaryKey(t => t.AddressId);

            //CreateTable(
            //    "dbo.AddressSource",
            //    c => new
            //        {
            //            AddressSourceId = c.Int(nullable: false, identity: true),
            //            AddressSourceName = c.String(maxLength: 50, unicode: false),
            //            DateCreated = c.DateTime(),
            //            UserCreated = c.String(maxLength: 100, unicode: false),
            //            DateModified = c.DateTime(),
            //            UserModified = c.String(maxLength: 100, unicode: false),
            //        })
            //    .PrimaryKey(t => t.AddressSourceId);

            //CreateTable(
            //    "dbo.AddressUserMapping",
            //    c => new
            //        {
            //            AddressUserMappingId = c.Int(nullable: false, identity: true),
            //            AddressId = c.Int(nullable: false),
            //            UserId = c.Int(),
            //            DateToVisit = c.DateTime(),
            //            DateVisited = c.DateTime(),
            //            VisitStatusId = c.Int(nullable: false),
            //            AppointmentDate = c.DateTime(),
            //            AppointmentTime = c.Time(nullable: false, precision: 7),
            //            Comments = c.String(maxLength: 250, unicode: false),
            //            IsActive = c.Boolean(nullable: false),
            //            DateCreated = c.DateTime(),
            //            UserCreated = c.String(maxLength: 100, unicode: false),
            //            DateModified = c.DateTime(),
            //            UserModified = c.String(maxLength: 100, unicode: false),
            //        })
            //    .PrimaryKey(t => t.AddressUserMappingId)
            //    .ForeignKey("dbo.Address", t => t.AddressId)
            //    .ForeignKey("dbo.AspNetUsers", t => t.UserId)
            //    .ForeignKey("dbo.VisitStatus", t => t.VisitStatusId)
            //    .Index(t => t.AddressId)
            //    .Index(t => t.UserId)
            //    .Index(t => t.VisitStatusId);

            //CreateTable(
            //    "dbo.AspNetUsers",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            UserParentId = c.Int(),
            //            Email = c.String(),
            //            EmailConfirmed = c.Boolean(nullable: false),
            //            PasswordHash = c.String(),
            //            SecurityStamp = c.String(),
            //            PhoneNumber = c.Long(),
            //            PhoneNumberConfirmed = c.Boolean(nullable: false),
            //            TwoFactorEnabled = c.Boolean(nullable: false),
            //            LockoutEndDateUtc = c.DateTime(),
            //            LockoutEnabled = c.Boolean(nullable: false),
            //            AccessFailedCount = c.Int(nullable: false),
            //            UserName = c.String(),
            //            FirstName = c.String(),
            //            LastName = c.String(),
            //            PartnerId = c.Int(nullable: false),
            //            UserTypeId = c.Int(nullable: false),
            //            IsActive = c.Boolean(nullable: false),
            //            ImageUrl = c.String(),
            //            DeviceName = c.String(),
            //            DeviceSerialNo = c.String(),
            //            DateCreated = c.DateTime(),
            //            UserCreated = c.String(maxLength: 100, unicode: false),
            //            DateModified = c.DateTime(),
            //            UserModified = c.String(maxLength: 100, unicode: false),
            //            UserType_UserTypeId = c.Int(),
            //            UserType_UserTypeId1 = c.Int(),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.Partner", t => t.PartnerId)
            //    .ForeignKey("dbo.UserType", t => t.UserType_UserTypeId)
            //    .ForeignKey("dbo.UserType", t => t.UserTypeId)
            //    .ForeignKey("dbo.AspNetUsers", t => t.UserParentId)
            //    .ForeignKey("dbo.UserType", t => t.UserType_UserTypeId1)
            //    .Index(t => t.UserParentId)
            //    .Index(t => t.PartnerId)
            //    .Index(t => t.UserTypeId)
            //    .Index(t => t.UserType_UserTypeId)
            //    .Index(t => t.UserType_UserTypeId1);

            //CreateTable(
            //    "dbo.Lead",
            //    c => new
            //        {
            //            LeadId = c.Int(nullable: false, identity: true),
            //            PrimaryEmail = c.String(maxLength: 50, unicode: false),
            //            SecondaryEmail = c.String(maxLength: 50, unicode: false),
            //            FirstName = c.String(maxLength: 50, unicode: false),
            //            LastName = c.String(maxLength: 100, unicode: false),
            //            DateOfBirth = c.String(maxLength: 50, unicode: false),
            //            UtilityAccountNo = c.String(maxLength: 50, unicode: false),
            //            UtilityCompanyName = c.String(maxLength: 100, unicode: false),
            //            HomePhoneNo = c.String(maxLength: 25, unicode: false),
            //            WorkPhoneNo = c.String(maxLength: 25, unicode: false),
            //            MobileNo = c.String(maxLength: 25, unicode: false),
            //            ServiceAddressId = c.Int(nullable: false),
            //            BillingAddressId = c.Int(nullable: false),
            //            RefferedBy = c.String(maxLength: 25, unicode: false),
            //            SubscriptionPercentage = c.Int(nullable: false),
            //            SSAType = c.String(maxLength: 25, unicode: false),
            //            AspNetUsersId = c.Int(),
            //            ChannelId = c.Int(),
            //            CampaignId = c.Int(nullable: false),
            //            LeadRankingId = c.Int(nullable: false),
            //            LandingPageId = c.Int(),
            //            LeadSourceId = c.Int(),
            //            EnvelopeId = c.String(maxLength: 50, unicode: false),
            //            TrancientId = c.String(maxLength: 150, unicode: false),
            //            IsActive = c.Boolean(nullable: false),
            //            InActiveReason = c.String(maxLength: 250, unicode: false),
            //            ReasonToClose = c.String(maxLength: 250, unicode: false),
            //            IsAssigned = c.Boolean(nullable: false),
            //            AllowDuplicateContract = c.Boolean(nullable: false),
            //            IsDuplicateLead = c.Boolean(nullable: false),
            //            DateCreated = c.DateTime(),
            //            UserCreated = c.String(maxLength: 100, unicode: false),
            //            DateModified = c.DateTime(),
            //            UserModified = c.String(maxLength: 100, unicode: false),
            //            LeadCounty_CountyId = c.Int(),
            //            LeadState_StateId = c.Int(),
            //        })
            //    .PrimaryKey(t => t.LeadId)
            //    .ForeignKey("dbo.LandingPage", t => t.LandingPageId)
            //    .ForeignKey("dbo.Address", t => t.BillingAddressId)
            //    .ForeignKey("dbo.Campaign", t => t.CampaignId)
            //    .ForeignKey("dbo.LeadSource", t => t.LeadSourceId)
            //    .ForeignKey("dbo.Channel", t => t.ChannelId)
            //    .ForeignKey("dbo.County", t => t.LeadCounty_CountyId)
            //    .ForeignKey("dbo.LeadRanking", t => t.LeadRankingId)
            //    .ForeignKey("dbo.AspNetUsers", t => t.AspNetUsersId)
            //    .ForeignKey("dbo.Address", t => t.ServiceAddressId)
            //    .ForeignKey("dbo.State", t => t.LeadState_StateId)
            //    .Index(t => t.ServiceAddressId)
            //    .Index(t => t.BillingAddressId)
            //    .Index(t => t.AspNetUsersId)
            //    .Index(t => t.ChannelId)
            //    .Index(t => t.CampaignId)
            //    .Index(t => t.LeadRankingId)
            //    .Index(t => t.LandingPageId)
            //    .Index(t => t.LeadSourceId)
            //    .Index(t => t.LeadCounty_CountyId)
            //    .Index(t => t.LeadState_StateId);

            //CreateTable(
            //    "dbo.LeadUserMappingHistory",
            //    c => new
            //        {
            //            LeadUserMappingHistoryId = c.Int(nullable: false, identity: true),
            //            LeadId = c.Int(nullable: false),
            //            AspNetUserId = c.Int(),
            //            DateCreated = c.DateTime(),
            //            UserCreated = c.String(maxLength: 100, unicode: false),
            //            DateModified = c.DateTime(),
            //            UserModified = c.String(maxLength: 100, unicode: false),
            //        })
            //    .PrimaryKey(t => t.LeadUserMappingHistoryId)
            //    .ForeignKey("dbo.Lead", t => t.LeadId)
            //    .ForeignKey("dbo.AspNetUsers", t => t.AspNetUserId)
            //    .Index(t => t.LeadId)
            //    .Index(t => t.AspNetUserId);

            //CreateTable(
            //    "dbo.CallLog",
            //    c => new
            //        {
            //            CallLogId = c.Int(nullable: false, identity: true),
            //            CallAppointmentId = c.Int(),
            //            LeadId = c.Int(nullable: false),
            //            CallComment = c.String(maxLength: 400, unicode: false),
            //            IsActive = c.Boolean(nullable: false),
            //            CreatedBy = c.String(maxLength: 100, unicode: false),
            //            IsDuplicateCall = c.Boolean(),
            //            CallStatusId = c.Int(nullable: false),
            //            DateCreated = c.DateTime(),
            //            UserCreated = c.String(maxLength: 100, unicode: false),
            //            DateModified = c.DateTime(),
            //            UserModified = c.String(maxLength: 100, unicode: false),
            //        })
            //    .PrimaryKey(t => t.CallLogId)
            //    .ForeignKey("dbo.CallStatus", t => t.CallStatusId)
            //    .ForeignKey("dbo.Lead", t => t.LeadId)
            //    .Index(t => t.LeadId)
            //    .Index(t => t.CallStatusId);

            //CreateTable(
            //    "dbo.CallAppointment",
            //    c => new
            //        {
            //            CallAppointmentId = c.Int(nullable: false),
            //            CallLogId = c.Int(nullable: false),
            //            DateToCall = c.DateTime(nullable: false),
            //            CallDate = c.DateTime(),
            //            DateCreated = c.DateTime(),
            //            UserCreated = c.String(maxLength: 100, unicode: false),
            //            DateModified = c.DateTime(),
            //            UserModified = c.String(maxLength: 100, unicode: false),
            //        })
            //    .PrimaryKey(t => t.CallAppointmentId)
            //    .ForeignKey("dbo.CallLog", t => t.CallAppointmentId)
            //    .Index(t => t.CallAppointmentId);

            //CreateTable(
            //    "dbo.CallStatus",
            //    c => new
            //        {
            //            CallStatusId = c.Int(nullable: false, identity: true),
            //            CallStatusName = c.String(maxLength: 70, unicode: false),
            //            IsActive = c.Boolean(nullable: false),
            //            DateCreated = c.DateTime(),
            //            UserCreated = c.String(maxLength: 100, unicode: false),
            //            DateModified = c.DateTime(),
            //            UserModified = c.String(maxLength: 100, unicode: false),
            //        })
            //    .PrimaryKey(t => t.CallStatusId);

            //CreateTable(
            //    "dbo.DupliacteContractRequest",
            //    c => new
            //        {
            //            DupliacteContractRequestId = c.Int(nullable: false, identity: true),
            //            LeadId = c.Int(nullable: false),
            //            Reason = c.String(maxLength: 250, unicode: false),
            //            IsDecided = c.Boolean(nullable: false),
            //            IsApproved = c.Boolean(nullable: false),
            //        })
            //    .PrimaryKey(t => t.DupliacteContractRequestId)
            //    .ForeignKey("dbo.Lead", t => t.LeadId)
            //    .Index(t => t.LeadId);

            //CreateTable(
            //    "dbo.LandingPage",
            //    c => new
            //    {
            //        LandingPageId = c.Int(nullable: false, identity: true),
            //        LandingPageName = c.String(maxLength: 400, unicode: false),
            //        IsActive = c.Boolean(nullable: false),
            //        DateCreated = c.DateTime(),
            //        UserCreated = c.String(maxLength: 100, unicode: false),
            //        DateModified = c.DateTime(),
            //        UserModified = c.String(maxLength: 100, unicode: false),
            //    })
            //    .PrimaryKey(t => t.LandingPageId);

            //CreateTable(
            //    "dbo.Campaign",
            //    c => new
            //        {
            //            CampaignId = c.Int(nullable: false, identity: true),
            //            CampaignName = c.String(maxLength: 200, unicode: false),
            //            AmountSpent = c.String(maxLength: 50, unicode: false),
            //            Description = c.String(maxLength: 250, unicode: false),
            //            TypeOfCampaign = c.String(maxLength: 150, unicode: false),
            //            Incentive = c.String(maxLength: 50, unicode: false),
            //            IsActive = c.Boolean(nullable: false),
            //            DateCreated = c.DateTime(),
            //            UserCreated = c.String(maxLength: 100, unicode: false),
            //            DateModified = c.DateTime(),
            //            UserModified = c.String(maxLength: 100, unicode: false),
            //        })
            //    .PrimaryKey(t => t.CampaignId);

            //CreateTable(
            //    "dbo.LeadSource",
            //    c => new
            //        {
            //            LeadSourceId = c.Int(nullable: false, identity: true),
            //            LeadSourceName = c.String(maxLength: 70, unicode: false),
            //            IsActive = c.Boolean(nullable: false),
            //        })
            //    .PrimaryKey(t => t.LeadSourceId);

            //CreateTable(
            //    "dbo.LeadSourceSubCategory",
            //    c => new
            //        {
            //            LeadSourceSubCategoryId = c.Int(nullable: false, identity: true),
            //            LeadSourceId = c.Int(nullable: false),
            //            LeadSourceSubCategoryName = c.String(maxLength: 150, unicode: false),
            //            IsActive = c.Boolean(nullable: false),
            //            DateCreated = c.DateTime(),
            //            UserCreated = c.String(maxLength: 100, unicode: false),
            //            DateModified = c.DateTime(),
            //            UserModified = c.String(maxLength: 100, unicode: false),
            //        })
            //    .PrimaryKey(t => t.LeadSourceSubCategoryId)
            //    .ForeignKey("dbo.LeadSource", t => t.LeadSourceId)
            //    .Index(t => t.LeadSourceId);

            //CreateTable(
            //    "dbo.Channel",
            //    c => new
            //        {
            //            ChannelId = c.Int(nullable: false, identity: true),
            //            ParentChannelId = c.Int(),
            //            ChannelName = c.String(maxLength: 50, unicode: false),
            //            IsActive = c.Boolean(nullable: false),
            //            ShowOnAdd = c.Boolean(nullable: false),
            //            DateCreated = c.DateTime(),
            //            UserCreated = c.String(maxLength: 100, unicode: false),
            //            DateModified = c.DateTime(),
            //            UserModified = c.String(maxLength: 100, unicode: false),
            //        })
            //    .PrimaryKey(t => t.ChannelId);

            //CreateTable(
            //    "dbo.County",
            //    c => new
            //        {
            //            CountyId = c.Int(nullable: false, identity: true),
            //            CountyName = c.String(maxLength: 100, unicode: false),
            //            IsActive = c.Boolean(nullable: false),
            //            StateId = c.Int(nullable: false),
            //            DateCreated = c.DateTime(),
            //            UserCreated = c.String(maxLength: 100, unicode: false),
            //            DateModified = c.DateTime(),
            //            UserModified = c.String(maxLength: 100, unicode: false),
            //        })
            //    .PrimaryKey(t => t.CountyId)
            //    .ForeignKey("dbo.State", t => t.StateId)
            //    .Index(t => t.StateId);

            //CreateTable(
            //    "dbo.State",
            //    c => new
            //        {
            //            StateId = c.Int(nullable: false, identity: true),
            //            StateName = c.String(maxLength: 150, unicode: false),
            //            IsActive = c.Boolean(nullable: false),
            //            DateCreated = c.DateTime(),
            //            UserCreated = c.String(maxLength: 100, unicode: false),
            //            DateModified = c.DateTime(),
            //            UserModified = c.String(maxLength: 100, unicode: false),
            //        })
            //    .PrimaryKey(t => t.StateId);

            //CreateTable(
            //    "dbo.LeadLeadStatusMapping",
            //    c => new
            //        {
            //            LeadLeadStatusMappingId = c.Int(nullable: false, identity: true),
            //            LeadId = c.Int(nullable: false),
            //            LeadStatusId = c.Int(nullable: false),
            //            UpdatedDate = c.DateTime(nullable: false),
            //            IsCurrent = c.Boolean(nullable: false),
            //            DateCreated = c.DateTime(),
            //            UserCreated = c.String(maxLength: 100, unicode: false),
            //            DateModified = c.DateTime(),
            //            UserModified = c.String(maxLength: 100, unicode: false),
            //        })
            //    .PrimaryKey(t => t.LeadLeadStatusMappingId)
            //    .ForeignKey("dbo.Lead", t => t.LeadId)
            //    .ForeignKey("dbo.LeadStatus", t => t.LeadStatusId)
            //    .Index(t => t.LeadId)
            //    .Index(t => t.LeadStatusId);

            //CreateTable(
            //    "dbo.LeadStatus",
            //    c => new
            //        {
            //            LeadStatusId = c.Int(nullable: false, identity: true),
            //            StatusName = c.String(maxLength: 70, unicode: false),
            //            IsActive = c.Boolean(nullable: false),
            //            DateCreated = c.DateTime(),
            //            UserCreated = c.String(maxLength: 100, unicode: false),
            //            DateModified = c.DateTime(),
            //            UserModified = c.String(maxLength: 100, unicode: false),
            //        })
            //    .PrimaryKey(t => t.LeadStatusId);

            //CreateTable(
            //    "dbo.LeadRanking",
            //    c => new
            //        {
            //            LeadRankingId = c.Int(nullable: false, identity: true),
            //            RankingName = c.String(maxLength: 50, unicode: false),
            //            DateCreated = c.DateTime(),
            //            UserCreated = c.String(maxLength: 100, unicode: false),
            //            DateModified = c.DateTime(),
            //            UserModified = c.String(maxLength: 100, unicode: false),
            //        })
            //    .PrimaryKey(t => t.LeadRankingId);

            //CreateTable(
            //    "dbo.Partner",
            //    c => new
            //        {
            //            PartnerId = c.Int(nullable: false, identity: true),
            //            PartnerNumber = c.String(maxLength: 20, unicode: false),
            //            PartnerName = c.String(maxLength: 50, unicode: false),
            //            PartnerLogoUrl = c.String(maxLength: 250, unicode: false),
            //            IsActive = c.Boolean(nullable: false),
            //            DateCreated = c.DateTime(),
            //            UserCreated = c.String(maxLength: 100, unicode: false),
            //            DateModified = c.DateTime(),
            //            UserModified = c.String(maxLength: 100, unicode: false),
            //        })
            //    .PrimaryKey(t => t.PartnerId);

            //CreateTable(
            //    "dbo.UserType",
            //    c => new
            //        {
            //            UserTypeId = c.Int(nullable: false, identity: true),
            //            UserTypeName = c.String(maxLength: 150, unicode: false),
            //            DateCreated = c.DateTime(),
            //            UserCreated = c.String(maxLength: 100, unicode: false),
            //            DateModified = c.DateTime(),
            //            UserModified = c.String(maxLength: 100, unicode: false),
            //        })
            //    .PrimaryKey(t => t.UserTypeId);

            //CreateTable(
            //    "dbo.VisitStatus",
            //    c => new
            //        {
            //            VisitStatusId = c.Int(nullable: false, identity: true),
            //            VisitName = c.String(maxLength: 150, unicode: false),
            //            IsActive = c.Boolean(nullable: false),
            //            DateCreated = c.DateTime(),
            //            UserCreated = c.String(maxLength: 100, unicode: false),
            //            DateModified = c.DateTime(),
            //            UserModified = c.String(maxLength: 100, unicode: false),
            //        })
            //    .PrimaryKey(t => t.VisitStatusId);

            //CreateTable(
            //    "dbo.AspNetUserClaims",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            UserId = c.Int(nullable: false),
            //            ClaimType = c.String(maxLength: 50, unicode: false),
            //            ClaimValue = c.String(maxLength: 50, unicode: false),
            //            DateCreated = c.DateTime(),
            //            UserCreated = c.String(maxLength: 100, unicode: false),
            //            DateModified = c.DateTime(),
            //            UserModified = c.String(maxLength: 100, unicode: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.AspNetUsers", t => t.UserId)
            //    .Index(t => t.UserId);

            //CreateTable(
            //    "dbo.AspNetUserLogins",
            //    c => new
            //        {
            //            UserId = c.Int(nullable: false),
            //            LoginProvider = c.String(maxLength: 50, unicode: false),
            //            ProviderKey = c.String(maxLength: 50, unicode: false),
            //            DateCreated = c.DateTime(),
            //            UserCreated = c.String(maxLength: 100, unicode: false),
            //            DateModified = c.DateTime(),
            //            UserModified = c.String(maxLength: 100, unicode: false),
            //        })
            //    .PrimaryKey(t => t.UserId)
            //    .ForeignKey("dbo.AspNetUsers", t => t.UserId)
            //    .Index(t => t.UserId);

            //CreateTable(
            //    "dbo.Audit",
            //    c => new
            //        {
            //            AuditId = c.Int(nullable: false, identity: true),
            //            RivisionStamp = c.DateTime(nullable: false),
            //            TableName = c.String(maxLength: 100, unicode: false),
            //            Actions = c.Int(nullable: false),
            //            OldData = c.String(storeType: "xml"),
            //            NewData = c.String(storeType: "xml"),
            //            ChangedColumns = c.String(maxLength: 400, unicode: false),
            //            DateCreated = c.DateTime(),
            //            UserCreated = c.String(maxLength: 100, unicode: false),
            //            DateModified = c.DateTime(),
            //            UserModified = c.String(maxLength: 100, unicode: false),
            //        })
            //    .PrimaryKey(t => t.AuditId);

            //CreateTable(
            //    "dbo.AspNetUserRoles",
            //    c => new
            //        {
            //            UserId = c.Int(nullable: false),
            //            RoleId = c.Int(nullable: false),
            //            DateCreated = c.DateTime(),
            //            UserCreated = c.String(maxLength: 100, unicode: false),
            //            DateModified = c.DateTime(),
            //            UserModified = c.String(maxLength: 100, unicode: false),
            //        })
            //    .PrimaryKey(t => new { t.UserId, t.RoleId })
            //    .ForeignKey("dbo.AspNetUsers", t => t.UserId)
            //    .ForeignKey("dbo.AspNetRoles", t => t.RoleId)
            //    .Index(t => t.UserId)
            //    .Index(t => t.RoleId);

            //CreateTable(
            //    "dbo.AspNetRoles",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Name = c.String(maxLength: 100, unicode: false),
            //            DateCreated = c.DateTime(),
            //            UserCreated = c.String(maxLength: 100, unicode: false),
            //            DateModified = c.DateTime(),
            //            UserModified = c.String(maxLength: 100, unicode: false),
            //        })
            //    .PrimaryKey(t => t.Id);

            //CreateTable(
            //    "dbo.LeadSourceCampaign",
            //    c => new
            //        {
            //            LeadSource_LeadSourceId = c.Int(nullable: false),
            //            Campaign_CampaignId = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => new { t.LeadSource_LeadSourceId, t.Campaign_CampaignId })
            //    .ForeignKey("dbo.LeadSource", t => t.LeadSource_LeadSourceId)
            //    .ForeignKey("dbo.Campaign", t => t.Campaign_CampaignId)
            //    .Index(t => t.LeadSource_LeadSourceId)
            //    .Index(t => t.Campaign_CampaignId);

        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AddressUserMapping", "VisitStatusId", "dbo.VisitStatus");
            DropForeignKey("dbo.AspNetUsers", "UserType_UserTypeId1", "dbo.UserType");
            DropForeignKey("dbo.AspNetUsers", "UserParentId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "UserTypeId", "dbo.UserType");
            DropForeignKey("dbo.AspNetUsers", "UserType_UserTypeId", "dbo.UserType");
            DropForeignKey("dbo.AspNetUsers", "PartnerId", "dbo.Partner");
            DropForeignKey("dbo.Lead", "LeadState_StateId", "dbo.State");
            DropForeignKey("dbo.Lead", "ServiceAddressId", "dbo.Address");
            DropForeignKey("dbo.Lead", "AspNetUsersId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Lead", "LeadRankingId", "dbo.LeadRanking");
            DropForeignKey("dbo.LeadLeadStatusMapping", "LeadStatusId", "dbo.LeadStatus");
            DropForeignKey("dbo.LeadLeadStatusMapping", "LeadId", "dbo.Lead");
            DropForeignKey("dbo.Lead", "LeadCounty_CountyId", "dbo.County");
            DropForeignKey("dbo.County", "StateId", "dbo.State");
            DropForeignKey("dbo.Lead", "ChannelId", "dbo.Channel");
            DropForeignKey("dbo.LeadSourceSubCategory", "LeadSourceId", "dbo.LeadSource");
            DropForeignKey("dbo.Lead", "LeadSourceId", "dbo.LeadSource");
            DropForeignKey("dbo.LeadSourceCampaign", "Campaign_CampaignId", "dbo.Campaign");
            DropForeignKey("dbo.LeadSourceCampaign", "LeadSource_LeadSourceId", "dbo.LeadSource");
            DropForeignKey("dbo.Lead", "CampaignId", "dbo.Campaign");
            DropForeignKey("dbo.Lead", "BillingAddressId", "dbo.Address");
            DropForeignKey("dbo.Lead", "LandingPageId", "dbo.LandingPage");
            DropForeignKey("dbo.DupliacteContractRequest", "LeadId", "dbo.Lead");
            DropForeignKey("dbo.CallLog", "LeadId", "dbo.Lead");
            DropForeignKey("dbo.CallLog", "CallStatusId", "dbo.CallStatus");
            DropForeignKey("dbo.CallAppointment", "CallAppointmentId", "dbo.CallLog");
            DropForeignKey("dbo.LeadUserMappingHistory", "AspNetUserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.LeadUserMappingHistory", "LeadId", "dbo.Lead");
            DropForeignKey("dbo.AddressUserMapping", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AddressUserMapping", "AddressId", "dbo.Address");
            DropIndex("dbo.LeadSourceCampaign", new[] { "Campaign_CampaignId" });
            DropIndex("dbo.LeadSourceCampaign", new[] { "LeadSource_LeadSourceId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.LeadLeadStatusMapping", new[] { "LeadStatusId" });
            DropIndex("dbo.LeadLeadStatusMapping", new[] { "LeadId" });
            DropIndex("dbo.County", new[] { "StateId" });
            DropIndex("dbo.LeadSourceSubCategory", new[] { "LeadSourceId" });
            DropIndex("dbo.DupliacteContractRequest", new[] { "LeadId" });
            DropIndex("dbo.CallAppointment", new[] { "CallAppointmentId" });
            DropIndex("dbo.CallLog", new[] { "CallStatusId" });
            DropIndex("dbo.CallLog", new[] { "LeadId" });
            DropIndex("dbo.LeadUserMappingHistory", new[] { "AspNetUserId" });
            DropIndex("dbo.LeadUserMappingHistory", new[] { "LeadId" });
            DropIndex("dbo.Lead", new[] { "LeadState_StateId" });
            DropIndex("dbo.Lead", new[] { "LeadCounty_CountyId" });
            DropIndex("dbo.Lead", new[] { "LeadSourceId" });
            DropIndex("dbo.Lead", new[] { "LandingPageId" });
            DropIndex("dbo.Lead", new[] { "LeadRankingId" });
            DropIndex("dbo.Lead", new[] { "CampaignId" });
            DropIndex("dbo.Lead", new[] { "ChannelId" });
            DropIndex("dbo.Lead", new[] { "AspNetUsersId" });
            DropIndex("dbo.Lead", new[] { "BillingAddressId" });
            DropIndex("dbo.Lead", new[] { "ServiceAddressId" });
            DropIndex("dbo.AspNetUsers", new[] { "UserType_UserTypeId1" });
            DropIndex("dbo.AspNetUsers", new[] { "UserType_UserTypeId" });
            DropIndex("dbo.AspNetUsers", new[] { "UserTypeId" });
            DropIndex("dbo.AspNetUsers", new[] { "PartnerId" });
            DropIndex("dbo.AspNetUsers", new[] { "UserParentId" });
            DropIndex("dbo.AddressUserMapping", new[] { "VisitStatusId" });
            DropIndex("dbo.AddressUserMapping", new[] { "UserId" });
            DropIndex("dbo.AddressUserMapping", new[] { "AddressId" });
            DropTable("dbo.LeadSourceCampaign");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.Audit");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.VisitStatus");
            DropTable("dbo.UserType");
            DropTable("dbo.Partner");
            DropTable("dbo.LeadRanking");
            DropTable("dbo.LeadStatus");
            DropTable("dbo.LeadLeadStatusMapping");
            DropTable("dbo.State");
            DropTable("dbo.County");
            DropTable("dbo.Channel");
            DropTable("dbo.LeadSourceSubCategory");
            DropTable("dbo.LeadSource");
            DropTable("dbo.Campaign");
            DropTable("dbo.LandingPage");
            DropTable("dbo.DupliacteContractRequest");
            DropTable("dbo.CallStatus");
            DropTable("dbo.CallAppointment");
            DropTable("dbo.CallLog");
            DropTable("dbo.LeadUserMappingHistory");
            DropTable("dbo.Lead");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AddressUserMapping");
            DropTable("dbo.AddressSource");
            DropTable("dbo.Address");
        }
    }
}
