namespace SunshareDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UANmax : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Lead", "UtilityCompanyName", c => c.String(maxLength: 8000, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Lead", "UtilityCompanyName", c => c.String(maxLength: 100, unicode: false));
        }
    }
}
