namespace SunshareDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class utilityaccount : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Lead", "UtilityAccountNo", c => c.String(maxLength: 255, unicode: false));
            AlterColumn("dbo.Lead", "UtilityCompanyName", c => c.String(maxLength: 50, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Lead", "UtilityCompanyName", c => c.String(maxLength: 255, unicode: false));
            AlterColumn("dbo.Lead", "UtilityAccountNo", c => c.String(maxLength: 50, unicode: false));
        }
    }
}
