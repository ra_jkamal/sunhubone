namespace SunshareDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class utilityacc : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Lead", "UtilityAccountNo", c => c.String(maxLength: 8000, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Lead", "UtilityAccountNo", c => c.String(maxLength: 255, unicode: false));
        }
    }
}
