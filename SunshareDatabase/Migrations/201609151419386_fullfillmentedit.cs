namespace SunshareDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fullfillmentedit : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.tbl_m_CampaignDetails_CM",
                c => new
                    {
                        CampaignID = c.Int(nullable: false, identity: true),
                        CampaignName = c.String(),
                        Status = c.String(),
                        ParentType = c.String(),
                        Type = c.String(),
                        SubType = c.String(),
                        Creative = c.String(),
                        OfferCode = c.String(),
                        Incentive = c.String(),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Isactive = c.Boolean(nullable: false),
                        Createdby = c.String(),
                        Created_datetime = c.DateTime(nullable: false),
                        Alteredby = c.String(),
                        Altered_datetime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.CampaignID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.tbl_m_CampaignDetails_CM");
        }
    }
}
