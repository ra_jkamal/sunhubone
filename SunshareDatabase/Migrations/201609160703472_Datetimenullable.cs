namespace SunshareDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Datetimenullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.tbl_m_CampaignDetails_CM", "Created_datetime", c => c.DateTime());
            AlterColumn("dbo.tbl_m_CampaignDetails_CM", "Altered_datetime", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.tbl_m_CampaignDetails_CM", "Altered_datetime", c => c.DateTime(nullable: false));
            AlterColumn("dbo.tbl_m_CampaignDetails_CM", "Created_datetime", c => c.DateTime(nullable: false));
        }
    }
}
