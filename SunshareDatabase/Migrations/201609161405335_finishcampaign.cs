namespace SunshareDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class finishcampaign : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Lead", "CampaignId", "dbo.Campaign");
        }
        
        public override void Down()
        {
            AddForeignKey("dbo.Lead", "CampaignId", "dbo.Campaign", "CampaignId");
        }
    }
}
