namespace SunshareDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class oldstage : DbMigration
    {
        public override void Up()
        {

            //DropForeignKey("dbo.AspNetUsers", "UserType_UserTypeId1", "dbo.UserType");
            DropIndex("dbo.AspNetUsers", new[] { "UserTypeId" });
            DropIndex("dbo.AspNetUsers", new[] { "UserType_UserTypeId" });
            //DropIndex("dbo.AspNetUsers", new[] { "UserType_UserTypeId1" });
            //DropColumn("dbo.AspNetUsers", "UserTypeId");
            //RenameColumn(table: "dbo.AspNetUsers", name: "UserType_UserTypeId", newName: "UserTypeId");
            AlterColumn("dbo.AspNetUsers", "UserTypeId", c => c.Int(nullable: false));
            CreateIndex("dbo.AspNetUsers", "UserTypeId");
            //DropColumn("dbo.AspNetUsers", "UserType_UserTypeId1");
        }

        public override void Down()
        {
            //AddColumn("dbo.AspNetUsers", "UserType_UserTypeId1", c => c.Int());
            DropIndex("dbo.AspNetUsers", new[] { "UserTypeId" });
            AlterColumn("dbo.AspNetUsers", "UserTypeId", c => c.Int());
            //RenameColumn(table: "dbo.AspNetUsers", name: "UserTypeId", newName: "UserType_UserTypeId");
            AddColumn("dbo.AspNetUsers", "UserTypeId", c => c.Int(nullable: false));
            //CreateIndex("dbo.AspNetUsers", "UserType_UserTypeId1");
            //CreateIndex("dbo.AspNetUsers", "UserType_UserTypeId");
            CreateIndex("dbo.AspNetUsers", "UserTypeId");
            AddForeignKey("dbo.AspNetUsers", "UserTypeId", "dbo.UserType", "UserTypeId");
        }
    }
}
