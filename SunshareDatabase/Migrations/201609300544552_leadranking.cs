namespace SunshareDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class leadranking : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LeadRanking", "DisplayOrder", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LeadRanking", "DisplayOrder");
        }
    }
}
