namespace SunshareDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class boundstatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CallLog", "BoundStatusId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CallLog", "BoundStatusId");
        }
    }
}
