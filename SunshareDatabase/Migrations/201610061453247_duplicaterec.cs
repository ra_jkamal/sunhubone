namespace SunshareDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class duplicaterec : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DupliacteContractRequest", "RepId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DupliacteContractRequest", "RepId");
        }
    }
}
