namespace SunshareDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class duplicaterecdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DupliacteContractRequest", "RequestDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DupliacteContractRequest", "RequestDate");
        }
    }
}
