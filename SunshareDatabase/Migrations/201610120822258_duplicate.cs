namespace SunshareDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class duplicate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DupliacteContractRequest", "CreateDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.DupliacteContractRequest", "ModifiedDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.DupliacteContractRequest", "ReqRepId", c => c.Int(nullable: false));
            AddColumn("dbo.DupliacteContractRequest", "ApproveId", c => c.Int(nullable: false));
            DropColumn("dbo.DupliacteContractRequest", "RepId");
            DropColumn("dbo.DupliacteContractRequest", "RequestDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DupliacteContractRequest", "RequestDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.DupliacteContractRequest", "RepId", c => c.Int(nullable: false));
            DropColumn("dbo.DupliacteContractRequest", "ApproveId");
            DropColumn("dbo.DupliacteContractRequest", "ReqRepId");
            DropColumn("dbo.DupliacteContractRequest", "ModifiedDate");
            DropColumn("dbo.DupliacteContractRequest", "CreateDate");
        }
    }
}
