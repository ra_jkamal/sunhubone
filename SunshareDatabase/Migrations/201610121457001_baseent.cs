namespace SunshareDatabase.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class baseent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DupliacteContractRequest", "DateCreated", c => c.DateTime());
            AddColumn("dbo.DupliacteContractRequest", "UserCreated", c => c.String(maxLength: 100, unicode: false));
            AddColumn("dbo.DupliacteContractRequest", "DateModified", c => c.DateTime());
            AddColumn("dbo.DupliacteContractRequest", "UserModified", c => c.String(maxLength: 100, unicode: false));
            DropColumn("dbo.DupliacteContractRequest", "CreateDate");
            DropColumn("dbo.DupliacteContractRequest", "ModifiedDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DupliacteContractRequest", "ModifiedDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.DupliacteContractRequest", "CreateDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.DupliacteContractRequest", "UserModified");
            DropColumn("dbo.DupliacteContractRequest", "DateModified");
            DropColumn("dbo.DupliacteContractRequest", "UserCreated");
            DropColumn("dbo.DupliacteContractRequest", "DateCreated");
        }
    }
}
