namespace SunshareDatabase.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<SunshareDatabase.SunshareDatabaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = false;
        }

        protected override void Seed(SunshareDatabase.SunshareDatabaseContext context)
        {
            ////  This method will be called after migrating to the latest version.

            ////  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            ////  to avoid creating duplicate seed data. E.g.
            ////
            ////    context.People.AddOrUpdate(
            ////      p => p.FullName,
            ////      new Person { FullName = "Andrew Peters" },
            ////      new Person { FullName = "Brice Lambson" },
            ////      new Person { FullName = "Rowan Miller" }
            ////    );
            ////
            ////  This method will be called after migrating to the latest version.

            ////  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            ////  to avoid creating duplicate seed data. E.g.
            ////
            ////    context.People.AddOrUpdate(
            ////      p => p.FullName,
            ////      new Person { FullName = "Andrew Peters" },
            ////      new Person { FullName = "Brice Lambson" },
            ////      new Person { FullName = "Rowan Miller" }
            ////    );
            ////

            ////States
            //context.States.AddOrUpdate(s => s.StateName,
            //    new State { StateName = "Minnesota", IsActive = true, StateId = 1 },
            //    new State { StateName = "Colarado", IsActive = true, StateId = 2 }
            //    );

            ////Counties

            //context.Counties.AddOrUpdate(c => c.CountyName,
            //    new County { CountyId = 1, CountyName = "Atkin", IsActive = true, StateId = 1 },
            //    new County { CountyId = 2, CountyName = "El Paso", IsActive = true, StateId = 2 }
            //    );

            ////UserType
            //context.UserTypes.AddOrUpdate(u => u.UserTypeName,

            //    new UserType { UserTypeId = 1, UserTypeName = "D2D" },
            //    new UserType { UserTypeId = 2, UserTypeName = "TeleSales" },
            //    new UserType { UserTypeId = 3, UserTypeName = "Website" });

            ////Channels
            //context.Channels.AddOrUpdate(c => c.ChannelName,
            //    new Channel { ChannelId = 1, IsActive = true, ChannelName = "Results - Call Center" },
            //    new Channel { ChannelId = 2, IsActive = true, ChannelName = "PCCW - CallCenter" },
            //    new Channel { ChannelId = 3, IsActive = true, ChannelName = "Web Enrollment - Auto" },
            //    new Channel { ChannelId = 4, IsActive = true, ChannelName = "D2D" },
            //    new Channel { ChannelId = 3, IsActive = true, ChannelName = "Web Enrollment - Landing Page" }
            //    );

            ////LeadRanking
            //context.LeadRakings.AddOrUpdate(l => l.RankingName,
            //    new LeadRanking { LeadRankingId = 1, RankingName = "Cold" },
            //    new LeadRanking { LeadRankingId = 2, RankingName = "Luke Warm" },
            //    new LeadRanking { LeadRankingId = 1, RankingName = "Warm" },
            //    new LeadRanking { LeadRankingId = 1, RankingName = "Hot" },
            //    new LeadRanking { LeadRankingId = 1, RankingName = "Fire" },
            //    new LeadRanking { LeadRankingId = 1, RankingName = "No Ranking" }
            //    );

            ////Partners
            //context.Partners.AddOrUpdate(p => p.PartnerName,
            //    new Partner { PartnerId = 1, PartnerNumber = "10002", PartnerName = "PCCW", PartnerLogoUrl = "Logo/pccw-logo.png", IsActive = true },
            //    new Partner { PartnerId = 2, PartnerNumber = "10001", PartnerName = "RESULTS", PartnerLogoUrl = "Logo/results-logo.png", IsActive = true },
            //    new Partner { PartnerId = 3, PartnerNumber = "10010", PartnerName = "ILS", PartnerLogoUrl = "Logo/ils-logo.png", IsActive = true },
            //    new Partner { PartnerId = 4, PartnerNumber = "10003", PartnerName = "Internal Sales Team", PartnerLogoUrl = "Logo/sunshare-logo.png", IsActive = true },
            //    new Partner { PartnerId = 5, PartnerNumber = "10007", PartnerName = "ILS", PartnerLogoUrl = "Logo/ils-logo.png", IsActive = true }
            //    );

            ////LeadSoureces
            //context.LeadSources.AddOrUpdate(l => l.LeadSourceName,
            //    new LeadSource { LeadSourceId = 1, LeadSourceName = "Educational Mailer", IsActive = true },
            //    new LeadSource { LeadSourceId = 2, LeadSourceName = "Rate Hike Mailer", IsActive = true },
            //    new LeadSource { LeadSourceId = 3, LeadSourceName = "Incentive Mailer", IsActive = true },
            //    new LeadSource { LeadSourceId = 4, LeadSourceName = "Radio", IsActive = true },
            //    new LeadSource { LeadSourceId = 5, LeadSourceName = "Newspaper", IsActive = true },
            //    new LeadSource { LeadSourceId = 6, LeadSourceName = "Online Video", IsActive = true },
            //    new LeadSource { LeadSourceId = 7, LeadSourceName = "Online Advertising : No Offer", IsActive = true },
            //    new LeadSource { LeadSourceId = 8, LeadSourceName = "Outbound Call", IsActive = true },
            //    new LeadSource { LeadSourceId = 9, LeadSourceName = "Billboard", IsActive = true },
            //    new LeadSource { LeadSourceId = 10, LeadSourceName = "Facebook", IsActive = true },
            //    new LeadSource { LeadSourceId = 11, LeadSourceName = "Twitter", IsActive = true },
            //    new LeadSource { LeadSourceId = 12, LeadSourceName = "Referal", IsActive = true },
            //    new LeadSource { LeadSourceId = 13, LeadSourceName = "Web Search", IsActive = true },
            //    new LeadSource { LeadSourceId = 14, LeadSourceName = "Other", IsActive = true },
            //    new LeadSource { LeadSourceId = 15, LeadSourceName = "Online Advertising : with $200 resturant.com Offer", IsActive = true }
            //    );

            ////CallStatus
            //context.CallStatus.AddOrUpdate(c => c.CallStatusName,
            //    new CallStatus { CallStatusId = 1, CallStatusName = "Don't Call", IsActive = true },
            //    new CallStatus { CallStatusId = 2, CallStatusName = "Call Back", IsActive = true },
            //    new CallStatus { CallStatusId = 3, CallStatusName = "Interested", IsActive = true },
            //    new CallStatus { CallStatusId = 4, CallStatusName = "Closed Lost", IsActive = true },
            //    new CallStatus { CallStatusId = 5, CallStatusName = "Incoming", IsActive = true },
            //     new CallStatus { CallStatusId = 5, CallStatusName = "No Answer", IsActive = true }
            //   );

            ////VisitStatus
            //context.VisitStatuses.AddOrUpdate(v => v.VisitName,
            //    new VisitStatus { VisitStatusId = 1, VisitName = "Not at Home", IsActive = true },
            //    new VisitStatus { VisitStatusId = 2, VisitName = "Not Interested", IsActive = true },
            //    new VisitStatus { VisitStatusId = 3, VisitName = "Go Back", IsActive = true },
            //    new VisitStatus { VisitStatusId = 4, VisitName = "Appointment", IsActive = true },
            //    new VisitStatus { VisitStatusId = 5, VisitName = "Close Sale", IsActive = true },
            //    new VisitStatus { VisitStatusId = 6, VisitName = "Others", IsActive = true },
            //    new VisitStatus { VisitStatusId = 7, VisitName = "Customer Interested", IsActive = true }
            //    );

            ////AspNetRoles
            //context.UserRoles.AddOrUpdate(r => r.Name,
            //    new AspNetRoles { Id = 1, Name = "Super Admin" },
            //    new AspNetRoles { Id = 2, Name = "Manager" },
            //    new AspNetRoles { Id = 3, Name = "Sales Rep" },
            //    new AspNetRoles { Id = 4, Name = "Executive Admin" }
            //    );

            ////LeadStatus
            //context.Status.AddOrUpdate(l => l.StatusName,
            //    new LeadStatus { LeadStatusId = 1, StatusName = "Contract Sent", IsActive = true },
            //    new LeadStatus { LeadStatusId = 2, StatusName = "Contract Signed", IsActive = true },
            //    new LeadStatus { LeadStatusId = 3, StatusName = "Contract Decliened", IsActive = true },
            //    new LeadStatus { LeadStatusId = 4, StatusName = "Sent Information", IsActive = true },
            //    new LeadStatus { LeadStatusId = 5, StatusName = "Lead Created", IsActive = true }
            //    );

            ////Campaign
            //context.Campaigns.AddOrUpdate(c => c.CampaignName,
            //   new Campaign { CampaignId = 1, CampaignName = "Hennepin D2D Q2 - 3", AmountSpent = "1000", IsActive = true, Incentive = "200" },
            //   new Campaign { CampaignId = 2, CampaignName = "St.Cloud D2D Q2 - 3", AmountSpent = "1000", IsActive = true, Incentive = "200" },
            //   new Campaign { CampaignId = 3, CampaignName = "Mankato D2D Q2 - 3", AmountSpent = "1000", IsActive = true, Incentive = "200" },
            //   new Campaign { CampaignId = 4, CampaignName = "St.Cloud Community Solar 101 3 Dates", AmountSpent = "1000", IsActive = true, Incentive = "200" },
            //   new Campaign { CampaignId = 5, CampaignName = "St.Cloud Summertime by George Event", AmountSpent = "1000", IsActive = true, Incentive = "200" },
            //   new Campaign { CampaignId = 6, CampaignName = "St.Cloud Granite City Days Event", AmountSpent = "1000", IsActive = true, Incentive = "200" },
            //   new Campaign { CampaignId = 7, CampaignName = "Door Hangers Q2-3", AmountSpent = "1000", IsActive = true, Incentive = "200" },
            //   new Campaign { CampaignId = 8, CampaignName = "Radio St. Cloud Mankato Q2 - 3", AmountSpent = "1000", IsActive = true, Incentive = "200" },
            //   new Campaign { CampaignId = 9, CampaignName = "Cable Hennepin Q2 - 3", AmountSpent = "1000", IsActive = true, Incentive = "200" },
            //   new Campaign { CampaignId = 10, CampaignName = "Paid Search Ads Q2-3", AmountSpent = "1000", IsActive = true, Incentive = "200" },
            //   new Campaign { CampaignId = 11, CampaignName = "Newspaper StarTrib Q2 - 3", AmountSpent = "1000", IsActive = true, Incentive = "200" },
            //   new Campaign { CampaignId = 12, CampaignName = "Red Plum Shared Mail Ads Q2-3", AmountSpent = "1000", IsActive = true, Incentive = "200" },
            //   new Campaign { CampaignId = 13, CampaignName = "DM 1 Educational $25 Amazon Q2_Q3", AmountSpent = "1000", IsActive = true, Incentive = "200" },
            //   new Campaign { CampaignId = 14, CampaignName = "DM 2 Incentive $50 Visa Q2_Q3", AmountSpent = "1000", IsActive = true, Incentive = "200" },
            //   new Campaign { CampaignId = 15, CampaignName = "Preroll Dr. Solis Q2_Q3", AmountSpent = "1000", IsActive = true, Incentive = "200" },
            //   new Campaign { CampaignId = 16, CampaignName = "Facebook Q2_Q3", AmountSpent = "1000", IsActive = true, Incentive = "200" },
            //   new Campaign { CampaignId = 17, CampaignName = "Twitter Q2_Q3", AmountSpent = "1000", IsActive = true, Incentive = "200" },
            //   new Campaign { CampaignId = 18, CampaignName = "TBD Q2_Q3", AmountSpent = "1000", IsActive = true, Incentive = "200" },
            //   new Campaign { CampaignId = 19, CampaignName = "TBD 2 Q2_Q3", AmountSpent = "1000", IsActive = true, Incentive = "200" },
            //   new Campaign { CampaignId = 20, CampaignName = "Display Ads 0 Q2_Q3", AmountSpent = "1000", IsActive = true, Incentive = "200" },
            //   new Campaign { CampaignId = 21, CampaignName = "Display Ads 1 Q2_Q3", AmountSpent = "1000", IsActive = true, Incentive = "200" }
            //  );
        }
    }
}
