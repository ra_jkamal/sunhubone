﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class Partner: BaseEntity
    {
        public Partner()
        {
        }
        [Key]
        public int PartnerId { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(20)]
        public string PartnerNumber { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string PartnerName { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(250)]
        public string PartnerLogoUrl { get; set; }
        public bool IsActive { get; set; }
    }
}