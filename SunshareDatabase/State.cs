﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class State : BaseEntity
    {
        public State()
        {
            Counties=new List<County>();
            Leads=new List<Lead>();
        }
        public int StateId { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(150)]
        public string StateName { get; set; }
        public bool IsActive { get; set; }
        protected virtual ICollection<County> Counties { get; set; }
        protected virtual ICollection<Lead> Leads { get; set; }        
    }
}