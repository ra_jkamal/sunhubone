﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;

namespace SunshareDatabase
{
    public interface ISunshareDatabaseContext
    {
        int SaveChanges();
       IDbSet<State> States { get; set; }
       IDbSet<County> Counties { get; set; }
       IDbSet<Lead> Leads { get; set; }
       IDbSet<Campaign> Campaigns { get; set; }
       IDbSet<Channel> Channels { get; set; }
       IDbSet<LeadStatus> Status { get; set; }
       IDbSet<CallAppointment> CallAppointment { get; set; }
       IDbSet<CallStatus> CallStatus { get; set; }
       IDbSet<LeadSource> LeadSources { get; set; }
       IDbSet<Audit> Audits { get; set; }
       IDbSet<CallLog> CallLogs { get; set; }
       IDbSet<Partner> Partners { get; set; }
       IDbSet<UserType> UserTypes { get; set; }
       IDbSet<Address> Address { get; set; }
       IDbSet<AddressSource> AddressSources { get; set; }
       IDbSet<AddressUserMapping> AddressUserMappings { get; set; }
       IDbSet<VisitStatus> VisitStatuses { get; set; }
       IDbSet<LeadRanking> LeadRakings { get; set; }
       IDbSet<LeadLeadStatusMapping> LeadLeadStatusMappings { get; set; }
       IDbSet<AspNetRoles> UserRoles { get; set; }
       IDbSet<AspNetUserRoles> UserInRoles { get; set; }
       IDbSet<AspNetUsers> Users { get; set; }
       IDbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
       IDbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
       IDbSet<LeadUserMappingHistory> AssignmentHistory { get; set; }
       DbEntityEntry GetMatchingDbEntity(object entity, EntityState[] filterStates);
       IEnumerable<DbEntityEntry> GetDbEntities<T>(EntityState[] filterStates);
       IDbSet<LeadSourceSubCategory> LeadSourceSubCategories { get; set; } 
        IDbSet<DupliacteContractRequest> DupliacteContractRequests { get; set; }
        IDbSet<LandingPage> LandingPages { get; set; }
        IDbSet<tbl_m_CampaignDetails_CM> CampaignDetails { get; set; }
        IDbSet<tbl_m_ContractMst_FF> Contracts { get; set; }
        IDbSet<tbl_m_CreditScore_FF> CreditScores { get; set; }
        IDbSet<tbl_m_UsageReceived_FF> UsagesReceived { get; set; }
        IDbSet<tbl_m_UsageRequest_FF> UsagesRequested { get; set; }
    }
    public class SunshareDatabaseContext : DbContext, ISunshareDatabaseContext
    {
        public SunshareDatabaseContext() : base("SunshareDatabase_CC")
        {
        }
        public IDbSet<State> States { get; set; }
        public IDbSet<County> Counties { get; set; }
        public IDbSet<Lead> Leads { get; set; }
        public IDbSet<Campaign> Campaigns { get; set; }
        public IDbSet<Channel> Channels { get; set; }
        public IDbSet<LeadStatus> Status { get; set; }
        public IDbSet<CallAppointment> CallAppointment { get; set; }
        public IDbSet<CallStatus> CallStatus { get; set; }
        public IDbSet<LeadSource> LeadSources { get; set; }
        public IDbSet<Audit> Audits { get; set; }
        public IDbSet<CallLog> CallLogs { get; set; }
        public IDbSet<Partner> Partners { get; set; }
        public IDbSet<UserType> UserTypes { get; set; }
        public IDbSet<Address> Address { get; set; }
        public IDbSet<AddressSource> AddressSources { get; set; }
        public IDbSet<AddressUserMapping> AddressUserMappings { get; set; }
        public IDbSet<VisitStatus> VisitStatuses { get; set; }
        public IDbSet<LeadRanking> LeadRakings { get; set; }
        public IDbSet<LeadLeadStatusMapping> LeadLeadStatusMappings { get; set; }
        public IDbSet<AspNetRoles> UserRoles { get; set; }
        public IDbSet<AspNetUserRoles> UserInRoles { get; set; }
        public IDbSet<AspNetUsers> Users { get; set; }
        public IDbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public IDbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public IDbSet<LeadUserMappingHistory> AssignmentHistory { get; set; }
        public IDbSet<LeadSourceSubCategory> LeadSourceSubCategories { get; set; }
        public IDbSet<DupliacteContractRequest> DupliacteContractRequests { get; set; }
        public IDbSet<tbl_m_CampaignDetails_CM> CampaignDetails { get; set; }
        public IDbSet<tbl_m_ContractMst_FF> Contracts { get; set; }
        public IDbSet<tbl_m_CreditScore_FF> CreditScores { get; set; }
        public IDbSet<tbl_m_UsageReceived_FF> UsagesReceived { get; set; }
        public IDbSet<tbl_m_UsageRequest_FF> UsagesRequested { get; set; }
        public IDbSet<LandingPage> LandingPages { get; set; }
        public DbEntityEntry GetMatchingDbEntity(object entity, EntityState[] filterStates)
        {
            return ChangeTracker.Entries()
                    .FirstOrDefault(e => e.Entity == entity && filterStates.Contains(e.State));
        }

        public IEnumerable<DbEntityEntry> GetDbEntities<T>(EntityState[] filterStates)
        {
            return ChangeTracker.Entries()
                .Where(e => e.Entity is T && filterStates.Contains(e.State));
        }
        public static void Prestart()
        {
            Database.SetInitializer<SunshareDatabaseContext>(null);
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            
            // ForeignKeyAssociationMultiplicityConvention, optional FK through nullable Ids.
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Entity<CallLog>().HasOptional(a => a.Appointment).WithRequired(l => l.CallLog);
            //modelBuilder.Configurations.Add(new LeadConfiguration());
        }

        //public class LeadConfiguration : EntityTypeConfiguration<Lead>
        //{
            
        //}

        public override int SaveChanges()
        {
            try
            {
                var entities = ChangeTracker.Entries().Where(x => x.Entity is BaseEntity && (x.State == EntityState.Added || x.State == EntityState.Modified));
                var currentUsername = HttpContext.Current != null && HttpContext.Current.User != null
                    ? HttpContext.Current.User.Identity.GetUserName()
                    : "Anonymous";
                foreach (var entity in entities)
                {
                    if (entity.State == EntityState.Added)
                    {
                        ((BaseEntity)entity.Entity).DateCreated = DateTime.Now;
                        ((BaseEntity)entity.Entity).UserCreated = currentUsername;
                    }
                    ((BaseEntity)entity.Entity).DateModified = DateTime.Now;
                    ((BaseEntity)entity.Entity).UserModified = currentUsername;
                }
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);
                var fullErrorMessage = string.Join("; ", errorMessages);
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        //public System.Data.Entity.DbSet<SunshareDatabase.Address> Addresses { get; set; }
    }
}