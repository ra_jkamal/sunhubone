﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class UserType : BaseEntity
    {
        public UserType()
        {
            Users=new List<AspNetUsers>();
        }
        public int UserTypeId { get; set; }
        [Column(TypeName = "VARCHAR")]
        [StringLength(150)]
        public string UserTypeName { get; set; }
        public virtual ICollection<AspNetUsers> Users { get; set; }
    }
}