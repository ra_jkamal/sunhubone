﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class tbl_m_CampaignDetails_CM 
    {
        [Key]
        public int CampaignID { get; set; }
        public string CampaignName { get; set; }
        public string Status { get; set; }
        public string ParentType { get; set; }
        public string Type { get; set; }
        public string SubType { get; set; }
        public string Creative { get; set; }
        public string OfferCode { get; set; }
        public string Incentive { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Isactive { get; set; }
        public string Createdby { get; set; }
        public DateTime? Created_datetime { get; set; }
        public string Alteredby { get; set; }
        public DateTime? Altered_datetime { get; set; }
    }
}
