﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class tbl_m_ContractMst_FF
    {
        [Key]
        public int ContractID { get; set; }
        public int LeadID { get; set; }
        public int VerificationStatus { get; set; }
        public int VerifiedRepID { get; set; }
        public DateTime VerifiedDatetime { get; set; }
        public string Comments { get; set; }
        public DateTime ContractSignedDate { get; set; }
        public bool ISCreditScoreChecked { get; set; }
        public string UserCreated { get; set; }
        public DateTime DateCreated { get; set; }
        public string UserModified { get; set; }
        public DateTime DateModified { get; set; }
    }
}
