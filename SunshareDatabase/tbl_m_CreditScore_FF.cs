﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace SunshareDatabase
{
    public class tbl_m_CreditScore_FF
    {
        [Key]
        public int CreditID { get; set; }
        public int ContractID { get; set; }
        public int LeadID { get; set; }
        public string UtilityProvider { get; set; }
        public int ScoreRange { get; set; }
        public string CreditStatus { get; set; }
        public bool OverrideCredit { get; set; }
        public string OverrideComments { get; set; }
        public string OverrideBy { get; set; }
        public DateTime OverrideDatetime { get; set; }
        public bool ISUsageRequestSent { get; set; }
        public string CreditCheckBy { get; set; }
        public DateTime CreditCheckDatetime { get; set; }
        public string CreditCheckUpdatedBy { get; set; }
        public DateTime CreditCheckUpdatedDatetime { get; set; }
    }
}
