﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class tbl_m_UsageReceived_FF
    {
        [Key]
        public int UsageReceivedID { get; set; }
        public int UsageRequestID { get; set; }
        public int ContractID { get; set; }
        public int LeadID { get; set; }
        public string UtilityProvider { get; set; }
        public string ReceivedMailFrom { get; set; }
        public string ReceivedMailTo { get; set; }
        public string ReceivedMailSubject { get; set; }
        public string ReceivedMailBody { get; set; }
        public string ReceivedUsageDetails { get; set; }
        public bool ISGardenAllocated { get; set; }
        public string ReceivedBy { get; set; }
        public DateTime ReceivedDatetime { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime UpdatedDatetime { get; set; }
    }
}
