﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SunshareDatabase
{
    public class tbl_m_UsageRequest_FF
    {
        [Key]
        public int UsageRequestID { get; set; }
        public int ContractID { get; set; }
        public int LeadID { get; set; }
        public string UtilityProvider { get; set; }
        public string SentMailFrom { get; set; }
        public string SentMailTo { get; set; }
        public string SentMailSubject { get; set; }
        public string SentMailBody { get; set; }
        public bool ISReceivedUsageDetails { get; set; }
        public bool RequestSentBy { get; set; }
        public DateTime RequestSentDatetime { get; set; }
        public DateTime RequestUpdatedBy { get; set; }
        public string RequestUpdatedDatetime { get; set; }
    }
}
