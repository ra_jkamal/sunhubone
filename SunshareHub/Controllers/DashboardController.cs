﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using SunshareDatabase;
using SunshareHub.ModelAssembler;
using SunshareHub.sunshareservice;

namespace SunshareHub.Controllers
{
    
    public class DashboardController : Controller
    {
        private static readonly SunshareDatabaseContext Context=new SunshareDatabaseContext();
        readonly DashboardViewModelAssembler _assembler=new DashboardViewModelAssembler(Context);
        ISunshareServices service = new SunshareServicesClient();
        // GET: Dashboard
        public ActionResult Index()
        {
            return View("Dashboard",_assembler.Assemble(6));
        }

        public ActionResult Upload()
        {
            var fileUpload = new FileUploadMessage { FileName = "test.jpg" };

            var stream = System.IO.File.Open(@"C:/a11.jpg", FileMode.OpenOrCreate);

            Stream memStream = new MemoryStream();
            byte[] buffer = new Byte[10000];
            int bytesRead = 10000;
            while (bytesRead != 0)
            {
                bytesRead = stream.Read(buffer, 0, buffer.Length);
                memStream.Write(buffer, 0, bytesRead);
            }

            fileUpload.FileByteStream = memStream;
            fileUpload.UserId = 6;
            service.FileUpload(fileUpload);
            stream.Close();
            return View("Dashboard", _assembler.Assemble(6));
        }
    }
}