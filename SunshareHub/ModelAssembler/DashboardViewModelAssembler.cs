﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SunshareDatabase;
using SunshareHub.Models;

namespace SunshareHub.ModelAssembler
{
    public interface IDashBoardViewModelAssember
    {
        DashboardViewModel Assemble(int salesRepId);
    }
    public class DashboardViewModelAssembler : IDashBoardViewModelAssember
    {
        protected SunshareDatabaseContext Sunshare { get; set; }

        public DashboardViewModelAssembler(SunshareDatabaseContext context)
        {
            Sunshare = context;
        }
        public DashboardViewModel Assemble(int salesRepId)
        {
            var model=new DashboardViewModel();
            var myLeads = Sunshare.Leads.Where(l => l.IsActive & l.AspNetUsersId == salesRepId);
            
            foreach (var lead in myLeads)
            {
                var leadAddress =
                    Sunshare.Address.FirstOrDefault(la => la.IsActive & la.AddressId == lead.ServiceAddressId);
                var leadLeadStatusMapping = lead.LeadLeadStatusMappings.FirstOrDefault(l => l.IsCurrent & l.LeadId == lead.LeadId);
                model.Leads.Add(new LeadInformation 

                {
                    Name = $"{lead.FirstName} {lead.LastName}",
                    Email = lead.PrimaryEmail,
                    Phone = lead.HomePhoneNo,
                    //DateTime = lead.CreatedDate.ToString("yy-mm-dd"), //TODO: needs to be changed.
                    State = leadAddress?.State,
                    LeadStausId = leadLeadStatusMapping?.LeadStatusId ?? 0
                }
                    );
            }
           // throw new NotImplementedException();
            return model;
        }
    }
}
