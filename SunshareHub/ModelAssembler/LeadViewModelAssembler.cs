﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using SunshareDatabase;
using SunshareHub.Models;

namespace SunshareHub.ModelAssembler
{
    public interface ILeadViewModelAssembler
    {
        LeadViewModel Assemble(int salesRepId);
        LeadViewModel Assemble(int salesRepId, int leadId);
    }
    public class LeadViewModelAssembler : ILeadViewModelAssembler
    {
        protected SunshareDatabaseContext Sunshare { get; set; }
        public LeadViewModelAssembler(SunshareDatabaseContext context)
        {
            Sunshare = context;
        }
        public LeadViewModel Assemble(int salesRepId)
        {
            var model = new LeadViewModel
            {
                States = Sunshare.States.Where(s => s.IsActive)
                    .Select(s => new SelectListItem() {Text = s.StateName, Value = s.StateId.ToString()}),
                 
            };
            return model;
            //throw new NotImplementedException();
        }

        public LeadViewModel Assemble(int salesRepId, int leadId)
        {
            throw new NotImplementedException();
        }
    }
}
