﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SunshareHub.Models
{
    public class DashboardViewModel
    {
        public DashboardViewModel()
        {
            Leads=new List<LeadInformation>();
        }
        public ICollection<LeadInformation> Leads { get; set; }

        public int ContractSent { get; set; }

        public int ContractSigned { get; set; }

        public int ContractDeclined { get; set; }

        public int InformationSent { get; set; }
    }

    public class LeadInformation
    {
        public string Name { get; set; }
        public string Phone { get; set; }

        public string State { get; set; }

        public string Email { get; set; }
        public string DateTime { get; set; }

        public int LeadStausId { get; set; }
    }
}
