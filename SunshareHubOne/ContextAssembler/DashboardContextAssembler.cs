﻿using SunshareCommon;
using SunshareDatabase;

namespace SunshareHubOne.ContextAssembler
{
    public interface IDashboardContextAssembler
    {
        void AssignLeadToUser(Lead lead, int salesRepId);
        void MarkDead(Lead lead,string reason);

        int GetLeadContractStatus(Lead lead);
    }
    public class DashboardContextAssembler : IDashboardContextAssembler
    {
        public int GetLeadContractStatus(Lead lead)
        {
            var method = new Methods();
            var result = method.UpdateLeadDocuSignStatus(lead);
            return result;
        }
        public void AssignLeadToUser(Lead lead,int salesRepId)
        {
            lead.AspNetUsersId = salesRepId;
            lead.IsAssigned = true;
            lead.AssignHistory.Add(new LeadUserMappingHistory()
            {
                AspNetUserId = salesRepId
            });
        }

        public void MarkDead(Lead lead, string reason)
        {
            lead.InActiveReason = reason;
            lead.IsActive = false;
            lead.AllowDuplicateContract = false;
        }
    }
}
