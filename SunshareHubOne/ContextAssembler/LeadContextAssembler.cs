﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using SunshareCommon;
using SunshareDatabase;
using SunshareHubOne.Models;

namespace SunshareHubOne.ContextAssembler
{
    public interface ILeadContextAssembler
    {
        bool Assemble(LeadViewModel model, Lead lead, int options);
        bool upload(Lead lead, int options);
        void CloseExistingCallBack(Lead lead);
        void LogCall(string callComment,string bound, Lead lead, int callStatusId, DateTime? callDate = null, DateTime? time = null);
        void GetLeadContractStatus(Lead lead);
        bool SetDuplicateRequest(Lead lead, string reason,int repId);
    }
    public class LeadContextAssembler : ILeadContextAssembler
    {
        readonly SunshareDatabaseContext _context = new SunshareDatabaseContext();

        public bool upload(Lead lead, int options)
        {
            SaveLeadLeadStatusMapping(lead, LeadContractStatus.LeadCreated);
            return true;
        }
        public bool Assemble(LeadViewModel model, Lead lead, int options)
        {
            AssignLeadValues(model, lead);
            AssignServiceAddressValues(model, lead);
            AssignBillingAddressValues(model, lead);
            if (!model.IsEdit)
            {
                SaveLeadLeadStatusMapping(lead, LeadContractStatus.LeadCreated);
                SaveLeadAssigningHistory(lead, model.SalesRepId);
            }

            switch (options)
            {
                case ContractOptions.GenerateContract:
                    if (_context.DupliacteContractRequests.Any(d => d.LeadId == lead.LeadId))
                    {
                        lead.AllowDuplicateContract = false;
                        var duplicate = _context.DupliacteContractRequests.FirstOrDefault(d => d.LeadId == lead.LeadId);
                        duplicate.IsApproved = false;
                        duplicate.IsDecided = false;
                    }
                    SaveLeadLeadStatusMapping(lead, LeadContractStatus.ContractSent);

                    return true;
                case ContractOptions.SendInfo:
                    if (model.LeadStausId != LeadContractStatus.ContractSent)
                    {
                        SaveLeadLeadStatusMapping(lead, LeadContractStatus.CotractInformationSent);
                    }
                    return true;
                case ContractOptions.Clone:
                    lead.IsActive = false;
                    lead.InActiveReason = "Refused lead";
                    //lead.IsDuplicateLead = true;
                    SaveLeadLeadStatusMapping(lead, LeadContractStatus.RefusedLead);
                    return true;

            }
            return true;
        }

        public bool SetDuplicateRequest(Lead lead, string reason,int repId)
        {
            try
            {
                if (lead == null) return false;
                if (!lead.DupliacteContractRequests.All(l => l.IsDecided)) return false;
                lead.DupliacteContractRequests.Add(new DupliacteContractRequest()
                {
                    Reason = reason,
                    IsApproved = false,
                    IsDecided = false,
                    ReqRepId = repId, 
                });
                lead.IsAssigned = false;
                lead.AllowDuplicateContract = false;
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static void SaveLeadLeadStatusMapping(Lead lead, int leadStatus)
        {
            var currentLeadStatus = lead.LeadLeadStatusMappings.FirstOrDefault(l => l.IsCurrent);
            if (currentLeadStatus != null)
            {
                currentLeadStatus.IsCurrent = false;
            }
            lead.LeadLeadStatusMappings.Add(new LeadLeadStatusMapping
            {
                LeadStatusId = leadStatus,
                LeadId = lead.LeadId,
                IsCurrent = true,
                UpdatedDate = DateTime.Now
            });
        }

        private static void SaveLeadAssigningHistory(Lead lead, int? userId)
        {
            lead.AssignHistory.Add(new LeadUserMappingHistory
            {
                LeadId = lead.LeadId,
                AspNetUserId = userId
            });
        }



        public void LogCall(string callComment,string bound, Lead lead, int callStatusId, DateTime? callDate = null, DateTime? time = null)
        {

            //CloseExistingCallBack(lead);
            if (callStatusId == CallStatuses.CallBack)
            {
                var datetrim = callDate.ToString();
                datetrim = datetrim.Remove(datetrim.Length - 11);
                var timetrim = time.ToString();
                timetrim = timetrim.Remove(0, 11);
                var Datetime = datetrim + "" + timetrim;
                if (Datetime != null)
                {
                    var callLog = new CallLog
                    {
                        CallComment = callComment,
                        BoundStatusId=bound,
                        IsDuplicateCall = false,
                        CallStatusId = callStatusId,
                        Appointment = new CallAppointment
                        {
                            DateToCall = Convert.ToDateTime(Datetime),
                            DateCreated = DateTime.Now,
                            DateModified = DateTime.Now
                        },
                        IsActive = true
                    };
                    lead.CallLogs.Add(callLog);
                }
            }
            else
            {
                lead.CallLogs?.Add(new CallLog
                {
                    CallComment = callComment,
                    BoundStatusId = bound,
                    IsDuplicateCall = false,
                    CallStatusId = callStatusId
                });
                var leadLeadStatusMapping = lead.LeadLeadStatusMappings.FirstOrDefault(c => c.IsCurrent);
                if (leadLeadStatusMapping != null && (callStatusId == CallStatuses.DontCall || callStatusId == CallStatuses.NotInsterested || callStatusId == CallStatuses.Ineligible) && leadLeadStatusMapping.LeadStatusId != LeadContractStatus.ContractSigned)
                {
                    lead.IsActive = false;
                    lead.InActiveReason = callComment;
                }
            }
            lead.IsAssigned = false;
        }

        public void CloseExistingCallBack(Lead lead)
        {
            var callBacks = lead.CallLogs?.Where(c => c.CallStatusId == CallStatuses.CallBack);
            if (callBacks == null) return;
            var callLogs = callBacks as IList<CallLog> ?? callBacks.ToList();
            if (callLogs.Count == 0) return;
            foreach (var callBack in callLogs)
            {
                callBack.IsActive = false;
            }
        }

        private static void AssignLeadValues(LeadViewModel model, Lead lead)
        {
            lead.IsActive = true;
            if (!model.IsEdit)
            {
                lead.AspNetUsersId = (model.ChannelId == LeadChannels.WebEnrollmentAuto || model.ChannelId == LeadChannels.WebLanding || model.ChannelId == LeadChannels.Sunshare) ? null : model.SalesRepId;
            }
            else
            {
                lead.AspNetUsersId = (model.SalesRepId == 0 || model.SalesRepId == null) ? null : model.SalesRepId;
            }
            //lead.ServiceAddressId = address.AddressId;
            lead.FirstName = model.FirstName;
            lead.LastName = model.LastName;
            lead.DateOfBirth = model.DateOfBirth?.ToString(CultureInfo.InvariantCulture);
            lead.PrimaryEmail = model.Email;
            lead.CampaignId = model.CampaignId;
            lead.ChannelId = model.ChannelId ?? LeadChannels.Results;
            lead.RefferedBy = model.ReferedBy;
            lead.LeadRankingId = model.LeadRankingId ?? LeadRankings.Standard;
            lead.HomePhoneNo = model.Phone;
            lead.UtilityAccountNo = model.UtilityAccountNumber;
            lead.LeadSourceId = 2;
            lead.SubscriptionPercentage = Convert.ToInt32(model.Subscription);
            lead.UtilityCompanyName = model.UtilityProvider;
            lead.IsAssigned = false;
        }


        private void AssignServiceAddressValues(LeadViewModel model, Lead lead)
        {
            lead.LeadServiceAddress = new Address
            {
                IsActive = true,
                AppartmentNo = model.ServiceApartmentNo,
                AddressInformation = model.ServiceAddress,
                AddressSourceId = LeadAddressSources.TeleSales,
                City = model.ServiceCity,
                County = _context.Counties.FirstOrDefault(m => m.CountyId == model.SerCountyId)?.CountyName,
                State = _context.States.FirstOrDefault(s => s.StateId == model.SerStateId)?.StateName,
                Zipcode = Convert.ToString(model.SerZipCode)
            };
        }

        private void AssignBillingAddressValues(LeadViewModel model, Lead lead)
        {
            if (model.sameAsService)
            {
                lead.LeadBillingAddress = new Address
                {
                    IsActive = true,
                    AppartmentNo = model.ServiceApartmentNo,
                    AddressInformation = model.ServiceAddress,
                    AddressSourceId = LeadAddressSources.TeleSales,
                    City = model.ServiceCity,
                    County = _context.Counties.FirstOrDefault(m => m.CountyId == model.SerCountyId)?.CountyName,
                    State = _context.States.FirstOrDefault(s => s.StateId == model.SerStateId)?.StateName,
                    Zipcode = Convert.ToString(model.SerZipCode)
                };
            }
            else
            {
                lead.LeadBillingAddress = new Address
                {
                    IsActive = true,
                    AppartmentNo = model.BillingApartmentNo,
                    AddressInformation = model.BillingAddress,
                    AddressSourceId = LeadAddressSources.TeleSales,
                    City = model.BillingCity,
                    County = _context.Counties.FirstOrDefault(m => m.CountyId == model.BilCountyId)?.CountyName,
                    State = _context.States.FirstOrDefault(s => s.StateId == model.BilStateId)?.StateName,
                    Zipcode = Convert.ToString(model.BilZipCode)
                };
            }
        }
        public void GetLeadContractStatus(Lead lead)
        {
            var method = new Methods();
            method.UpdateLeadDocuSignStatus(lead);
        }
    }
}
