﻿using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SunshareHubOne.Models;
using System;
using System.Web.Hosting;
using SunshareDatabase;
using System.Data;
using System.Web.Script.Serialization;
using System.Web.Security;
using SunshareCommon;



namespace SunshareHubOne.Controllers
{
    [Authorize]
    public class AccountController : BaseController
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private ISunshareDatabaseContext _context;
       

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager, ISunshareDatabaseContext context)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            _context = context;
        }

        private ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            set
            {
                _signInManager = value;
            }
        }

        public ActionResult Acquistion()
        {
            var model = new PermissionModel();
            using (var entities = new SunshareDatabaseContext())
            {
                var roles = entities.UserInRoles.Where(r => r.UserId == User.Id);
                model.hasBillingAccess = roles.Any(r => r.RoleId == 1);
                model.hasHubOneAccess = roles.Any(r => r.RoleId == 2 || r.RoleId == 3 || r.RoleId == 4);
            }
            return View();
        }

        private ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            set
            {
                _userManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            var logOnModel = new LoginViewModel();
            var httpCookie = Request.Cookies["Login"];
            if (httpCookie != null)
            {
                var existingCookie = httpCookie["UserName"];
                if (existingCookie == null) return View();
                logOnModel.Email = httpCookie["UserName"];
                logOnModel.Password = httpCookie["UserPass"];
            }
            //logOnModel.RememberMe = true;
            //logOnModel.Password=
            return View(logOnModel);
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            if (model.RememberMe)
            {
                AuthenticationManager.SignIn(new AuthenticationProperties { IsPersistent = true });
                var cookies = new HttpCookie("Login");
                cookies.Values.Add("UserName", model.Email);
                cookies.Values.Add("UserPass", model.Password);
                cookies.Values.Add("Rememberme", model.RememberMe.ToString());
                Response.Cookies.Add(cookies);
            }

            var entities = new SunshareDatabaseContext();
            if (ModelState.IsValid)
            {
                var isValidUser = Membership.ValidateUser(model.Email, model.Password);
                if (isValidUser)
                {
                    //var memebrship = Membership.FirstOrDefault(m => m.EmailId == model.Email);
                    var user = entities.Users.First(u => u.Email == model.Email & u.PasswordHash==model.Password);

                    var serializeModel = new SunsharePrincipalSerializeModel
                    {
                        Id = (int)user.Id,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Roles = entities.UserInRoles.Where(u=>u.UserId==user.Id).Select(r=>r.UserRole.Name).ToList() //.Select(r => r.Role.Name).ToList()
                    };
                    var serializer = new JavaScriptSerializer();
                    var userData = serializer.Serialize(serializeModel);
                    var authTicket = new FormsAuthenticationTicket(1, model.Email, DateTime.Now, DateTime.Now.AddMinutes(15), false, userData);
                    var encTicket = FormsAuthentication.Encrypt(authTicket);
                    var faCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
                    Response.Cookies.Add(faCookie);
                    if (Url.IsLocalUrl(returnUrl))
                    {
                        return Redirect(returnUrl);
                    }
                    return RedirectToAction("Wheel", "Account");
                }
                ModelState.AddModelError("", "The user name or password provided is incorrect.");
            }
            ModelState.Remove("Password");
            return View();
        }

        [Authorize]
        public ActionResult Wheel()
        {
            
            return View("SunshareWheel");
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return await RedirectToLocal(model.ReturnUrl, null);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid) return View(model);
            var user = new ApplicationUser
            {
                UserName = model.Email,
                Email = model.Email,
                IsActive = model.IsActive,
                PartnerId = 1,
                UserTypeId = 1
            };
            var result = await UserManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                // string code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                // var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);
                // await UserManager.SendEmailAsync(user.Id, "Confirm your account", "Please confirm your account by clicking <a href=\"" + callbackUrl + "\">here</a>");
                return RedirectToAction("Index", "Dashboard", new { user.Id });
            }
            AddErrors(result);

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(int userId, string code)
        {
            if (code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    ModelState.AddModelError(string.Empty, "Email not registered/activated");
                    return View(model);
                    //return RedirectToAction("ForgotPasswordConfirmation", "Account");
                    // Don't reveal that the user does not exist or is not confirmed
                    //return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                if (Request.Url == null) return RedirectToAction("ForgotPasswordConfirmation", "Account");
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, username = user.UserName, code }, protocol: Request.Url.Scheme);
                var filePath = HostingEnvironment.MapPath("~/FileServer/Upload/PasswordResetTemplate.html");
                if (filePath == null) return RedirectToAction("ForgotPasswordConfirmation", "Account");
                var emailContent = System.IO.File.ReadAllText(filePath)
                    .Replace("{0}", $"{user.UserName}")
                    .Replace("{1}", callbackUrl);
                await UserManager.SendEmailAsync(user.Id, "Reset Password", Convert.ToString(emailContent));
                return RedirectToAction("ForgotPasswordConfirmation", "Account");
            }
            return View(model);
        }

        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code, string username)
        {
            return code == null ? View("Error") : View();
        }

        //[AllowAnonymous]
        //public ActionResult ResetPassword()
        //{
        //    return View();
        //}

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            var currentId = User.Identity.GetUserId();
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {

                ModelState.AddModelError(string.Empty, "Email not registered/activated");
                return View(model);
                // Don't reveal that the user does not exist
                //return RedirectToAction("ResetPasswordConfirmation", "Account");
            }

            if (currentId != null)
            {
                if (currentId != user.Id.ToString())
                {
                    ModelState.AddModelError(string.Empty, "Wrong email");
                    return View(model);
                }
                var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var result = await UserManager.ResetPasswordAsync(user.Id, code, model.Password);
                if (result.Succeeded)
                {
                    return RedirectToAction("ResetPasswordConfirmation", "Account");
                }
                AddErrors(result);
                return View();
            }
            else
            {
                var mail = model.username;

                if (mail != model.Email)
                {
                    ModelState.AddModelError(string.Empty, "Wrong email");
                    return View(model);
                }
                var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var result = await UserManager.ResetPasswordAsync(user.Id, code, model.Password);
                if (result.Succeeded)
                {
                    return RedirectToAction("ResetPasswordConfirmation", "Account");
                }
                AddErrors(result);
                return View();
            }
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, model.ReturnUrl, model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return await RedirectToLocal(returnUrl, null);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return await RedirectToLocal(returnUrl, user.Email);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Login", "Account");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private async Task<ActionResult> RedirectToLocal(string returnUrl, string email)
        {
            if (email == null) return RedirectToAction("Login", "Account");
            var user = await UserManager.FindByNameAsync(email);
            var userId = user.Id;

            if (user.Roles.Any(rol => rol.RoleId == 2))
            {
                return RedirectToAction("Manage", "Dashboard");
            }
            else if (user.Roles.Any(rol => rol.RoleId == 4))
            {
                return RedirectToAction("Index", "Dashboard");
            }
            else
            {
                return RedirectToAction("Index", "Dashboard");
            }
        }


      

        public ActionResult AdminRespondRequest(int duplicateId, bool isApproved, string adminuser, string adminpassword, string reason, int salesRepId, int leadId, string Requestdate)
        {
            var result = Membership.ValidateUser(adminuser,adminpassword);
            
            if (result)
            {
                _context = new SunshareDatabaseContext();
                var signedUser = _context.Users.FirstOrDefault(u => u.Email == adminuser & u.PasswordHash == adminpassword);
                var isAdmin = _context.UserInRoles.Where(r => r.UserId == signedUser.Id).Any(r => r.UserRole.Name == "Executive Admin" || r.UserRole.Name == "Manager");
                return RedirectToAction("AdminRespondRequest", "Lead",
                    new
                    {
                        duplicateId = duplicateId,
                        isApproved = isApproved,
                        adminuser = adminuser,
                        adminpassword = adminpassword,
                        reason = reason,
                        salesRepId = salesRepId,
                        leadId = leadId,
                        Requestdate = Requestdate,
                        isAdminLogged = isAdmin
                    });
            }
            return RedirectToAction("AdminRespondMessage", "Lead");
            //    switch (result)
            //    {
            //        case SignInStatus.Success:
            //        return RedirectToAction("AdminRespondRequest", "Lead", new { duplicateId= duplicateId, isApproved= isApproved, adminuser= adminuser, adminpassword= adminpassword, reason= reason, salesRepId= salesRepId, leadId= leadId, Requestdate= Requestdate });

            //    default:
            //        return RedirectToAction("AdminRespondMessage", "Lead");
            //}
        }




        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            private string LoginProvider { get; }
            private string RedirectUri { get; }
            private string UserId { get; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}