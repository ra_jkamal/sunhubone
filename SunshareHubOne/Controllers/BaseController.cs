﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SunshareHubOne.Controllers
{
    public class BaseController : Controller
    {
        protected virtual new SunsharePrincipal User
        {
            get { return HttpContext.User as SunsharePrincipal; }
        }
    }
}