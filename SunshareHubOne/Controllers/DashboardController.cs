﻿using System;
using System.Linq;
using System.Net.Mime;
using System.Web.Mvc;
using SunshareDatabase;
using SunshareHubOne.ModelAssembler;
using SunshareHubOne.Models;
using SunshareHubOne.ContextAssembler;
using SunshareCommon;


namespace SunshareHubOne.Controllers
{
    [Authorize]
    public class DashboardController : BaseController
    {
       
        private readonly ISunshareDatabaseContext _context;
        private readonly IDashBoardViewModelAssember _dashBoardViewModelAssember;
        private readonly IDashboardContextAssembler _dashboardContextAssembler;
        

        public DashboardController(ISunshareDatabaseContext context,IDashBoardViewModelAssember dashBoardViewModelAssember,IDashboardContextAssembler dashboardContextAssembler)//ApplicationSignInManager signInManager
        {
            
            _dashboardContextAssembler = dashboardContextAssembler;
            _context = context;
            _dashBoardViewModelAssember = dashBoardViewModelAssember;
        }

        public ViewResult Index()
        {
            ModelState.Clear();
            var salesRep = _context.Users.FirstOrDefault(u => u.IsActive && u.Id == User.Id);
            var model = _dashBoardViewModelAssember.Assemble(salesRep, User.IsInRole("Executive Admin") || User.IsInRole("Manager"));
            return (User.IsInRole("Sales Rep")) ? View("Dashboard", model) : View("Administrate", model);
        }

        //[HttpGet]
        //public ViewResult Index()
        //{
        //    Response.AddHeader("Refresh", "900");
        //    ModelState.Clear();
        //    var salesRep = _context.Users.FirstOrDefault(u => u.IsActive && u.Id == User.Id);
        //    var model = _dashBoardViewModelAssember.Assemble(salesRep);
        //    return View("Dashboard", model);
        //}

        //[HttpGet]
        //public ViewResult IndexAdmin()
        //{
        //    Response.AddHeader("Refresh", "900");
        //    ModelState.Clear();
        //    var salesRep = _context.Users.FirstOrDefault(u => u.IsActive && u.Id == User.Id);
        //    var model = _dashBoardViewModelAssember.Assemble(salesRep, true);
        //    return View("Administrate", model);
        //}

        [HttpGet]
        public ViewResult Dashboard()
        {
            Response.AddHeader("Refresh", "900");
            ModelState.Clear();
            var salesRep = _context.Users.FirstOrDefault(u => u.IsActive && u.Id == User.Id);
            var model = _dashBoardViewModelAssember.Assemble(salesRep, true);
            return View("HomeDashboard", model);
        }

        [HttpGet]
        public ViewResult Manage()
        {
            Response.AddHeader("Refresh", "900");
            ModelState.Clear();
            var salesRep = _context.Users.FirstOrDefault(u => u.IsActive && u.Id == User.Id);
            var model = _dashBoardViewModelAssember.AssembleToManage(salesRep, true);
            return View("Administrate", model);
        }

        [HttpGet]
        public ViewResult IndexAdminDormet()
        {
            Response.AddHeader("Refresh", "900");
            ModelState.Clear();
            var salesRep = _context.Users.FirstOrDefault(u => u.IsActive && u.Id == User.Id);
            var model = _dashBoardViewModelAssember.Assemble(salesRep, true,true);
            return View("Dormet", model);
        }

    

        public ActionResult Createdata()
        {
            Response.AddHeader("Refresh", "900");
            ModelState.Clear();
            var user = _context.Users.FirstOrDefault(u => u.Id == User.Id);
            var leadstatusMapping = _context.LeadLeadStatusMappings.Where(u => u.LeadStatusId == LeadContractStatus.LeadCreated && u.UserCreated == user.Email).Select(i => i.LeadId).ToList();
            var leads = _context.Leads.Where(l => leadstatusMapping.Contains(l.LeadId) & l.IsActive).ToList();
            var email = user.Email;
            var model = _dashBoardViewModelAssember.Assemblecreate(leads, user);
            return View("CreateandClose", model);
        }

        public ActionResult Closeddata()
        {
            Response.AddHeader("Refresh", "900");
            ModelState.Clear();
            var user = _context.Users.FirstOrDefault(u => u.Id == User.Id);
            var leadstatusMapping = _context.LeadLeadStatusMappings.Where(u => u.LeadStatusId == LeadContractStatus.ContractSigned && u.UserCreated == user.Email & u.IsCurrent).Select(i => i.LeadId).ToList();
            var leads = _context.Leads.Where(l => leadstatusMapping.Contains(l.LeadId) & l.IsActive).ToList();
            var email = user.Email;
            var model = _dashBoardViewModelAssember.Assembleclose(leads,user);
            return View("CreateandClose", model);
        }

        public ActionResult RefreshAll()
        {
            var rep = _context.Users.FirstOrDefault(l => l.Id == User.Id && l.IsActive);
            if (rep != null)
            {
                foreach (var lead in rep.Leads)
                {
                    _dashboardContextAssembler.GetLeadContractStatus(lead);
                }
                _context.SaveChanges();
            }
            var user = _context.Users.FirstOrDefault(u => u.Id == User.Id);
            var model = _dashBoardViewModelAssember.Assemble(user, User.IsInRole("Executive Admin") || User.IsInRole("Manager"));
            return View(User.IsInRole("Executive Admin") || User.IsInRole("Manager") ? "Administrate" : "Dashboard", model);
        }

        public ActionResult Search(string searchText)
        {
            ModelState.Clear();
            var admin= User.IsInRole("Executive Admin") || User.IsInRole("Manager");
            var salesRep = _context.Users.FirstOrDefault(u => u.IsActive && u.Id == User.Id);
            var repstate = _context.UserInRoles.FirstOrDefault(u=>u.UserId==User.Id);
            var model = _dashBoardViewModelAssember.AssembleSearch(salesRep, searchText.Trim(),admin);
            model.SearchText = searchText;
            return PartialView("_SearchLeads", model);
        }

        public ActionResult Assign(int leadId)
        {
            var lead = _context.Leads.FirstOrDefault(l => l.LeadId == leadId);
            var modal = _dashBoardViewModelAssember.AssembleAssignInfo(lead, User.Id);
            return PartialView("_AssignToUser", modal);
        }

        public ActionResult AdminAssign(int leadId)
        {
            var lead = _context.Leads.FirstOrDefault(l => l.LeadId == leadId);
            var modal = _dashBoardViewModelAssember.AssembleAssignInfo(lead, User.Id,true);
            modal.IsAdmin = true;
            return PartialView("_AssignToUser", modal);
        }
        
        public ActionResult AdminAssignInfo(string leadIds)
        {
            if (!string.IsNullOrEmpty(leadIds))
            {
                var modal = _dashBoardViewModelAssember.AssembleAssignInfo(leadIds, User.Id, true);
                modal.IsAdmin = true;
                return PartialView("_AssignToUser", modal);
            }
            var salesRep = _context.Users.FirstOrDefault(u => u.IsActive && u.Id == User.Id);
            var model = _dashBoardViewModelAssember.Assemble(salesRep, true);
            return View("Administrate", model);
        }

        [HttpPost]
        public ViewResult Assign(RepInformation modal)
        {
            var salesRep = _context.Users.FirstOrDefault(u => u.IsActive & u.Id == modal.SalesRepId );
            if (!string.IsNullOrEmpty(modal.LeadIdCollection))
            {
                var leadIdCollection = modal.LeadIdCollection.Split(',').ToList();
                if (leadIdCollection.Count > 0)
                    foreach (
                        var lead in
                            leadIdCollection.Select(leadId => Convert.ToInt32(leadId))
                                .Select(id => _context.Leads.FirstOrDefault(l => l.LeadId == id)))
                    {
                        _dashboardContextAssembler.AssignLeadToUser(lead, modal.SelectedRepId);
                    }
            }
            else
            {
                var lead = _context.Leads.FirstOrDefault(l => l.IsActive & l.LeadId == modal.LeadId);
                _dashboardContextAssembler.AssignLeadToUser(lead, modal.SelectedRepId);
            }
            _context.SaveChanges();
            var model = _dashBoardViewModelAssember.Assemble(salesRep, modal.IsAdmin);
            return View(modal.IsAdmin ? "Administrate" : "Dashboard", model);
        }

        [HttpPost]
        public ActionResult MarkDead(string leadIds, string reason)
        {
            if (string.IsNullOrEmpty(leadIds)) return Content("No lead been selected", MediaTypeNames.Text.RichText);
            var leadIdCollection = leadIds.Split(',').ToList();
            foreach (var lead in leadIdCollection.Select(leadId => Convert.ToInt32(leadId)).Select(id => _context.Leads.FirstOrDefault(l => l.LeadId == id)))
            {
                _dashboardContextAssembler.MarkDead(lead, reason);
            }
            _context.SaveChanges();
            var salesRep = _context.Users.FirstOrDefault(u => u.IsActive && u.Id == User.Id);
            var model = _dashBoardViewModelAssember.Assemble(salesRep, true);
            return View(User.IsInRole("Executive Admin") || User.IsInRole("Manager") ? "Administrate" : "Dashboard", model);
        }


        public ViewResult RequestOpen()
        {
            var model = _dashBoardViewModelAssember.AssembleToOpenDuplicate(User.Id,User.IsInRole("Executive Admin") || User.IsInRole("Manager"));
            return View("Duplicate", model);
        }

        public ViewResult RequestClosed()
        {
            var model = _dashBoardViewModelAssember.AssembleToClosedDuplicate(User.Id, User.IsInRole("Executive Admin") || User.IsInRole("Manager"));
            return View("Duplicate", model);
        }

        public ViewResult RequestAdminOpenAssemble()
        {
            var model = _dashBoardViewModelAssember.AssembleToOpenDuplicate(User.Id, User.IsInRole("Executive Admin") || User.IsInRole("Manager"));
            return View("supervisor", model);
        }

        public ViewResult RequestAdminClosedAssemble()
        {
            var model = _dashBoardViewModelAssember.AssembleToClosedDuplicate(User.Id, User.IsInRole("Executive Admin") || User.IsInRole("Manager"));
            return View("supervisor", model);
        }
        public ViewResult RequestMasterAssemble()
        {
            var model = _dashBoardViewModelAssember.AssembleManageDuplicate(User.Id, User.IsInRole("Executive Admin") || User.IsInRole("Manager"));
            return View("supervisor", model);
        }

        public PartialViewResult RespondRequest(int duplicateId, bool isApproved)
        {
            var duplicate = _context.DupliacteContractRequests.FirstOrDefault(d => d.DupliacteContractRequestId == duplicateId);
            if (duplicate != null)
            {
                duplicate.Lead.AllowDuplicateContract = isApproved;
                duplicate.IsApproved = isApproved;
                duplicate.IsDecided = true;
                _context.SaveChanges();
            }
            var model = _dashBoardViewModelAssember.AssembleToManageDuplicate(User.Id, User.IsInRole("Executive Admin") || User.IsInRole("Manager"));
                return PartialView("_Duplicate", model);
        }


        public PartialViewResult IndexRespondRequest(int duplicateId, bool isApproved)
        {
            var duplicate = _context.DupliacteContractRequests.FirstOrDefault(d => d.DupliacteContractRequestId == duplicateId);
            if (duplicate != null)
            {
                duplicate.Lead.AllowDuplicateContract = isApproved;
                duplicate.IsApproved = isApproved;
                duplicate.IsDecided = true;
                _context.SaveChanges();
            }
            var model = _dashBoardViewModelAssember.AssembleToManageDuplicate(User.Id, User.IsInRole("Executive Admin") || User.IsInRole("Manager"));
            return PartialView("_index", model);
        }

        public PartialViewResult ManagerRespondRequest(int duplicateId, bool isApproved)
        {
            var duplicate =
                _context.DupliacteContractRequests.FirstOrDefault(d => d.DupliacteContractRequestId == duplicateId);
            if (duplicate != null)
            {
                duplicate.Lead.AllowDuplicateContract = isApproved;
                duplicate.IsApproved = isApproved;
                duplicate.IsDecided = true;
                _context.SaveChanges();
            }
            var model = _dashBoardViewModelAssember.AssembleToManageDuplicate(User.Id, User.IsInRole("Executive Admin") || User.IsInRole("Manager"));
            return PartialView("_supervisor", model);
        }



        public ActionResult AssignToMe(int leadId,string searchText)
        {
            var lead = _context.Leads.FirstOrDefault(l => l.IsActive & l.LeadId == leadId);
            var salesRep = _context.Users.FirstOrDefault(u => u.IsActive && u.Id == User.Id);
            _dashboardContextAssembler.AssignLeadToUser(lead, User.Id);
            _context.SaveChanges();
            return PartialView("_SearchLeads", _dashBoardViewModelAssember.AssembleSearch(salesRep, searchText));
        }
    }
}