﻿using System.Data.OleDb;
using System.Web.Mvc;
using SunshareDatabase;

namespace SunshareHubOne.Controllers
{
    [Authorize]
    public class DataMigrationController : Controller
    {
        private readonly ISunshareDatabaseContext _context;

        public DataMigrationController(ISunshareDatabaseContext context)
        {
            _context = context;
        }
        // GET: DataMigration
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Importexcel()
        {
            var httpPostedFileBase = Request.Files[" fileupload1"];
            if (httpPostedFileBase == null || httpPostedFileBase.ContentLength <= 0) return RedirectToAction("Index");
            var path1 = $"{Server.MapPath("~/Content/UploadedFolder")}/{httpPostedFileBase.FileName}";
            if (System.IO.File.Exists(path1))
                System.IO.File.Delete(path1);
            httpPostedFileBase.SaveAs(path1);
            //const string sqlConnectionString = @"Data Source=216.15.146.34\MSSQLSERVER2014;Initial Catalog=SunshareDatabase_DevData;Integrated Security=False;User Id=sa;Password=welcome@123;MultipleActiveResultSets=True";
            //Create connection string to Excel work book
            var excelConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path1 + ";Extended Properties=Excel 12.0;Persist Security Info=False";
            //Create Connection to Excel work book
            var excelConnection = new OleDbConnection(excelConnectionString);
            //Create OleDbCommand to fetch data from Excel
            var cmd = new OleDbCommand("Select [Id],[Email],[FirstName],[LastName],[PartnerId],[UserTypeId],[ImageUrl] from [Sheet1$]", excelConnection);
            excelConnection.Open();
            var dReader = cmd.ExecuteReader();
            if (dReader != null && dReader.HasRows)
            {
                while (dReader.Read())
                {
                   var user= _context.Users.Add(new AspNetUsers
                    {
                        Id = dReader.GetInt32(0),
                        Email = dReader.GetString(1),
                        EmailConfirmed = true,
                        UserName = dReader.GetString(1),
                        FirstName = dReader.GetString(2),
                        LastName = dReader.GetString(3),
                        PartnerId = dReader.GetInt32(4),
                        UserTypeId = dReader.GetInt32(5),
                        IsActive = true,
                        ImageUrl = dReader.GetString(6)
                    });

                    _context.UserInRoles.Add(new AspNetUserRoles
                    {
                        RoleId = dReader.GetInt32(7),
                        UserId = user.Id
                    });
                }
            }
            _context.SaveChanges();
            //var sqlBulk = new SqlBulkCopy(sqlConnectionString) {DestinationTableName = "AspNetUsers" };
            //Give your Destination table name
            //sqlBulk.WriteToServer(dReader);
            excelConnection.Close();
            return RedirectToAction("Index");
        }
    }
}