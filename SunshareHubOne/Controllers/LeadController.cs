﻿using System.Web.Mvc;
using SunshareCommon;
using SunshareDatabase;
using SunshareHubOne.ContextAssembler;
using SunshareHubOne.ModelAssembler;
using SunshareHubOne.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using NPOI.SS.UserModel;
using System.IO;

using Address = SunshareDatabase.Address;

namespace SunshareHubOne.Controllers
{
    [Authorize]
    public class LeadController : BaseController
    {
        private readonly ISunshareDatabaseContext _context;
        private readonly ILeadViewModelAssembler _leadViewModelAssembler;
        private readonly ILeadContextAssembler _leadContextAssembler;
        readonly Methods _methods = new Methods();

        public LeadController()
        {

        }
        public LeadController(ISunshareDatabaseContext context, ILeadViewModelAssembler modelAssembler,
            ILeadContextAssembler contextAssembler)
        {
            _context = context;
            _leadViewModelAssembler = modelAssembler;
            _leadContextAssembler = contextAssembler;
        }

        [HttpGet]
        public ViewResult Index()
        {
            ModelState.Clear();
            var model = _leadViewModelAssembler.Assemble(User.Id, User.IsInRole("Executive Admin") || User.IsInRole("Manager"));
            return View(model);
        }

        [HttpPost]
        public ActionResult GetCounty(string stateId)
        {
            var districtNames = new List<SelectListItem>();
            if (string.IsNullOrEmpty(stateId)) return Json(districtNames, JsonRequestBehavior.AllowGet);
            var statId = Convert.ToInt32(stateId);
            var districts = _context.Counties.Where(x => x.StateId == statId).ToList();
            districts.ForEach(x =>
            {
                districtNames.Add(new SelectListItem { Text = x.CountyName, Value = x.CountyId.ToString() });
            });
            return Json(districtNames.OrderBy(o=>o.Text), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCampaign(string campaignId)
        {
            var districtNames = new List<SelectListItem>();
            if (string.IsNullOrEmpty(campaignId)) return Json(districtNames, JsonRequestBehavior.AllowGet);
            var campId = Convert.ToInt32(campaignId);
            var districts = _context.CampaignDetails.FirstOrDefault(x => x.CampaignID == campId);
            return Json(districts, JsonRequestBehavior.AllowGet);
        }

        public ViewResult Edit( int leadId)
        {
            ModelState.Clear();
            var model = _leadViewModelAssembler.Assemble(User.Id, leadId, User.IsInRole("Executive Admin") || User.IsInRole("Manager"));
            model.IsEdit = true;
            return View("Index", model);
        }

        public PartialViewResult ParEdit( int leadId)
        {
            ModelState.Clear();
            var model = _leadViewModelAssembler.Assemble(User.Id, leadId, User.IsInRole("Executive Admin") || User.IsInRole("Manager"));
            model.IsEdit = true;
            return PartialView("_index", model);
        }

        [HttpPost]
        public ActionResult MultipleCommand(LeadViewModel model, string command)
        {
            Lead lead = null;
            var user = _context.Users.FirstOrDefault(u => u.IsActive & u.Id == model.SalesRepId);
            var repId = model.SalesRepId;
            var lId = model.LeadId;
            ModelState.Clear();
            if (command == Messages.SubmitTypeClear)
                return (!model.IsAdmin) ? RedirectToAction("Index", "DashBoard", new { Id = repId }) : RedirectToAction("Index", "DashBoard");
            if (!ModelState.IsValid)
            {
                TempData[Messages.ErrorTempData] = "Record cannot be saved";
                return lId == 0 ? RedirectToAction("Index", "Lead") : RedirectToAction("Edit", "Lead", new { leadId = lId, isAdmin = model.IsAdmin });
            }
            if (model.LeadId == 0)
            {
                if (!_context.Leads.Any(l => (l.PrimaryEmail == model.Email || l.SecondaryEmail == model.Email || l.HomePhoneNo == model.Phone) & ((l.IsActive & l.IsDuplicateLead) ||l.IsActive))) //((l.IsActive & l.IsDuplicateLead)
                {
                    lead = _context.Leads.Add(new Lead());
                }
                else
                {
                    ModelState.AddModelError(Messages.ErrorTempData, Messages.LeadWithEmailExisits);
                    TempData[Messages.ErrorTempData] = Messages.LeadWithEmailExisits;
                }
            }
            else
            {
                if (!_context.Leads.Any(
                    l =>
                        (l.PrimaryEmail == model.Email || l.SecondaryEmail == model.Email || l.HomePhoneNo == model.Phone) &
                        l.LeadId != model.LeadId & ((l.IsActive & l.IsDuplicateLead) || l.IsActive)))
                {
                    lead = _context.Leads.FirstOrDefault(l => l.LeadId == model.LeadId);
                    model.IsEdit = true;
                }
                else
                {
                    TempData[Messages.ErrorTempData] = Messages.LeadWithEmailExisits;
                }
            }
            if (lead == null)
                return lId == 0
                    ? RedirectToAction("Index", "Lead")
                    : RedirectToAction("Edit", "Lead", new { leadId = lId});
            lId = lead.LeadId;
            bool result;
            switch (command)
            {
                case Messages.SubmitTypeSave:
                    result = _leadContextAssembler.Assemble(model, lead, ContractOptions.SaveLead);
                    _context.SaveChanges();
                    lId = lead.LeadId;
                    if (result)
                    {
                        TempData[Messages.SuccessTempdata] = model.IsEdit
                            ? Messages.LeadUpdatedSuccessfully
                            : Messages.LeadCreatedSuccessfully;
                    }
                    else
                        TempData[Messages.ErrorTempData] = Messages.LeadWithEmailExisits;
                    break;
                case Messages.SubmitTypeInfo:
                    result = _leadContextAssembler.Assemble(model, lead, ContractOptions.SendInfo);
                    if (result)
                    {
                        _context.SaveChanges();
                        lId = lead.LeadId;
                        _methods.SendInformation(lId);
                        ModelState.AddModelError(Messages.ErrorTempData, Messages.InfoSentSuccessfully);
                        TempData[Messages.SuccessTempdata] = Messages.InfoSentSuccessfully;
                    }
                    else
                        TempData[Messages.ErrorTempData] = Messages.LeadWithEmailExisits;
                    break;
                case Messages.SubmitTypeFixedClone:
                    result = _leadContextAssembler.Assemble(model, lead, ContractOptions.Clone);
                    _context.SaveChanges();
                    lId = lead.LeadId;
                    var data = _context.Leads.Where(x => x.LeadId == lId).Select(x => x).FirstOrDefault();
                    var newlead = _context.Leads.Add(new Lead());
                    newlead.PrimaryEmail = data.PrimaryEmail;
                    newlead.SecondaryEmail = data.SecondaryEmail;
                    newlead.FirstName = data.FirstName;
                    newlead.LastName = data.LastName;
                    newlead.DateOfBirth = data.DateOfBirth;
                    newlead.UtilityAccountNo = data.UtilityAccountNo;
                    newlead.UtilityCompanyName = data.UtilityCompanyName;
                    newlead.HomePhoneNo = data.HomePhoneNo;
                    newlead.WorkPhoneNo = data.WorkPhoneNo;
                    newlead.MobileNo = data.MobileNo;
                    newlead.ServiceAddressId = data.ServiceAddressId;
                    newlead.BillingAddressId = data.BillingAddressId;
                    newlead.RefferedBy = data.RefferedBy;
                    newlead.SubscriptionPercentage = data.SubscriptionPercentage;
                    newlead.SSAType = data.SSAType;
                    newlead.AspNetUsersId = data.AspNetUsersId;
                    newlead.ChannelId = data.ChannelId;
                    newlead.CampaignId = data.CampaignId;
                    newlead.LeadRankingId = data.LeadRankingId;
                    newlead.LandingPageId = data.LeadRankingId;
                    newlead.LeadSourceId = data.LeadSourceId;
                    newlead.EnvelopeId = data.EnvelopeId;
                    newlead.TrancientId = data.TrancientId;
                    newlead.ReasonToClose = data.ReasonToClose;
                    newlead.InActiveReason = null;
                    newlead.IsActive = true;
                    newlead.IsAssigned = data.IsAssigned;
                    newlead.AllowDuplicateContract = data.AllowDuplicateContract;
                    newlead.IsDuplicateLead = data.IsDuplicateLead;
                    newlead.DateCreated = data.DateCreated;
                    newlead.UserCreated = data.UserCreated;
                    newlead.DateModified = data.DateModified;
                    newlead.UserModified = data.UserModified;
                    newlead.LeadState = data.LeadState;
                    newlead.LeadCounty = data.LeadCounty;
                    _context.Leads.Add(newlead);
                    var status = _context.LeadLeadStatusMappings.Where(x => x.LeadId == lId).Select(x => x).FirstOrDefault();
                    var newmapping = _context.LeadLeadStatusMappings.Add(new LeadLeadStatusMapping());
                    newmapping.LeadStatusId = LeadContractStatus.LeadCreated;
                    newmapping.UpdatedDate = status.UpdatedDate;
                    newmapping.IsCurrent = true;
                    newmapping.DateCreated = status.DateCreated;
                    newmapping.UserCreated = status.UserCreated;
                    newmapping.DateModified = status.DateModified;
                    newmapping.UserModified = status.UserModified;
                    _context.LeadLeadStatusMappings.Add(newmapping);
                    _context.SaveChanges();
                    lId = lead.LeadId;
                    TempData[Messages.SuccessTempdata] = Messages.CloneCreatedSuccessfully;
                    break;
                case Messages.SubmitTypeContract:
                case Messages.SubmitTypeReContract:
                    result = _leadContextAssembler.Assemble(model, lead, ContractOptions.GenerateContract);
                    if (result)
                    {
                        _context.SaveChanges();
                    }
                        var leadManager = _context.Leads.FirstOrDefault(l => l.LeadId == lead.LeadId);
                    var Manager = _context.UserInRoles.FirstOrDefault(r => r.UserId == model.SalesRepId);
                    if (leadManager.DupliacteContractRequests.Any(d => d.LeadId == lead.LeadId))
                    {
                        var duplicate = leadManager.DupliacteContractRequests.LastOrDefault(d => d.LeadId == model.LeadId);
                        if (!duplicate.IsApproved || !duplicate.IsDecided)
                        {
                            if (Manager.RoleId == 2)
                            {
                                if (duplicate != null)
                                {
                                    duplicate.Lead.AllowDuplicateContract = true;
                                    duplicate.IsApproved = true;
                                    duplicate.IsDecided = true;
                                    _context.SaveChanges();
                                    ModelState.Clear();
                                    var mod = _leadViewModelAssembler.Assemble(model.SalesRepId, model.LeadId, model.IsAdmin);
                                    mod.SalesRepId = model.SalesRepId;
                                    mod.IsEdit = true;
                                }
                            }
                        }
                    }
                    result = _leadContextAssembler.Assemble(model, lead, ContractOptions.GenerateContract);
                    if (result)
                    {
                        _context.SaveChanges();
                        lId = lead.LeadId;
                        _methods.ContractReturn(lead, lead.LeadServiceAddress, user, null, false);
                        TempData[Messages.SuccessTempdata] = Messages.ContractGeneratedSuccessfully;
                    }
                    else
                        TempData[Messages.ErrorTempData] = Messages.LeadWithEmailExisits;
                    break;
            }
            //return lId == 0 ? RedirectToAction("Index", "Lead", new {Id = repId}) : RedirectToAction("Edit", "Lead", new {salesRepId = repId, leadId = lId, isAdmin = model.IsAdmin});
            return RedirectToAction("Edit", "Lead", new { leadId = lId });
        }

        public ActionResult Refresh(int leadid)
        {
            var lead = _context.Leads.FirstOrDefault(l => l.LeadId == leadid && l.IsActive);
            _leadContextAssembler.GetLeadContractStatus(lead);
            _context.SaveChanges();
            var model = _leadViewModelAssembler.Assemble(User.Id, leadid, User.IsInRole("Executive Admin") || User.IsInRole("Manager"));
            model.IsEdit = true;
            return View("Index", model);
        }

        [HttpPost]
        public PartialViewResult CallLog(string callComment, string bound, int leadId, int callStatusId, string callDate, DateTime? time)
        {
            var lead = _context.Leads.FirstOrDefault(l => l.LeadId == leadId);
            var date = string.IsNullOrEmpty(callDate) ? (DateTime?)null : Convert.ToDateTime(callDate, System.Globalization.CultureInfo.GetCultureInfo("en-US").DateTimeFormat);
            if (callStatusId != CallStatuses.NoAnswer)
                _leadContextAssembler.CloseExistingCallBack(lead);
            _context.SaveChanges();
            _leadContextAssembler.LogCall(callComment, bound, lead, callStatusId, date, time);
            _context.SaveChanges();
            return PartialView("_CallLogHistory", _leadViewModelAssembler.Assemble(User.Id, leadId, User.IsInRole("Executive Admin") || User.IsInRole("Manager")));
        }

        public string CheckEmail(string input, int leadId)
        {
            if (string.IsNullOrEmpty(input)) return string.Empty;
            var ifuser = (leadId != 0) ? _context.Leads.Any(l => (l.PrimaryEmail == input || l.SecondaryEmail == input) & l.IsActive & l.LeadId != leadId) :
                _context.Leads.Any(l => (l.PrimaryEmail == input || l.SecondaryEmail == input) & l.IsActive);
            return ReturnAvailability(ifuser, "Email", "Email already exist");
        }

        public string CheckPhone(string input, int leadId)
        {
            if (string.IsNullOrEmpty(input)) return string.Empty;
            var ifuser = (leadId != 0) ? _context.Leads.Any(l => (l.WorkPhoneNo == input || l.HomePhoneNo == input) & l.IsActive & l.LeadId != leadId) :
                _context.Leads.Any(l => (l.WorkPhoneNo == input || l.HomePhoneNo == input) & l.IsActive);
            return ReturnAvailability(ifuser, "Phone", "Phone already exist");
        }

        public string AdminRespondMessage()
        {
            var ifuser = false;
            return ReturnAvailability(ifuser, "adminpassword", "Please enter valid EmailId & Password.");
        }

        public string AdminRespondRequest(int duplicateId, bool isApproved, string adminuser, string adminpassword, string reason, int leadId, string Requestdate,bool isAdminLogged=false)
        {
            var user = _context.Users.FirstOrDefault(a => a.Email == adminuser);
            var isAdmin = isAdminLogged;
            var model = _leadViewModelAssembler.Assemble(User.Id, leadId, isAdmin);
            model.IsEdit = true;
            if (isAdmin)
            {
                var lead = _context.Leads.FirstOrDefault(l => l.LeadId == leadId);
                lead.DupliacteContractRequests.Add(new DupliacteContractRequest()
                {
                    Reason = reason,
                    IsApproved = false,
                    IsDecided = false,
                    ReqRepId = User.Id,
                    
                });
                lead.IsAssigned = false;
                lead.AllowDuplicateContract = false;
                _context.SaveChanges();
                // Accept Request
                var duplicate = _context.DupliacteContractRequests.FirstOrDefault(d => d.LeadId == leadId && !d.IsDecided);
                duplicate.Lead.AllowDuplicateContract = isApproved;
                duplicate.IsApproved = isApproved;
                duplicate.IsDecided = true;
                duplicate.ApproveId = user.Id;
                
                _context.SaveChanges();
                //return PartialView("_index", model);
                var ifuser = true;
                return ReturnAvailability(ifuser, "adminpassword", "Success");
            }

            else
            {
                var ifuser = false;
                return ReturnAvailability(ifuser, "adminpassword", "Please enter valid EmailId & Password.");
            }

        }


        public FileResult DownloadExcel()
        {
            var filePath = HostingEnvironment.MapPath("~/Content/Excel/Sunshare Document.xlsx");
            //string path = "Content/Excel/Sunshare Document.xlsx";
            return File(filePath, "application/vnd.ms-excel", "Sunshare Document.xlsx");
        }

        [HttpPost]
        public ActionResult UploadExcel(LeadViewModel model, HttpPostedFileBase fileUpload)
        {
            //var errorLineAndLocation = "0";
            try
            {
                if (fileUpload == null)
                {
                    TempData[Messages.ErrorTempData] = "No files to upload";
                    return RedirectToAction("Index", "Dashboard");
                }
            if (fileUpload.ContentType != "application/vnd.ms-excel" &&
                fileUpload.ContentType != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            {
                TempData[Messages.ErrorTempData] = "Only excel file format is allowed";
                    return RedirectToAction("Index", "Dashboard");
                }
            var filename = fileUpload.FileName;
            var targetpath = Server.MapPath("~/Content/Excel/Import/");
            fileUpload.SaveAs(targetpath + filename);
            var pathToExcelFile = targetpath + filename;
            var insertcount = 0;
            ISheet worksheet;
            using (var fs = new FileStream(pathToExcelFile, FileMode.Open, FileAccess.ReadWrite))
            {
                //errorLineAndLocation = "384";
                var workbook = WorkbookFactory.Create(fs);              
                worksheet = workbook.GetSheetAt(0);
            }
            for (var rowIndex = 1; rowIndex <= worksheet.PhysicalNumberOfRows; rowIndex++)
            {
                    //errorLineAndLocation = "389";
                    var row = worksheet.GetRow(rowIndex);
                if (string.IsNullOrEmpty(row.Cells[0]?.ToString())) break;
                
                    if (!string.IsNullOrEmpty(row.Cells[0]?.ToString()) && !string.IsNullOrEmpty(row.Cells[1]?.ToString()) && !string.IsNullOrEmpty(row.Cells[10]?.ToString()) &&
                       !string.IsNullOrEmpty(row.Cells[3]?.ToString()))
                    {
                        const bool duplicate = false;
                        var tu = new Lead();
                        var ad = new Address();
                        var lp = new LeadLeadStatusMapping();
                        ad.AddressInformation = row.Cells[4]?.ToString() ?? string.Empty;
                        ad.AppartmentNo = row.Cells[5]?.ToString()?? string.Empty;
                        ad.City = row.Cells[6]?.ToString() ?? string.Empty;
                        ad.County = row.Cells[8]?.ToString() ?? string.Empty;
                        ad.State = row.Cells[7]?.ToString() ?? string.Empty;
                        ad.Zipcode = row.Cells[9]?.ToString() ?? string.Empty;
                        ad.AddressSourceId = 4;
                        ad.Lattitude = 0;
                        ad.Longitude = 0;
                        ad.IsActive = true;
                        ad.DeviceLat = 0;
                        ad.DeviceLong = 0;
                        //errorLineAndLocation = "412";
                        _context.Address.Add(ad);
                        _context.SaveChanges();
                        var add = _context.Address.Max(ss => ss.AddressId);
                        tu.FirstName = row.Cells[0]?.ToString();
                        tu.LastName = row.Cells[1]?.ToString();
                        tu.DateOfBirth = row.Cells[2]?.ToString() ?? string.Empty;
                        tu.PrimaryEmail = row.Cells[3]?.ToString();
                        tu.SecondaryEmail = row.Cells[3]?.ToString();
                        tu.UtilityAccountNo = row.Cells[13]?.ToString() ?? string.Empty;
                        tu.UtilityCompanyName = row.Cells[14]?.ToString() ?? string.Empty;
                        tu.HomePhoneNo = row.Cells[10]?.ToString();
                        tu.WorkPhoneNo = row.Cells[10]?.ToString();
                        tu.MobileNo = row.Cells[10]?.ToString();
                        tu.LeadRankingId = 2;
                        tu.CampaignId = 3;
                        tu.RefferedBy = row.Cells[11]?.ToString() ?? string.Empty;
                        var percentsub = row.Cells[12]?.ToString() ?? string.Empty;
                        tu.SubscriptionPercentage = Convert.ToInt32(percentsub);
                        tu.ChannelId = 6;
                        tu.ServiceAddressId = add;
                        tu.BillingAddressId = add;
                        tu.AspNetUsersId = model.SalesRepId;
                        tu.LandingPageId = 6; //signup
                        tu.LeadSourceId = 14;
                        tu.IsActive = true;
                        tu.IsDuplicateLead = duplicate;
                        //errorLineAndLocation = "437";
                        _context.Leads.Add(tu);
                        _context.SaveChanges();
                        var leadcus = _context.Leads.Max(le => le.LeadId);
                        lp.LeadId = leadcus;
                        lp.LeadStatusId = LeadContractStatus.LeadCreated;
                        lp.UpdatedDate = DateTime.Now;
                        lp.IsCurrent = true;
                        _context.LeadLeadStatusMappings.Add(lp);
                        //errorLineAndLocation = "446";
                        _context.SaveChanges();
                        insertcount++;
                    }
                    else
                    {
                        if (System.IO.File.Exists(pathToExcelFile))
                        {
                            //errorLineAndLocation = "455";
                            System.IO.File.Delete(pathToExcelFile);
                        }
                        TempData[Messages.SuccessTempdata]= insertcount == 0
                            ? "No record been uploaded"
                            : insertcount + "Records been uploaded";
                        return RedirectToAction("Index", "Dashboard");
                    }

                }
                if ((System.IO.File.Exists(pathToExcelFile)))
                {
                    System.IO.File.Delete(pathToExcelFile);
                }
                TempData[Messages.SuccessTempdata] = "Uploaded successfully";
                return RedirectToAction("Index", "Dashboard");
            }
            catch (Exception ex)
            {
                //foreach (var entityValidationErrors in ex.EntityValidationErrors)
                //{
                //    foreach (var validationError in entityValidationErrors.ValidationErrors)
                //    {
                //        Response.Write("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
                //    }
                //}
                throw new Exception($"{ex.Message}");
                //throw new Exception($"{errorLineAndLocation} , {ex.Message}");
                //return Json(ex.InnerException, JsonRequestBehavior.AllowGet);
            }
            //return RedirectToAction("Index", "Dashboard");
            //return Json("something went wrong", JsonRequestBehavior.AllowGet);
        }
        private string ReturnAvailability(bool ifuser, string key, string error)
        {
            if (!ifuser)
            {
                return "Available";
            }
            ModelState.AddModelError(key, error);
            return "Not Available";
        }

        [HttpPost]
        public string RequestDuplicate(int leadId, string reason)
        {
            var lead = _context.Leads.FirstOrDefault(l => l.LeadId == leadId);
            if (!_leadContextAssembler.SetDuplicateRequest(lead, reason, User.Id))
                return "false";
            _context.SaveChanges();
            return "true";
        }
    }
}