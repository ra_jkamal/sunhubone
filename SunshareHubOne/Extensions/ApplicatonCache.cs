﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using AppCache = System.Web.Caching.Cache;

namespace SunshareHubOne.Extensions
{
    public interface IApplicationCache
    {
        void Add<T>(string id, T item, TimeSpan timeToLive) where T : class;
        void Add<T>(string id, T item) where T : class;
        T Get<T>(string id) where T : class;
    }

    public class ApplicationCache : IApplicationCache
    {
        private readonly HttpContextBase _context;

        public ApplicationCache(HttpContextBase context)
        {
            _context = context;
        }

        public void Add<T>(string id, T item) where T : class
        {
            Add(id, item, TimeSpan.FromHours(2));
        }

        public void Add<T>(string id, T item, TimeSpan timeToLive) where T : class
        {
            var cache = GetCache();
            if (cache == null) return;
            cache.Add(id, item, null, DateTime.Now.Add(timeToLive), AppCache.NoSlidingExpiration, CacheItemPriority.Normal, null);
        }

        public T Get<T>(string id) where T : class
        {
            var cache = GetCache();
            if (cache == null) return default(T);
            return cache[id] as T;
        }

        private AppCache GetCache()
        {
            return _context.Cache;
        }
    }
}
