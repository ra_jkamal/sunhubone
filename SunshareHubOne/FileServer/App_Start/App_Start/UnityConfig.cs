using System;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.Practices.Unity;
using SunshareDatabase;
using SunshareHubOne.ContextAssembler;
using SunshareHubOne.Controllers;
using SunshareHubOne.ModelAssembler;
using SunshareHubOne.Models;

namespace SunshareHubOne.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main unityContainer.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static readonly Lazy<IUnityContainer> Container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity unityContainer.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return Container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity unityContainer.</summary>
        /// <param name="unityContainer">The unity unityContainer to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        private static void RegisterTypes(IUnityContainer unityContainer)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // unityContainer.LoadConfiguration();

            
            unityContainer.RegisterType<DbContext, ApplicationDbContext>(
    new HierarchicalLifetimeManager());
            unityContainer.RegisterType<UserManager<ApplicationUser,int>>(
                new HierarchicalLifetimeManager());
            unityContainer.RegisterType<IUserStore<ApplicationUser,int>, CustomUserStore>(
                new HierarchicalLifetimeManager());

            unityContainer.RegisterType<AccountController>(
                new InjectionConstructor());
            // TODO: Register your types here
            unityContainer.RegisterType<ILeadViewModelAssembler, LeadViewModelAssembler>();
            unityContainer.RegisterType<ISunshareDatabaseContext, SunshareDatabaseContext>();
            unityContainer.RegisterType<ILeadContextAssembler, LeadContextAssembler>();
            unityContainer.RegisterType<IDashBoardViewModelAssember, DashboardViewModelAssembler>();
            unityContainer.RegisterType<IDashboardContextAssembler, DashboardContextAssembler>();
        }
    }
}
