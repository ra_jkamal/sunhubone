﻿using System.Web.Optimization;

namespace SunshareHubOne
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        //"~/Scripts/select2.js",
                        //"~/Scripts/DynamicScript.js",
                        "~/Scripts/jquery-ui-1.14.1.js",
                        "~/Scripts/jquery.slimscroll.min.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                                 "~/Scripts/jquery.validate*",
                                 "~/Scripts/jquery.unobtrusive-ajax.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryCustom").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery-ui-1.12.0.js"));

            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
                "~/Scripts/bootstrap.min.js",
                "~/Scripts/metisMenu.min.js",
                "~/Scripts/jquery.icheck.min.js",
                "~/Scripts/jquery.peity.js",
                "~/Scripts/jquery.dataTables.min.js",
                "~/Scripts/jquery.dataTables.columnFilter.js",
                "~/Scripts/dataTables.bootstrap.min.js",
                 "~/Scripts/jquery.maskedinput.js",
                "~/Scripts/jquery.datetimepicker.js",
                "~/Scripts/jquery.sparkline.js",
                "~/Scripts/sweetalert.min.js",
                "~/Scripts/Custom/scripts/dataTables.checkboxes.min.js",
                "~/Scripts/Custom/scripts/Homer.js",
                "~/Scripts/Custom/scripts/Charts.js",
                "~/Scripts/custom/scripts/mfb.js",
                "~/Scripts/jquery.maskedinput.js",
                "~/Scripts/pace.min.js",
                "~/Scripts/jquery.easing.min.js",
                "~/Scripts/perfect-scrollbar.min.js",
                "~/Scripts/viewportchecker.js"
                ));

            bundles.Add(new StyleBundle("~/Content/bundle/custom").Include(
                "~/Content/custom/fontawesome/css/font-awesome.css",
                "~/Content/custom/metisMenu/dist/metisMenu.css",
                "~/Content/custom/animate.css/animate.css",
                "~/Content/custom/bootstrap/dist/css/bootstrap.css",
                "~/Content/custom/fonts/pe-icon-7-stroke/css/helper.css",
                "~/Content/custom/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css",
                "~/Content/custom/styles/style.css",
                "~/Content/custom/styles/mfb.css",
                "~/Content/custom/styles/static_custom.css",
                "~/Content/custom/datatables_plugins/integration/bootstrap/3/dataTables.bootstrap.css",
                "~/Content/custom/styles/jquery.datetimepicker.css",
                "~/Content/custom/styles/base.css",
                "~/Content/custom/styles/dataTables.checkboxes.css",
                "~/Styles/sweetalert.css",
                "~/Content/custom/pace-theme-flash.css",
                "~/Content/bootstrap-theme.min.css",
                "~/Content/custom/perfect-scrollbar.css",
                "~/Content/custom/all.css",
                "~/Content/custom/responsive.css"
                ));
        }
    }
}
