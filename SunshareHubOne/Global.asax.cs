﻿using System;
using SunshareDatabase;
using System.Data.Entity;
using System.Security.Principal;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web;

namespace SunshareHubOne
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            Database.SetInitializer<SunshareDatabaseContext>(null);
        }
        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            var authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie == null) return;
            var authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            var serializer = new JavaScriptSerializer();
            if (authTicket == null) return;
            var serializeModel = serializer.Deserialize<SunsharePrincipalSerializeModel>(authTicket.UserData);
            if (serializeModel?.Roles == null) return;
            var newUser = new SunsharePrincipal(new GenericIdentity(authCookie.Name), serializeModel.Roles.ToArray())
            {
                Id = serializeModel.Id,
                FirstName = serializeModel.FirstName,
                LastName = serializeModel.LastName
            }; // SunsharePrincipal(authTicket.Name)
            //                                                                                                      //{

            //};
            HttpContext.Current.User = newUser;
            //HttpContext.Current.User.IsInRole()
        }
    }
}
