﻿using System;
using System.Collections.Generic;
using System.Linq;
using SunshareDatabase;
using SunshareHubOne.Models;
using SunshareCommon;
using System.Web.Mvc;

namespace SunshareHubOne.ModelAssembler
{
    public interface IDashBoardViewModelAssember
    {
        DashboardViewModel Assemble(AspNetUsers user, bool admin = false, bool onlyDormet = false);
        DashboardViewModel Assemblecreate(List<Lead> leads, AspNetUsers user);
        DashboardViewModel Assembleclose(List<Lead> leads, AspNetUsers user);
        RepInformation AssembleAssignInfo(Lead lead, int salesRepId, bool admin = false);
        DashboardViewModel AssembleSearch(AspNetUsers user, string searchText, bool admin = false);
        DashboardViewModel AssembleToManage(AspNetUsers user, bool admin = false, bool onlyDormet = false);
        RepInformation AssembleAssignInfo(string leadIds, int salesRepId, bool admin = false);
        DashboardViewModel AssembleToOpenDuplicate(int salesRepId, bool isAdmin);
           DashboardViewModel AssembleToClosedDuplicate(int salesRepId, bool isAdmin);
        DashboardViewModel AssembleToManageDuplicate(int salesRepId, bool isAdmin);
        DashboardViewModel AssembleManageDuplicate(int salesRepId, bool isAdmin);
    }
    public class DashboardViewModelAssembler : IDashBoardViewModelAssember
    {
        private ISunshareDatabaseContext Context { get; }

        public DashboardViewModelAssembler(ISunshareDatabaseContext context)
        {
            Context = context;
        }

        public DashboardViewModel Assemble(AspNetUsers user, bool admin = false, bool onlyDormet = false)
        {
            var model = new DashboardViewModel { SalesRepId = user?.Id ?? 1 };
            if (user == null) return model;
            var myLeads = !onlyDormet ? !admin ? user.Leads.Where(l => l.IsActive & !l.IsDuplicateLead).ToList() : Context.Leads.Where(l => l.IsActive & !l.IsDuplicateLead).ToList() :
                !admin ? user.Leads.Where(l => !l.IsActive).ToList() : Context.Leads.Where(l => !l.IsActive).ToList();
            foreach (var lead in myLeads.Where(l => l.CallLogs != null))
            {
                AddLeadToViewModel(model, lead, lead.LeadServiceAddress, lead.LeadLeadStatusMappings.FirstOrDefault(l => l.IsCurrent), lead.LeadRep);
            }
            SetUpCounts(model, myLeads);
            return model;
        }

        public DashboardViewModel AssembleToManage(AspNetUsers user, bool admin = false, bool onlyDormet = false)
        {
            var model = new DashboardViewModel { SalesRepId = user?.Id ?? 1 };
            if (user == null) return model;
            var myLeads = !onlyDormet ? !admin ? user.Leads.Where(l => l.IsActive & !l.IsDuplicateLead).ToList() : Context.Leads.Where(l => l.IsActive & !l.IsDuplicateLead).ToList() :
                !admin ? user.Leads.Where(l => !l.IsActive).ToList() : Context.Leads.Where(l => !l.IsActive).ToList();
            foreach (var lead in myLeads.Where(l => l.CallLogs != null))
            {
                AddLeadToViewModel(model, lead, lead.LeadServiceAddress, lead.LeadLeadStatusMappings.FirstOrDefault(l => l.IsCurrent), lead.LeadRep);
            }
            SetUpCounts(model, myLeads);
            return model;
        }
        public DashboardViewModel Assemblecreate(List<Lead> leads, AspNetUsers user)
        {
            var model = new DashboardViewModel { SalesRepId = user?.Id ?? 1 };

            foreach (var lead in leads)
            {
                AddLeadToViewModel(model, lead, lead.LeadServiceAddress, lead.LeadLeadStatusMappings.FirstOrDefault(l => l.IsCurrent), lead.LeadRep);
            }
            //SetUpCounts(model, myLeads);
            return model;
        }


        public DashboardViewModel Assembleclose(List<Lead> leads,AspNetUsers user)
        {
            var model = new DashboardViewModel { SalesRepId = user?.Id ?? 1 };
            
            foreach (var lead in leads)
            {
                AddLeadToViewModel(model, lead, lead.LeadServiceAddress, lead.LeadLeadStatusMappings.FirstOrDefault(l =>l.LeadStatusId==LeadContractStatus.ContractSigned & l.IsCurrent), lead.LeadRep);
            }
            //SetUpCounts(model, myLeads);
            return model;
        }

        private static void AddLeadToViewModel(DashboardViewModel model, Lead lead, Address leadAddress, LeadLeadStatusMapping leadLeadStatusMapping, AspNetUsers repForLead)
        {
            model.Leads.Add(SetUpLeadInformation(lead, leadAddress, leadLeadStatusMapping, repForLead));
        }

        private static void SetUpCounts(DashboardViewModel model, IReadOnlyCollection<Lead> myLeads)
        {
            model.ContractDeclined =
                myLeads.Count(
                    l =>
                        !l.IsAssigned &
                        l.LeadLeadStatusMappings.Any(s => s.IsCurrent & s.LeadStatusId == LeadContractStatus.ContractDeclined));
            model.ContractSigned =
                myLeads.Count(
                    l =>
                    !l.IsAssigned & l.LeadLeadStatusMappings.Any(s => s.IsCurrent & s.LeadStatusId == LeadContractStatus.ContractSigned));
            model.ContractSent =
                myLeads.Count(
                    l =>
                        !l.IsAssigned &
                        l.LeadLeadStatusMappings.Any(s => s.IsCurrent & s.LeadStatusId == LeadContractStatus.ContractSent));
            model.InformationSent =
                myLeads.Count(
                    l =>
                        !l.IsAssigned &
                        l.LeadLeadStatusMappings.Any(
                            s => s.IsCurrent & s.LeadStatusId == LeadContractStatus.CotractInformationSent));
            model.SavedLead =
                myLeads.Count(
                    l =>
                        l.IsActive & !l.IsAssigned &
                        l.LeadLeadStatusMappings.Any(
                            s => s.IsCurrent & s.LeadStatusId == LeadContractStatus.LeadCreated));
            model.RefusedLead =
               myLeads.Count(
                   l =>
                       l.IsActive & !l.IsAssigned &
                       l.LeadLeadStatusMappings.Any(
                           s => s.IsCurrent & s.LeadStatusId == LeadContractStatus.RefusedLead));
            model.AssignedLead = myLeads.Count(l => l.IsAssigned);
            //model.AssignedLead = myLeads.Count(
            //        l =>
            //            (l.IsAssigned && l.LeadLeadStatusMappings.Any(s => s.IsCurrent & s.LeadStatusId != LeadContractStatus.ContractSigned))
            //            || (l.IsActive & l.DupliacteContractRequests.Any(s => !s.IsDecided) && l.LeadLeadStatusMappings.Any(s => s.IsCurrent & s.LeadStatusId == LeadContractStatus.ContractSigned)));
            model.CallBack =
                myLeads.Count(
                    l =>
                    {
                        return l.LeadLeadStatusMappings.Count(c => c.IsCurrent && (c.LeadStatusId == LeadContractStatus.ContractDeclined || c.LeadStatusId == LeadContractStatus.ContractSigned)) == 0 && l.CallLogs != null && l.CallLogs.Any(x => x.Appointment?.DateToCall != null && l.IsActive && x.IsActive && !l.IsAssigned);
                    });
            model.OnceCalled =
                myLeads.Count(
                    lead =>
                    {
                        return lead.LeadLeadStatusMappings.Count(c => c.IsCurrent && (c.LeadStatusId == LeadContractStatus.ContractDeclined || c.LeadStatusId == LeadContractStatus.ContractSigned)) == 0 && lead.CallLogs != null &&
                               !lead.IsAssigned & lead.CallLogs.Count(c => c.CallStatusId != CallStatuses.Incoming) == 1;
                    });
            model.TwiceCalled =
                myLeads.Count(
                    lead =>
                    {
                        return lead.LeadLeadStatusMappings.Count(c => c.IsCurrent && (c.LeadStatusId == LeadContractStatus.ContractDeclined || c.LeadStatusId == LeadContractStatus.ContractSigned)) == 0 && lead.CallLogs != null &&
                               !lead.IsAssigned & lead.CallLogs.Count(c => c.CallStatusId != CallStatuses.Incoming) == 2;
                    });
            model.MoreThriceCalled =
                myLeads.Count(
                    lead =>
                    {
                        return lead.LeadLeadStatusMappings.Count(c => c.IsCurrent && (c.LeadStatusId == LeadContractStatus.ContractDeclined || c.LeadStatusId == LeadContractStatus.ContractSigned)) == 0 && lead.CallLogs != null &&
                               !lead.IsAssigned & lead.CallLogs.Count(c => c.CallStatusId != CallStatuses.Incoming) > 2;
                    });
            model.WebEnrollment =
                myLeads.Count(
                    lead => lead.ChannelId != null && !lead.IsAssigned && lead.ChannelId.Value == LeadChannels.WebEnrollmentAuto && lead.LeadRep == null);
            model.WebLanding =
                myLeads.Count(
                    lead => lead.ChannelId != null && !lead.IsAssigned && lead.ChannelId.Value == LeadChannels.WebLanding && lead.LeadRep == null);
            model.Others =
                myLeads.Count(
                    lead => lead.ChannelId != null && !lead.IsAssigned && lead.ChannelId.Value == LeadChannels.Sunshare && lead.LeadRep == null);
            model.D2D =
                myLeads.Count(lead => lead.ChannelId != null && !lead.IsAssigned & lead.ChannelId.Value == LeadChannels.D2D);
            model.all = myLeads.Count;
        }

        public RepInformation AssembleAssignInfo(Lead lead, int salesRepId, bool admin = false)
        {
            var repInfo = SetUpRepInfo(salesRepId, admin);
            repInfo.LeadId = lead.LeadId;
            lead.IsAssigned = true;
            return repInfo;
        }

        private RepInformation SetUpRepInfo(int salesRepId, bool admin)
        {
            var repInfo = new RepInformation
            {
                IsAdmin = admin,
                SalesRepId = salesRepId,
                Reps =
                    !admin
                        ? Context.Users.Where(u => u.UserParentId == salesRepId && u.UserTypeId != SalesUserTypes.D2D)
                            .Select(s => new SelectListItem { Text = s.FirstName, Value = s.Id.ToString() })
                            .ToList()
                        : Context.Users.Where(u => u.Id != salesRepId && u.PartnerId == 1)
                            .Select(s => new SelectListItem { Text = s.FirstName, Value = s.Id.ToString() })
                            .ToList(),
            };
            return repInfo;
        }

        public RepInformation AssembleAssignInfo(string leadIds, int salesRepId, bool admin = false)
        {
            var repInfo = SetUpRepInfo(salesRepId, admin);
            repInfo.LeadIdCollection = leadIds;
            return repInfo;
        }

        public DashboardViewModel AssembleToManageDuplicate(int salesRepId, bool isAdmin)
        {
            var leads = isAdmin
                ? Context.Leads.Where(l => l.DupliacteContractRequests.Count != 0 && l.IsActive).ToList()
                : Context.Leads.Where(l => (l.DupliacteContractRequests.Count != 0 & l.AspNetUsersId == salesRepId) && l.IsActive).ToList();
            var duplicateRequest = new DashboardViewModel { SalesRepId = salesRepId, IsAdmin = isAdmin };
            IterateForDuplicateRequestLeads(leads, duplicateRequest);
            return duplicateRequest;
        }
        public DashboardViewModel AssembleToOpenDuplicate(int salesRepId, bool isAdmin)
        {
            var leads = isAdmin
                ? Context.Leads.Where(l => l.DupliacteContractRequests.Count != 0 && l.IsActive).ToList()
                : Context.Leads.Where(l => (l.DupliacteContractRequests.Count != 0 & l.AspNetUsersId == salesRepId) && l.IsActive).ToList();
            var duplicateRequest = new DashboardViewModel { SalesRepId = salesRepId, IsAdmin = isAdmin };
            IterateForDuplicateRequestLeads(leads, duplicateRequest);
            return duplicateRequest;
        }

        public DashboardViewModel AssembleToClosedDuplicate(int salesRepId, bool isAdmin)
        {
            var leads = isAdmin
                ? Context.Leads.Where(l => l.DupliacteContractRequests.Count != 0 && l.IsActive).ToList()
                : Context.Leads.Where(l => (l.DupliacteContractRequests.Count != 0 & l.AspNetUsersId == salesRepId) && l.IsActive).ToList();
            var duplicateRequest = new DashboardViewModel { SalesRepId = salesRepId, IsAdmin = isAdmin };
            IterateForDuplicateRequestclosedLeads(leads, duplicateRequest);
            return duplicateRequest;
        }

        public DashboardViewModel AssembleManageDuplicate(int salesRepId, bool isAdmin)
        {
            var leads = isAdmin
                ? Context.Leads.Where(l => l.DupliacteContractRequests.Count != 0 && l.IsActive).ToList()
                : Context.Leads.Where(l => (l.DupliacteContractRequests.Count != 0 & l.AspNetUsersId == salesRepId) && l.IsActive).ToList();
            var duplicateRequest = new DashboardViewModel { SalesRepId = salesRepId, IsAdmin = isAdmin };
            IterateForDuplicateRequestLeads(leads, duplicateRequest);
            return duplicateRequest;
        }

        private static void IterateForDuplicateRequestLeads(IEnumerable<Lead> leads, DashboardViewModel duplicateRequest)
        {
            foreach (var lead in leads)
            {
                var leadAddress = lead.LeadServiceAddress;
                var leadLeadStatusMapping = lead.LeadLeadStatusMappings.FirstOrDefault(l => l.IsCurrent);
                var repForLead = lead.LeadRep;
                SetUpDuplicateRequests(duplicateRequest, lead, leadAddress, leadLeadStatusMapping, repForLead);
            }
        }

        private static void IterateForDuplicateRequestclosedLeads(IEnumerable<Lead> leads, DashboardViewModel duplicateRequest)
        {
            foreach (var lead in leads)
            {
                var leadAddress = lead.LeadServiceAddress;
                var leadLeadStatusMapping = lead.LeadLeadStatusMappings.FirstOrDefault(l => l.IsCurrent);
                var repForLead = lead.LeadRep;
                SetUpDuplicateclosedRequests(duplicateRequest, lead, leadAddress, leadLeadStatusMapping, repForLead);
            }
        }

        private static void SetUpDuplicateRequests(DashboardViewModel duplicateRequest, Lead lead, Address leadAddress,
            LeadLeadStatusMapping leadLeadStatusMapping, AspNetUsers repForLead)
        {
            foreach (var request in lead.DupliacteContractRequests.Where(request => request != null && (!request.IsApproved && !request.IsDecided)))
            {
                SetUpDuplicateRequest(duplicateRequest, lead, leadAddress, leadLeadStatusMapping, repForLead, request);
            }
        }

        private static void SetUpDuplicateclosedRequests(DashboardViewModel duplicateRequest, Lead lead, Address leadAddress,
         LeadLeadStatusMapping leadLeadStatusMapping, AspNetUsers repForLead)
        {
            foreach (var request in lead.DupliacteContractRequests.Where(request => request != null && (request.IsApproved || request.IsDecided)))
            {
                SetUpDuplicateRequest(duplicateRequest, lead, leadAddress, leadLeadStatusMapping, repForLead, request);
            }
        }

        private static void SetUpDuplicateRequest(DashboardViewModel duplicateRequest, Lead lead, Address leadAddress,
            LeadLeadStatusMapping leadLeadStatusMapping, AspNetUsers repForLead, DupliacteContractRequest request)
        {
            duplicateRequest.Leads.Add(SetUpLeadInformation(lead, leadAddress, leadLeadStatusMapping, repForLead, request));
        }

        private static LeadInformation SetUpLeadInformation(Lead lead, Address leadAddress, LeadLeadStatusMapping leadLeadStatusMapping, AspNetUsers repForLead = null, DupliacteContractRequest request = null)
        {
            var leadinfo = new LeadInformation
            {
                LeadId = lead.LeadId,
                SalesRepId = lead.AspNetUsersId ?? 0,
                Name = $"{lead.FirstName} {lead.LastName}",
                FirstName = lead.FirstName,
                LastName = lead.LastName,
                Email = lead.PrimaryEmail,
                Phone = lead.HomePhoneNo,
                DateTime = leadLeadStatusMapping?.UpdatedDate.ToString("MM/dd/yyyy hh:mm tt") ?? string.Empty,
                State = leadAddress?.State,
                LeadStausId = leadLeadStatusMapping?.LeadStatusId ?? 0,
                CallCount = lead.CallLogs?.Count(c => c.CallStatusId != CallStatuses.Incoming & lead.IsActive) ?? 0,
                LeadChannelId = lead.ChannelId ?? 0,
                reason = lead.InActiveReason,
                RepName = (lead.AspNetUsersId != 0)
                    ? repForLead != null
                        ? $"{repForLead.FirstName} {repForLead.LastName}"
                        : "NA"
                    : "NA",
                IsAssigned = lead.IsAssigned.ToString(),
                CallDate = Convert.ToString(lead.CallLogs?.LastOrDefault(c => c.CallStatusId == CallStatuses.CallBack && c.IsActive)?
                            .Appointment?.DateToCall.ToString("MM/dd/yyyy hh:mm tt")),
                AppointmnetCount = lead.LeadLeadStatusMappings.Count(c => c.IsCurrent && (c.LeadStatusId == LeadContractStatus.ContractDeclined || c.LeadStatusId == LeadContractStatus.ContractSigned)) == 0 ? lead.CallLogs?.Count(c => c.IsActive && c.CallStatusId == CallStatuses.CallBack) ?? 0 : 0,
                AssignedDate = leadLeadStatusMapping?.UpdatedDate.ToString("MM/dd/yyyy hh:mm tt") ?? string.Empty,
                AllowDuplicateContract = lead.AllowDuplicateContract,
                Signedunlock=(lead.LeadLeadStatusMappings.Any(l=>l.LeadStatusId==LeadContractStatus.ContractSigned) && lead.DupliacteContractRequests.Any(D=>D.IsDecided))?"True":"False",
            };
            SetUpLeadInfoRequests(request, leadinfo);
            if (repForLead != null)
                leadinfo.RepName = $"{repForLead.FirstName} {repForLead.LastName}";
            return leadinfo;
        }

        private static void SetUpLeadInfoRequests(DupliacteContractRequest request, LeadInformation leadinfo)
        {
            if (request == null) return;
            leadinfo.DuplicateRequestId = request.DupliacteContractRequestId;
            leadinfo.reasontip = request.Reason;
            leadinfo.DuplicateReason = request.Reason.Length > 10 ? string.Format("{0}...", request.Reason.Substring(0, 9)) : request.Reason;
            leadinfo.IsDuplicateContractApproved = request.IsApproved;
            leadinfo.IsDuplicateRequestDecided = request.IsDecided;
        }

        public DashboardViewModel AssembleSearch(AspNetUsers user, string searchText, bool admin = false)
        {
            var model = new DashboardViewModel { SalesRepId = user?.Id ?? 1 };
            if (user == null) return model;
            var myLeads = user.Leads.Where(l => l.IsActive & !l.IsDuplicateLead).ToList();
            foreach (var lead in myLeads)
            {
                var leadAddress = lead.LeadServiceAddress;
                var leadLeadStatusMapping = lead.LeadLeadStatusMappings.FirstOrDefault(l => l.IsCurrent);
                var count = lead.CallLogs?.Count(x => x.Appointment?.DateToCall != null && lead.IsActive && x.CallStatusId != CallStatuses.Incoming && lead.IsActive);
                if (count != null)
                    model.Leads.Add(SetUpLeadInformation(lead, leadAddress, leadLeadStatusMapping));
            }
            var searchLeads = Context.Leads.Where(l => !l.IsDuplicateLead & l.IsActive & (l.PrimaryEmail == searchText || l.HomePhoneNo == searchText) & l.AspNetUsersId != user.Id).ToList();
            model.SearchLeads = new List<LeadInformation>();
            foreach (var lead in searchLeads)
            {
                var leadAddress = lead.LeadServiceAddress;
                var leadLeadStatusMapping = lead.LeadLeadStatusMappings.FirstOrDefault(l => l.IsCurrent);
                var count = lead.CallLogs?.Count(x => x.Appointment?.DateToCall != null && lead.IsActive && x.CallStatusId != CallStatuses.Incoming && lead.IsActive);
                if (count != null)
                    model.SearchLeads.Add(SetUpLeadInformation(lead, leadAddress, leadLeadStatusMapping));
            }
            model.IsAdmin = admin;
            SetUpCounts(model, myLeads);
            return model;
        }



     
    }
}
