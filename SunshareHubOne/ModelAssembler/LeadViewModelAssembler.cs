﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SunshareCommon;
using SunshareDatabase;
using SunshareHubOne.Models;


namespace SunshareHubOne.ModelAssembler
{
    public interface ILeadViewModelAssembler
    {
        LeadViewModel Assemble(int? salesRepId,bool isAdmin=false);
        LeadViewModel Assemble(int? salesRepId, int leadId,bool isAdmin=false);
    }
    public class LeadViewModelAssembler : ILeadViewModelAssembler
    {
        private ISunshareDatabaseContext Sunshare { get; }
        public LeadViewModelAssembler(ISunshareDatabaseContext context)
        {
            Sunshare = context;
        }
        public LeadViewModel Assemble(int? salesRepId, bool isAdmin = false)
        {
            var model = new LeadViewModel
            {
                IsAdmin = isAdmin,
                States = Sunshare.States.Where(s => s.IsActive).Select(s => new SelectListItem() { Text = s.StateName, Value = s.StateId.ToString() }),
                Counties = Sunshare.Counties.Where(c => c.IsActive).Select(c => new SelectListItem { Text = c.CountyName, Value = c.CountyId.ToString() }).OrderBy(o => o.Text),
                Channels = Sunshare.Channels.Where(c => c.IsActive & c.ShowOnAdd).Select(c => new SelectListItem { Text = c.ChannelName, Value = c.ChannelId.ToString()}).OrderBy(o => o.Text),
                Campaigns = Sunshare.CampaignDetails.Where(c => c.StartDate<=DateTime.Now & c.EndDate>= DateTime.Now).Select(c => new SelectListItem { Text = c.CampaignName, Value = c.CampaignID.ToString() }).OrderBy(o => o.Text),
                LeadRankings = Sunshare.LeadRakings.Select(r => new SelectListItem { Text = r.RankingName, Value = r.LeadRankingId.ToString()}).OrderBy(o => o.Value),
                SalesRepId = salesRepId,
                LeadSources = Sunshare.LeadSources.Where(l => l.IsActive).Select(l => new SelectListItem { Text = l.LeadSourceName, Value = l.LeadSourceId.ToString() }).OrderBy(o => o.Text),
                CallStatus = Sunshare.CallStatus.Where(c => c.IsActive).Select(c => new SelectListItem { Text = c.CallStatusName, Value = c.CallStatusId.ToString() }).OrderBy(o => o.Text),
                LeadStausId = 0,
                Subscription = "100",
                IsActive = true
            };
            return model;
        }

        public LeadViewModel Assemble(int? salesRepId, int leadId,bool isAdmin=false)
        {
            LeadViewModel model;
            var lead = Sunshare.Leads.FirstOrDefault(l => l.LeadId == leadId);
            if (lead == null) return new LeadViewModel();
            var serviceAddress = lead.LeadServiceAddress;
            var billingAddress = lead.LeadBillingAddress;

            if (serviceAddress == null) return new LeadViewModel();
            {
                model = SetUpLeadViewModel(salesRepId, isAdmin, lead, serviceAddress, billingAddress);
                var leadStatus =
                    lead.LeadLeadStatusMappings.FirstOrDefault(l => l.IsCurrent);
                if (leadStatus != null)
                    model.LeadStausId = leadStatus.LeadStatusId;
                var serviceStateInfo = Sunshare.States.FirstOrDefault(s => s.StateName == serviceAddress.State);
                if (serviceStateInfo != null)
                    model.SerStateId = serviceStateInfo.StateId;
                var serviceCountyInfo = Sunshare.Counties.FirstOrDefault(c => c.CountyName == serviceAddress.County);
                if (serviceCountyInfo != null)
                    model.SerCountyId = serviceCountyInfo.CountyId;
                var billingStateInfo = Sunshare.States.FirstOrDefault(s => s.StateName == billingAddress.State);
                if (billingStateInfo != null)
                    model.BilStateId = billingStateInfo.StateId;
                var billingCountyInfo = Sunshare.Counties.FirstOrDefault(c => c.CountyName == billingAddress.County);
                if (billingCountyInfo != null)
                    model.BilCountyId = billingCountyInfo.CountyId;
                model.LeadHistories = GetLeadHistory(lead);
                model.CallHistories = GetCallHistory(lead);
                model.AssignHistories = GetAssignHistory(lead);
            }
            return model;
        }

        private LeadViewModel SetUpLeadViewModel(int? salesRepId, bool isAdmin, Lead lead, Address serviceAddress, Address billingAddress)
        {
            var IsRoleManager = Sunshare.UserInRoles.FirstOrDefault(m=>m.UserId==salesRepId);
            var request = Sunshare.DupliacteContractRequests.FirstOrDefault(m => m.LeadId == lead.LeadId && (!m.IsApproved & !m.IsDecided));
            var ISManager = false;
            if (IsRoleManager.RoleId==2)
            {
                ISManager = true;
            }
            return new LeadViewModel
            {
                IsAdmin = isAdmin,
                States = Sunshare.States.Where(s => s.IsActive).Select(s => new SelectListItem { Text = s.StateName, Value = s.StateId.ToString() }),
                Counties = Sunshare.Counties.Where(c => c.IsActive).Select(c => new SelectListItem { Text = c.CountyName, Value = c.CountyId.ToString() }).OrderBy(o => o.Text),
                Channels = Sunshare.Channels.Where(c => c.IsActive).Select(c => new SelectListItem { Text = c.ChannelName, Value = c.ChannelId.ToString() }).OrderBy(o => o.Text),
                Campaigns = Sunshare.CampaignDetails.Where(c => c.StartDate <= DateTime.Now & c.EndDate >= DateTime.Now).Select(c => new SelectListItem { Text = c.CampaignName, Value = c.CampaignID.ToString() }).OrderBy(o => o.Text),
                ParentsId = lead.LeadCampaign.ParentType,
                SubParentsId = lead.LeadCampaign.SubType,
                CreativeId = lead.LeadCampaign.Creative,
                IncentiveId = lead.LeadCampaign.Incentive,
                LeadRankings = Sunshare.LeadRakings.Select(r => new SelectListItem { Text = r.RankingName, Value = r.LeadRankingId.ToString() }).OrderBy(o => o.Text),
                LeadSources = Sunshare.LeadSources.Where(l => l.IsActive).Select(l => new SelectListItem { Text = l.LeadSourceName, Value = l.LeadSourceId.ToString() }).OrderBy(o => o.Text),
                CallStatus = Sunshare.CallStatus.Where(c => c.IsActive).Select(c => new SelectListItem { Text = c.CallStatusName, Value = c.CallStatusId.ToString() }).OrderBy(o => o.Text),
                SalesRepId = salesRepId,
                FirstName = lead.FirstName,
                LastName = lead.LastName,
                ServiceApartmentNo = serviceAddress.AppartmentNo,
                ServiceAddress = serviceAddress.AddressInformation,
                SerZipCode = serviceAddress.Zipcode,
                ServiceCity = serviceAddress.City,
                BillingApartmentNo = billingAddress.AppartmentNo,
                BillingAddress = billingAddress.AddressInformation,
                BillingCity = billingAddress.City,
                BilZipCode = billingAddress.Zipcode,
                Email = lead.PrimaryEmail,
                Phone = lead.HomePhoneNo,
                LeadRankingId = lead.LeadRankingId,
                LeadId = lead.LeadId,
                ReferedBy = lead.RefferedBy,
                CampaignId = lead.CampaignId,
                ChannelId = lead.ChannelId ?? 0,
                LeadSourceId = lead.LeadSourceId ?? 0,
                UtilityAccountNumber = lead.UtilityAccountNo,
                Subscription = lead.SubscriptionPercentage.ToString(),
                DateOfBirth = string.IsNullOrEmpty(lead.DateOfBirth) ? (DateTime?)null : Convert.ToDateTime(lead.DateOfBirth, System.Globalization.CultureInfo.GetCultureInfo("en-US").DateTimeFormat),
                AllowDuplicateContract = lead.AllowDuplicateContract,
                HasPendingRequest = lead.DupliacteContractRequests.Any(d => !d.IsDecided),
                IsActive = lead.IsActive,
                Reason = request != null ? request.Reason : string.Empty,
                adminuser = null,
                adminpassword=null,
                Manager =ISManager,
                DuplicateRequestId= request != null ? request.DupliacteContractRequestId : 0
            };
        }

        private static LeadHistory GetLeadHistory(Lead lead)
        {
            var statusHistory = lead.LeadLeadStatusMappings;
            var leadHistory = new LeadHistory
            {
                reason = lead.InActiveReason,
                LeadCreatedBy = lead.UserCreated,
                LeadCreatedOn = lead.DateCreated
            };
            foreach (var status in statusHistory)
            {
                switch (status.LeadStatusId)
                {
                    case LeadContractStatus.ContractSent:
                    {
                        leadHistory.ContractSentBy = status.UserCreated;
                        leadHistory.ContractSentOn = status.DateCreated;
                        break;
                    }
                    case LeadContractStatus.ContractDeclined:
                    {
                        leadHistory.ContractDeclinedBy = status.UserCreated;
                        leadHistory.ContractDeclinedOn = status.DateCreated;
                        break;
                    }
                    case LeadContractStatus.ContractSigned:
                    {
                        leadHistory.ContractSignedOn = status.DateCreated;
                        leadHistory.ContractSignedBy = status.UserCreated;
                        break;
                    }
                    case LeadContractStatus.CotractInformationSent:
                    {
                        leadHistory.InformationSentBy = status.UserCreated;
                        leadHistory.InformationSentOn = status.DateCreated;
                        break;
                    }
                }
            }
            return leadHistory;
        }
        private static ICollection<CallHistory> GetCallHistory(Lead lead)
        {
            var callHistory = lead.CallLogs;
            var hisotories = callHistory.Select(call => new CallHistory()
            {
                Title = call.CallStatus.CallStatusName,
                Boundstatus = call.BoundStatusId,
                Date = call.DateCreated?.ToString("MM-dd-yyyy hh:mm tt") ?? string.Empty,
                DoneBy = call.UserCreated,
                Comments = call.CallComment,
                ProcessDate = (call.Appointment != null) ? "Appointment Date:" + call.Appointment.DateToCall.ToString("MM-dd-yyyy hh:mm tt") : string.Empty,
            });
            return hisotories.OrderByDescending(h => h.Date).ToList();
        }
        private static ICollection<AssignHistory> GetAssignHistory(Lead lead)
        {
            var assignHistory = lead.AssignHistory;
            var hisotories = assignHistory.Select(assignment => new AssignHistory
            {
                Title = assignment.mappedUser!=null?assignment.mappedUser.UserName:string.Empty,
                Date = assignment.DateCreated?.ToString("MM-dd-yyyy hh:mm tt") ?? string.Empty,
                DoneBy = assignment.UserCreated,
            });
            return hisotories.OrderByDescending(h => h.Date).ToList();
        }
    }
}
