﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SunshareHubOne.Models
{
    public class BulkViewModel
    {
        public string PrimaryEmail { get; set; }   
        public string SecondaryEmail { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DateOfBirth { get; set; }
        public string UtilityAccountNo { get; set; }
        public string UtilityCompanyName { get; set; }
        public string HomePhoneNo { get; set; }
        public string WorkPhoneNo { get; set; }
        public string MobileNo { get; set; }
        public int ServiceAddressId { get; set; }
        public int BillingAddressId { get; set; }
        public string RefferedBy { get; set; }
        public int SubscriptionPercentage { get; set; }
        public int? AspNetUsersId { get; set; }
        public int? ChannelId { get; set; }
        public int CampaignId { get; set; }
        public int LeadRankingId { get; set; }
        public int? LandingPageId { get; set; }
        public int? LeadSourceId { get; set; }
        public bool IsDuplicateLead { get; set; }
        public string AppartmentNo { get; set; }
       public string AddressInformation { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zipcode { get; set; }
        public string County { get; set; }
        public DateTime UpdatedDate { get; set; }
        


    }
}