﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SunshareHubOne.Models
{
    public class DashboardViewModel
    {
        public DashboardViewModel()
        {
            Leads=new List<LeadInformation>();
            //SearchLeads = new List<LeadInformation>();
        }

        public string SearchText = "";
        public ICollection<LeadInformation> Leads { get; private set; }

        public ICollection<LeadInformation> SearchLeads { get; set; }

        public int ContractSent { get; set; }

        public int ContractSigned { get; set; }

        public int ContractDeclined { get; set; }

        public int InformationSent { get; set; }

        public int SavedLead { get; set; }
        public int RefusedLead { get; set; }

        public int AssignedLead { get; set; }

        public int CallBack { get; set; }

        public int OnceCalled { get; set; }

        public int TwiceCalled { get; set; }

        //public int ThriceCalled { get; set; }

        public int MoreThriceCalled { get; set; }
        public int Others { get; set; }

        public int WebEnrollment { get; set; }

        public int WebLanding { get; set; }

        public int D2D { get; set; }

        public int SalesRepId { get; set; }

        public bool IsAdmin { get; set; }

        public int all { get; set; }
        public string Reason { get; set; }
    }

    public class LeadInformation
    {
        public int LeadId { get; set; }
        public int SalesRepId { get; set; }
        public string Name { get; set; }
        [DisplayName("First name")]
        public string FirstName { get; set;}
        [DisplayName("Last name")]
        public string LastName { get; set; }

        public string Phone { get; set; }

        public string State { get; set; }

        public string Email { get; set; }
        public string DateTime { get; set; }

        public int LeadStausId { get; set; }

        public int CallCount { get; set; }

        public string RepName { get; set; }

        public int AppointmnetCount { get; set; }

        public bool IsAdmin { get; set; }
        public string IsAssigned { get; set; }
        public int DuplicateRequestId { get; set; }

        public string DuplicateReason { get; set; }

        public string reasontip { get; set; }

        public bool IsDuplicateContractApproved { get; set; }

        public bool IsDuplicateRequestDecided { get; set; }
        public bool AllowDuplicateContract { get; set; }

        public int LeadChannelId { get; set; }
        public int LeadSourceId { get; set; }
        public string AssignedDate { get; set; }
        public string CallDate { get; set; }
        public string reason { get; set; }
        public string Signedunlock { get; set; }
    }

    public class RepInformation
    {
        public string LeadIdCollection { get; set; }
        public bool IsAdmin { get; set; }
        public int SalesRepId { get; set; }
        public int LeadId { get; set;}
        [DisplayName("Representative")]
        public List<SelectListItem> Reps { get; set; }
        public int SelectedRepId { get; set; }
        
    }
    public class Rep
    {
        public int RepId { get; set; }

        public int RepName { get; set; }
    }
}
