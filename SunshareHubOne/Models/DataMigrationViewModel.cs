﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SunshareHubOne.Models
{
    public class LeadDataViewModel
    {
        
    }
    public class LeadOnlyViewModel
    {
        
    }
    public class UsersOnlyViewModel
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PartnerId { get; set; }
        public string UserTypeId { get; set; }
        public bool IsActive { get; set; }
        public bool ImageUrl { get; set; }
    }
}