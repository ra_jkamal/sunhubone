﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using SunshareHubOne.Extensions;

namespace SunshareHubOne.Models
{
    public class LeadViewModel
    {
        [DisplayName("Billing address same as Service address")]
        public bool sameAsService { get; set; }
        public bool IsAdmin { get; set; }

        [Required(ErrorMessage = "Please enter first name")]
        [RegularExpression(@"([a-zA-Z]+)", ErrorMessage ="Enter proper names only")]
        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please enter last name")]
        [RegularExpression(@"([a-zA-Z ]+)", ErrorMessage = "Enter proper names only")]
        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [DisplayName("Apartment / Suite")]
        //[RegularExpression(@"([^-\s][a-zA-Z{1}0-9--s]+$)", ErrorMessage = "Please enter valid apartment / door no.")]
        public string ServiceApartmentNo { get;set; }

        [DisplayName("Apartment / Suite")]
        //[RegularExpression(@"([^-\s][a-zA-Z{1}0-9--s]+$)", ErrorMessage = "Please enter valid apartment / door no.")]
        public string BillingApartmentNo { get; set; }

        [DisplayName("Service Address")]
        [DataType(DataType.MultilineText)]
        [RegularExpression(@"(^[\s\S]{0,150}$)+", ErrorMessage = "Please enter valid Address.")]
        [Required(ErrorMessage = "Please enter address detail")]
        public string ServiceAddress { get; set; }

        [DisplayName("Billing Address")]
        [DataType(DataType.MultilineText)]
        [RegularExpression(@"(^[\s\S]{0,150}$)+", ErrorMessage = "Please enter valid Address.")]
        [Required(ErrorMessage = "Please enter address detail")]
        public string BillingAddress { get; set; }

        [Required(ErrorMessage = "Please enter phone number")]
        [MinLength(14, ErrorMessage = "Enter a 10 digit phone number")]
        [MaxLength(14, ErrorMessage = "Enter a 10 digit phone number")]
        public string Phone { get; set; }

        [DisplayName("State")]
        public IQueryable<SelectListItem> States { get; set; }

        [DisplayName("Service State")]
        [Required(ErrorMessage ="Please select a state")]
        public int? SerStateId { get; set; }

        [DisplayName("Billing State")]
        [Required(ErrorMessage = "Please select a state")]
        public int BilStateId { get; set; }

        [Required(ErrorMessage = "Please enter a city")]
        [RegularExpression(@"(^([\s\.]?[a-zA-Z]+)+$)", ErrorMessage = "Please enter the city name only")]
        //[RegularExpression(@"([a-zA-Z]{1}[a-zA-Z]*[\s]{0,1}[a-zA-Z])+([\s]{0,1}[a-zA-Z]+)", ErrorMessage = "Please enter the city name only")]
        //[RegularExpression(@"( ^[a-zA-Z]+(?:[\s.][a-zA-Z]+)*$)", ErrorMessage = "Please enter the city name only")]
        [DisplayName("Service City")]
        public string ServiceCity { get; set; }

        [Required(ErrorMessage = "Please enter a city")]
        [DisplayName("Billing City")]
        [RegularExpression(@"(^([\s\.]?[a-zA-Z]+)+$)", ErrorMessage = "Please enter the city name only")]
        //[RegularExpression(@"(^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$)", ErrorMessage = "Please enter the city name only")]
        public string BillingCity { get; set; }

      
        [DisplayName("County")]
        [RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "Please enter county")]
        [Required]
        public IQueryable<SelectListItem> Counties { get; set; }

        //[DataType(DataType.PostalCode, ErrorMessage = "A")]
        [DisplayName("Service County")]
        [RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "Please enter county")]
        [Required]
        public int SerCountyId { get; set; }

        
        [DisplayName("Billing County")]
        [RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "Please enter county")]
        [Required]
        public int BilCountyId { get; set; }

        [DisplayName("Service Zip Code")]
        [Required(ErrorMessage ="Please enter a zip code")]
        [RegularExpression(@"(^(?!0{5})(\d{5})(?!-?0{4})(|-\d{4})?$)", ErrorMessage = "Please enter valid ZIP or ZIP+4 code")]
        public string SerZipCode { get; set; }

        [DisplayName("Billing Zip Code")]
        [Required(ErrorMessage = "Please enter a zip code")]
        [RegularExpression(@"(^(?!0{5})(\d{5})(?!-?0{4})(|-\d{4})?$)", ErrorMessage = "Please enter valid ZIP or ZIP+4 code")]
        public string BilZipCode { get; set; }

        [DisplayName("Contract Status")]
        public int? LeadStausId { get; set; }

        public string LeadStatus { get; set; }
        [Required(ErrorMessage = "The email address is required")]
        [RegularExpression(@"^([\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+\.)*[\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+@((((([a-zA-Z0-9]{1}[a-zA-Z0-9\-]{0,62}[a-zA-Z0-9]{1})|[a-zA-Z])\.)+[a-zA-Z]{2,6})|(\d{1,3}\.){3}\d{1,3}(\:\d{1,5})?)$",ErrorMessage ="Please enter a valid Email Address")]
        public string Email { get; set; }

        public IQueryable<SelectListItem> Subscriptions { get; set; }

        public string Subscription { get; set; }

        [DisplayName("Utility Account Number")]
        [MaxLength(15,ErrorMessage = "Please enter valid utility account number")]
        [RegularExpression(@"^[\d -]+$", ErrorMessage = "Please enter valid utility account number")]
        public string UtilityAccountNumber { get; set; }

        [DisplayName("Utility Provider")]
        public string UtilityProvider { get; set; }

        [DisplayName("Sales Channel")]
        public IQueryable<SelectListItem> Channels { get; set; }

        [DisplayName("Sales Channel")]
        public int? ChannelId { get; set; }

        [DisplayName("Campaign")]
        public IQueryable<SelectListItem> Campaigns { get; set; }

        [DisplayName("Campaign")]
        public int CampaignId { get; set; }

        [DisplayName("Parent Campaign")]
       
        public string ParentsId { get; set; }

        [DisplayName("Sub Campaign")]
       
        public string SubParentsId { get; set; }

        [DisplayName("Referred By")]
        [RegularExpression(@"([a-zA-Z]{1}[a-zA-Z]*[\s]{0,1}[a-zA-Z])+([\s]{0,1}[a-zA-Z]+)", ErrorMessage = "Enter proper names only")]
        public string ReferedBy { get; set; }

        [DisplayName("Creative")]
      
        public string CreativeId { get; set; }

        [DisplayName("Incentive")]
        public string IncentiveId { get; set; }

        [DisplayName("Lead Source")]
        public IQueryable<SelectListItem> LeadSources { get; set; }

        [DisplayName("Lead Source")]
        public int LeadSourceId { get; set; }

        public int? SalesRepId { get; set; }

        [DisplayName("Date Of Birth")]
        public DateTime? DateOfBirth { get; set; }

        [DisplayName("Lead Ranking")]
        public IQueryable<SelectListItem> LeadRankings { get; set; } 

        public int? LeadRankingId { get; set; }

        public int LeadId { get; set; }

        public bool Manager { get; set; }
        public bool IsEdit { get; set; }

        [DisplayName("Comments")]
        [Required(ErrorMessage = "Comments is required")]
        public string CallComment { get; set; }

        public bool? IsDuplicateCall { get; set; }

        public int? CallStatusId { get; set; }

        [DisplayName("Call Status")]
        public IQueryable<SelectListItem> CallStatus { get; set; }

        [DisplayName("Appointment Date")]
        [AllowFutureDate]
        public DateTime? CallDate { get; set; }
        public DateTime? Time { get; set; }
        public int CallLogId { get; set; }
        public LeadHistory LeadHistories { get; set; }
        public ICollection<CallHistory> CallHistories { get; set; }
        public ICollection<AssignHistory> AssignHistories { get; set; }
        public bool AllowDuplicateContract { get; set; }
        public bool HasPendingRequest { get; set; }
        [Required(ErrorMessage = "Please enter reason")]
        [RegularExpression(@"(^[a-z]+$)+", ErrorMessage = "Please enter valid reason")]
        public string Reason { get; set; }
        public bool IsActive { get; set; }
        public IQueryable<SelectListItem> BoundStatuss { get; set; }
        public string BoundStatus { get; set; }
        public int DuplicateRequestId { get; set; }
        [DisplayName("User name")]
        public string adminuser { get; set; }
        [DisplayName("Password")]
        public string adminpassword { get; set; }
    }

    public class BaseHistory
    {
        public string Title { get; set; }

        public string Boundstatus { get; set; }
        public string Date { get; set; }
        public string ProcessDate { get; set; }

        public string Comments { get; set; }

        public string DoneBy { get; set; }
    }

    public class LeadHistory 
    {
        public string LeadCreatedBy { get; set; }
        public string LeadUpdatedBy { get; set; } 
        public  string InformationSentBy { get; set; }
        public string ContractSentBy { get; set; }
        public string ContractSignedBy { get; set; }
        public string ContractDeclinedBy { get; set; }

        public DateTime? LeadCreatedOn { get; set; }
        public DateTime? LeadUpdatedOn { get; set; }
        public DateTime? InformationSentOn { get; set; }
        public DateTime? ContractSentOn { get; set; }
        public DateTime? ContractDeclinedOn { get; set; }
        public DateTime? ContractSignedOn { get; set; }

        public string reason { get; set; }

    }
    public class CallHistory : BaseHistory
    {
        
    }
    public class AssignHistory:BaseHistory
    { }
}
