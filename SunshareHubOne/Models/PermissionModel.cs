﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SunshareHubOne.Models
{
    public class PermissionModel
    {
        public bool hasHubOneAccess { get; set; }
        public bool hasBillingAccess { get; set; }
    }
}