﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace SunshareHubOne
{
    public class SunsharePrincipal : GenericPrincipal, ISunsharePrincipal
    {
        public SunsharePrincipal(IIdentity identity, string[] roles) : base(identity, roles)
        {

        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}