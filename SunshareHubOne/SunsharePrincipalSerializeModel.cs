﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SunshareHubOne
{
    public class SunsharePrincipalSerializeModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<string> Roles { get; set; }
    }
}