﻿using System.ServiceModel;
using System.ServiceModel.Web;
using System.Web.Http.Cors;
using SunshareCommon;

namespace SunshareServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface ISunshareServices
    {
        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/ValidateEncryptLogin",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        UserDetails ValidateEncryptLogin(string emailId, string password);

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/ChangeEncryptPassword",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        PasswordReturn ChangeEncryptPassword(string emailId, string oldPassword, string newPassword);

        [OperationContract]
        [WebInvoke(Method = "POST",
            UriTemplate = "/ResetEncryptPassword",
            RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped)]
        PasswordReturn ResetEncryptPassword(string emailId, string newPassword);
    }    
}
