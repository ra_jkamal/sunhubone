﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Hosting;
using System.Web.Security;
using SunshareDatabase;
using System.IO;
using System.Web;
using SunshareCommon;
using System.ServiceModel.Activation;
using System.Web.Http.Cors;
using System.Data.Entity;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;

namespace SunshareServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class SunshareServices : ISunshareServices
    {
        private readonly SunshareDatabaseContext _context = new SunshareDatabaseContext();

        private readonly UserDetails _userDetail = new UserDetails
        {
            ReturnCode = 401,
            ReturnMsg = "No data",
            PartnerLogoPic = string.Empty,
            ProfilePic = string.Empty
        };

        private readonly PasswordReturn _passwordReturn = new PasswordReturn {ReturnCode = 401, ReturnMsg = "No Data"};
        readonly Methods _methods = new Methods();

        public PasswordReturn ResetEncryptPassword(string emailId, string newPassword)
        {
            PasswordHasher hash = new PasswordHasher();
            if (!string.IsNullOrEmpty(emailId) && !string.IsNullOrEmpty(newPassword))
            {
                if (!_context.Users.Any()) return _passwordReturn;
                if (_context.Users.Any(u => u.Email == emailId & u.IsActive))
                {
                    var user = _context.Users.FirstOrDefault(u => u.Email == emailId & u.IsActive);
                    if (user != null)
                    {
                        //if (hash.VerifyHashedPassword(user.PasswordHash, newPassword) != PasswordVerificationResult.Success)
                        //{
                        //    _passwordReturn.ReturnMsg = Messages.ValidPassword;
                        //    return _passwordReturn;
                        //}
                        user.PasswordHash = hash.HashPassword(newPassword);
                        _context.SaveChanges();
                        _passwordReturn.ReturnCode = 200;
                        _passwordReturn.ReturnMsg = Messages.PasswordChangedSuccess;
                        return _passwordReturn;
                    }
                }
            }
            _passwordReturn.ReturnMsg = Messages.ValidEmailId;
            return _passwordReturn;
        }

        public PasswordReturn ChangeEncryptPassword(string emailId, string oldPassword, string newPassword)
        {
            var _passwordReturn = new PasswordReturn {ReturnCode = 401, ReturnMsg = "No Data"};
            PasswordHasher hash = new PasswordHasher();
            if (!string.IsNullOrEmpty(emailId) && !string.IsNullOrEmpty(oldPassword) &&
                !string.IsNullOrEmpty(newPassword))
            {

                if (!_context.Users.Any()) return _passwordReturn;
                if (_context.Users.Any(u => u.Email == emailId & u.IsActive))
                {
                    var user = _context.Users.FirstOrDefault(u => u.Email == emailId & u.IsActive);
                    if (user != null)
                    {
                        if (hash.VerifyHashedPassword(user.PasswordHash, oldPassword) !=
                            PasswordVerificationResult.Success)
                        {
                            _passwordReturn.ReturnMsg = Messages.ValidPassword;
                            return _passwordReturn;
                        }
                        user.PasswordHash = hash.HashPassword(newPassword);
                        _context.SaveChanges();
                        _passwordReturn.ReturnCode = 200;
                        _passwordReturn.ReturnMsg = Messages.PasswordChangedSuccess;
                        return _passwordReturn;
                    }
                }
            }
            _passwordReturn.ReturnMsg = Messages.ValidEmailId;
            return _passwordReturn;
        }

        public UserDetails ValidateEncryptLogin(string emailId, string password)
        {
            PasswordHasher hash = new PasswordHasher();
            var userDetail = new UserDetails {ReturnMsg = "No Users found", ReturnCode = 400};
            if (!string.IsNullOrEmpty(emailId) && !string.IsNullOrEmpty(password))
            {
                if (!_context.Users.Any()) return userDetail;
                if (!_context.Users.Any(u => u.Email == emailId & u.IsActive))
                {
                    _userDetail.ReturnMsg = Messages.ValidEmailId;
                    return _userDetail;
                }
                var user = _context.Users.FirstOrDefault(u => u.Email == emailId);
                if (user != null)
                {
                    if (hash.VerifyHashedPassword(user.PasswordHash, password) != PasswordVerificationResult.Success)
                    {
                        _userDetail.ReturnMsg = Messages.ValidPassword;
                        return _userDetail;
                    }
                    return new UserDetails
                    {
                        Name = $"{user.FirstName} {user.LastName}",
                        ProfilePic = string.IsNullOrEmpty(user.ImageUrl) ? string.Empty : user.ImageUrl,
                        //PartnerLogoPic = VirtualPathUtility.ToAbsolute($"~/FileServer/{user.Partner.PartnerLogoUrl}"),
                        ReturnCode = 200,
                        ReturnMsg = Messages.ValidationSuccess,
                        SaleRepId = user.Id
                    };
                }
            }
            _userDetail.ReturnMsg = Messages.ValidEmailandPassword;
            return _userDetail;
        }





        private void UpdateLog(int salesRepId, float lat, float longi, string cusAddress, string cusCity, string cusZip,
            string cusFirstName, string cusLastName, string cusMobile, int visitStatusId, int rank, string notes,
            string cusEmailId, int logId, string cusState, BaseReturnWithLogId logreturn, float dlat, float dlongi,
            bool isSendInfo, string cusSubsription = null, string cusAppointmentTime = null,
            string cusAppointmentDate = null, string cusDob = null, string cusUtilityAccountNo = null,
            string cusCounty = null, bool isContract = false)
        {
            var rep = _context.Users.FirstOrDefault(u => u.IsActive && u.Id == salesRepId);
            if (rep == null) return;
            var mapping = rep.AddressUserMappings.FirstOrDefault(a => a.AddressUserMappingId == logId & a.IsActive);
            var userAddress = mapping?.Address;
                // _context.Address.FirstOrDefault(a => a.AddressId == mapping.AddressId & a.IsActive);
            if (userAddress == null) return;
            bool isModified = !rep.Leads.Any(
                s => (s.FirstName == cusFirstName && s.LastName == cusLastName && s.PrimaryEmail == cusEmailId));
            var userLead = rep.Leads.FirstOrDefault(
                s =>
                    (s.ServiceAddressId == userAddress.AddressId || s.BillingAddressId == userAddress.AddressId) &
                    s.IsActive);
            if (userLead == null) return;
            if (!isModified & !isContract)
                isModified =
                    !userLead.LeadRep.AddressUserMappings.Any(
                        a =>
                            a.IsActive & a.AddressId == userLead.ServiceAddressId &
                            (!a.AppointmentDate.HasValue ||
                             (a.AppointmentDate.Value.CompareTo(Convert.ToDateTime(cusAppointmentDate)) == 0)) &
                            a.AppointmentTime.CompareTo(string.IsNullOrEmpty(cusAppointmentTime)
                                ? TimeSpan.Zero
                                : TimeSpan.Parse(cusAppointmentTime)) == 0);
            userLead.FirstName = cusFirstName;
            userLead.LastName = cusLastName;
            userLead.MobileNo = cusMobile;
            userLead.AspNetUsersId = salesRepId;
            userLead.PrimaryEmail = cusEmailId;
            userLead.CampaignId = 2;
            userLead.IsActive = true;
            userLead.LeadSourceId = 1;
            if (rank != 0)
                userLead.LeadRankingId = rank;
            if (!string.IsNullOrEmpty(cusDob))
                userLead.DateOfBirth = cusDob;
            if (!string.IsNullOrEmpty(cusUtilityAccountNo))
                userLead.UtilityAccountNo = cusUtilityAccountNo;
            if (!string.IsNullOrEmpty(cusSubsription))
                userLead.SubscriptionPercentage = Convert.ToInt32(cusSubsription);
            var address = userLead.LeadServiceAddress;

            address.AddressInformation = cusAddress;
            address.City = cusCity;
            address.Zipcode = cusZip;
            address.State = cusState ?? string.Empty;
            address.Longitude = longi;
            address.Lattitude = lat;
            address.County = cusCounty ?? string.Empty;
            address.IsActive = true;
            address.DeviceLong = dlongi;
            address.DeviceLat = dlat;

            //var currentLeadStatus = userLead.LeadLeadStatusMappings.FirstOrDefault(l => l.IsCurrent);
            //if (currentLeadStatus != null)
            //{
            //    currentLeadStatus.IsCurrent = false;
            //}
            //var leadStatusMapping= userLead.LeadLeadStatusMappings.FirstOrDefault(l=>l.IsCurrent);
            //if (leadStatusMapping != null)
            //{
            //    leadStatusMapping.LeadStatusId = LeadContractStatus.LeadCreated;
            //    leadStatusMapping.LeadId = userLead.LeadId;
            //    leadStatusMapping.IsCurrent = true;
            //    leadStatusMapping.UpdatedDate = DateTime.Now;
            //}

            //_context.SaveChanges();
            var addressUserMapping = userLead.LeadRep.AddressUserMappings.FirstOrDefault(a => a.IsActive);
            if (addressUserMapping != null)
            {
                addressUserMapping.AddressId = userLead.ServiceAddressId;
                addressUserMapping.VisitStatusId = visitStatusId;
                addressUserMapping.AppointmentDate =
                    !string.IsNullOrEmpty(cusAppointmentDate)
                        ? Convert.ToDateTime(cusAppointmentDate)
                        : (DateTime?) null;
                addressUserMapping.AppointmentTime =
                    !string.IsNullOrEmpty(cusAppointmentTime) ? TimeSpan.Parse(cusAppointmentTime) : TimeSpan.Zero;
                addressUserMapping.DateVisited = DateTime.Today;
                addressUserMapping.IsActive = true;
                addressUserMapping.UserId = salesRepId;
                addressUserMapping.Comments = notes;
            }
            _context.SaveChanges();
            if (visitStatusId == LeadVisitStatus.Appointment & !isSendInfo & isModified & !isContract)
            {
                var filePath = HostingEnvironment.MapPath("~/FileServer/Upload/SendAppoinmentTemplate.html");
                if (filePath == null) return;
                var mailTemplate =
                    File.ReadAllText(filePath)
                        .Replace("{0}", userLead.FirstName).Replace("{3}", $"{rep.FirstName} {rep.LastName}")
                        .Replace("{4}", rep.Email).Replace("{5}", rep.PhoneNumber.ToString())
                        .Replace("{1}", userLead.LastName)
                        .Replace("{2}",
                            $"{Convert.ToDateTime(cusAppointmentDate).Add(TimeSpan.Parse(cusAppointmentTime)):MMM dd, yyyy}")
                        .Replace("{7}", $"{rep.FirstName}")
                        .Replace("{6}",
                            $"{Convert.ToDateTime(cusAppointmentDate).Add(TimeSpan.Parse(cusAppointmentTime)):HH:mm tt}"
                                .Replace("AM", "").Replace("PM", ""));
                Methods.SendEmail(userLead.PrimaryEmail, mailTemplate, Messages.EmailAppointmentSubject, true);
            }
            //TODO: check firstname, Lastname, email,appointment date & time record available? if record available skip appointment, else send appointment again. All these before updating.
        }


        private
            bool SaveLog(int salesRepId, float lat, float longi, string cusAddress, string cusCity, string cusZip,
                string cusFirstName, string cusLastName, string cusMobile,
                int visitStatusId, int rank, string notes, string cusEmailId, string cusState,
                BaseReturnWithLogId logreturn, out BaseReturnWithLogId logVisits, float dlat, float dlongi,
                int addressSourceId, bool isSendInfo, string cusSubsription = null, string cusAppointmentDate = null,
                string cusAppointmentTime = null, string cusDob = null, string cusUtilityAccountNo = null,
                string cusCounty = null)
        {
            var rep = _context.Users.FirstOrDefault(u => u.IsActive && u.Id == salesRepId);
            if (rep != null)
            {
                var lead = _context.Leads.Add(new Lead
                {
                    FirstName = cusFirstName,
                    LastName = cusLastName,
                    MobileNo = cusMobile,
                    AspNetUsersId = salesRepId,
                    PrimaryEmail = cusEmailId,
                    CampaignId = 2,
                    //CreatedDate = DateTime.Today,
                    //LeadStatusId = LeadContractStatus.LeadCreated,
                    IsActive = true,
                    LeadSourceId = 1
                });
                if (rank != 0)
                    lead.LeadRankingId = rank;
                if (!string.IsNullOrEmpty(cusDob))
                    lead.DateOfBirth = cusDob;
                if (!string.IsNullOrEmpty(cusUtilityAccountNo))
                    lead.UtilityAccountNo = cusUtilityAccountNo;
                if (!string.IsNullOrEmpty(cusSubsription))
                    lead.SubscriptionPercentage = Convert.ToInt32(cusSubsription);
                lead.LeadServiceAddress = new Address
                {
                    AddressInformation = cusAddress,
                    City = cusCity,
                    Zipcode = cusZip,
                    State = cusState ?? string.Empty,
                    Longitude = longi,
                    Lattitude = lat,
                    AddressSourceId = addressSourceId, //LeadAddressSources.D2D,
                    County = cusCounty ?? string.Empty,
                    IsActive = true,
                    DeviceLong = dlongi,
                    DeviceLat = dlat
                };
                var currentLeadStatus = lead.LeadLeadStatusMappings.FirstOrDefault(l => l.IsCurrent);
                if (currentLeadStatus != null)
                {
                    currentLeadStatus.IsCurrent = false;
                }
                lead.LeadLeadStatusMappings.Add(new LeadLeadStatusMapping()
                {
                    LeadStatusId = LeadContractStatus.LeadCreated,
                    LeadId = lead.LeadId,
                    IsCurrent = true,
                    UpdatedDate = DateTime.Now
                });


                _context.SaveChanges();
                lead.LeadRep.AddressUserMappings.Add(new AddressUserMapping()
                {
                    AddressId = lead.ServiceAddressId,
                    VisitStatusId = visitStatusId,
                    AppointmentDate =
                        !string.IsNullOrEmpty(cusAppointmentDate)
                            ? Convert.ToDateTime(cusAppointmentDate)
                            : (DateTime?) null,
                    AppointmentTime =
                        !string.IsNullOrEmpty(cusAppointmentTime) ? TimeSpan.Parse(cusAppointmentTime) : TimeSpan.Zero,
                    DateVisited = DateTime.Today,
                    IsActive = true,
                    UserId = salesRepId,
                    Comments = notes
                });
                _context.SaveChanges();
                if (visitStatusId == LeadVisitStatus.Appointment & !isSendInfo)
                {
                    var filePath = HostingEnvironment.MapPath("~/FileServer/Upload/SendAppoinmentTemplate.html");
                    if (filePath != null)
                    {
                        var mailTemplate =
                            File.ReadAllText(filePath)
                                .Replace("{0}", lead.FirstName).Replace("{3}", $"{rep.FirstName} {rep.LastName}")
                                .Replace("{4}", rep.Email).Replace("{5}", rep.PhoneNumber.ToString())
                                .Replace("{1}", lead.LastName)
                                .Replace("{2}",
                                    $"{Convert.ToDateTime(cusAppointmentDate).Add(TimeSpan.Parse(cusAppointmentTime)):MMM dd, yyyy}")
                                .Replace("{7}", $"{rep.FirstName}")
                                .Replace("{6}",
                                    $"{Convert.ToDateTime(cusAppointmentDate).Add(TimeSpan.Parse(cusAppointmentTime)):HH:mm tt}"
                                        .Replace("AM", "").Replace("PM", ""));
                        Methods.SendEmail(lead.PrimaryEmail, mailTemplate, Messages.EmailAppointmentSubject, true);
                    }
                }

                var addressUserMapping =
                    rep.AddressUserMappings.FirstOrDefault(m => m.AddressId == lead.ServiceAddressId);
                if (addressUserMapping != null)
                {
                    logreturn.LogId =
                        Convert.ToString(addressUserMapping.AddressUserMappingId);
                    logreturn.ReturnMsg = Messages.Success;
                    logreturn.ReturnCode = 200;
                    logVisits = logreturn;
                    return true;
                }
            }
            logreturn.ReturnMsg = Messages.FailureNoRepFound;
            logreturn.LogId = "0";
            logreturn.ReturnCode = 400;
            logVisits = logreturn;
            return false;
        }
    }
}

