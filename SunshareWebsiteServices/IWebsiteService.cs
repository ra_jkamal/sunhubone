﻿using SunshareCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace SunshareWebsiteServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IWebsiteService
    {
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/EnrollLead", RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]

        ContractReturn EnrollLead(string cusFirstName, string cusLastName, string cusEmailId, string cusMobile,
            string cusAddress, string cusAptNo, string cusCity, string cusState,
            string cusZip, string cusCounty, string cusSubsription, string dateOfBirth, string cusUtilityAccountNo,
            bool isAutoEnrollment, string landingPageName);
        
    }
}
