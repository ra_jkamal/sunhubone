﻿using System;
using System.Linq;
using System.ServiceModel.Activation;
using SunshareCommon;
using SunshareDatabase;

namespace SunshareWebsiteServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class WebsiteService : IWebsiteService
    {
        private readonly SunshareDatabaseContext _context = new SunshareDatabaseContext();
        private readonly Methods _methods = new Methods();
        private bool SaveWebLog(bool isAutoEnrollment, int salesRepId, float lat, float longi, string cusAddress, string cusCity, string cusZip, string cusFirstName, string cusLastName, string cusMobile, string notes, string cusEmailId, string cusState, BaseReturnWithLogId logreturn, out BaseReturnWithLogId logVisits, float dlat, float dlongi,bool isDuplicate, string cusSubsription = null, string cusAppointmentDate = null, string cusAppointmentTime = null, string cusDob = null, string cusUtilityAccountNo = null, string cusCounty = null)
        {
            var lead = SetLead(isAutoEnrollment, salesRepId, cusFirstName, cusLastName, cusMobile, cusEmailId, cusSubsription, cusDob, cusUtilityAccountNo,isDuplicate);
            SetLeadAddress(lat, longi, cusAddress, cusCity, cusZip, cusState, dlat, dlongi, lead, cusCounty);
            SetLeadStatus(lead);
            _context.SaveChanges();
            MakeAddressChange(salesRepId, notes, lead, cusAppointmentDate, cusAppointmentTime);
            var addressUserMapping = _context.AddressUserMappings.FirstOrDefault(m => m.AddressId == lead.ServiceAddressId);
            if (addressUserMapping != null)
            {
                logreturn.LogId =
                    Convert.ToString(addressUserMapping.AddressUserMappingId);
                logreturn.ReturnMsg = Messages.Success;
                logreturn.ReturnCode = 200;
                logVisits = logreturn;
                return true;
            }
            logreturn.ReturnMsg = Messages.FailureNoRepFound;
            logreturn.LogId = "0";
            logreturn.ReturnCode = 400;
            logVisits = logreturn;
            return false;
        }

        private static void SetLeadStatus(Lead lead)
        {
            var currentLeadStatus = lead.LeadLeadStatusMappings.FirstOrDefault(l => l.IsCurrent);
            if (currentLeadStatus != null)
            {
                currentLeadStatus.IsCurrent = false;
            }
            lead.LeadLeadStatusMappings.Add(new LeadLeadStatusMapping
            {
                LeadStatusId = LeadContractStatus.LeadCreated,
                LeadId = lead.LeadId,
                IsCurrent = true,
                UpdatedDate = DateTime.Now
            });
        }

        private static void SetLeadAddress(float lat, float longi, string cusAddress, string cusCity, string cusZip,
            string cusState, float dlat, float dlongi, Lead lead, string cusCounty)
        {
            lead.LeadServiceAddress = new Address
            {
                AddressInformation = cusAddress,
                City = cusCity,
                Zipcode = cusZip,
                State = cusState ?? string.Empty,
                Longitude = longi,
                Lattitude = lat,
                AddressSourceId = LeadAddressSources.Website,
                County = cusCounty ?? string.Empty,
                IsActive = true,
                DeviceLong = dlongi,
                DeviceLat = dlat
            };
        }

        private Lead SetLead(bool isAutoEnrollment, int salesRepId, string cusFirstName, string cusLastName, string cusMobile,
            string cusEmailId, string cusSubsription, string cusDob, string cusUtilityAccountNo,bool isDuplicate)
        {
            return _context.Leads.Add(new Lead
            {
                FirstName = cusFirstName,
                LastName = cusLastName,
                HomePhoneNo = cusMobile,
                AspNetUsersId = (salesRepId == 0) ? (int?) null : salesRepId,
                PrimaryEmail = cusEmailId,
                CampaignId = 2, //TODO: where this will come from?
                ChannelId = (isAutoEnrollment) ? LeadChannels.WebEnrollmentAuto : LeadChannels.WebLanding,
                IsActive = true,
                LeadSourceId = 1, //TODO: where this will come from?
                LeadRankingId = LeadRankings.Standard,
                DateOfBirth = (cusDob== "0/0/0" || cusDob== null) ? null : cusDob,
                UtilityAccountNo = !string.IsNullOrEmpty(cusUtilityAccountNo) ? cusUtilityAccountNo : string.Empty,
                SubscriptionPercentage = !string.IsNullOrEmpty(cusSubsription) ? Convert.ToInt32(cusSubsription) : 0,
                IsDuplicateLead = isDuplicate
            });
        }

        private void MakeAddressChange(int salesRepId, string notes, Lead lead, string cusAppointmentDate,
            string cusAppointmentTime)
        {
            _context.AddressUserMappings.Add(new AddressUserMapping
            {
                AddressId = lead.ServiceAddressId,
                VisitStatusId = LeadVisitStatus.CustomerInterested,
                AppointmentDate =
                    !string.IsNullOrEmpty(cusAppointmentDate)
                        ? Convert.ToDateTime(cusAppointmentDate)
                        : (DateTime?) null,
                AppointmentTime =
                    !string.IsNullOrEmpty(cusAppointmentTime) ? TimeSpan.Parse(cusAppointmentTime) : TimeSpan.Zero,
                DateVisited = DateTime.Today,
                IsActive = true,
                UserId = salesRepId == 0 ? (int?) null : salesRepId,
                Comments = notes
            });
            _context.SaveChanges();
        }

        private bool IsEmailExists(string emailId)
        {
            if (string.IsNullOrEmpty(emailId)) return false;
            return !_context.Leads.Any() || _context.Leads.Any(u => (u.PrimaryEmail == emailId || u.SecondaryEmail ==emailId) & u.IsActive);
        }

        private bool IsPhoneExists(string mobile)
        {
            if (string.IsNullOrEmpty(mobile)) return false;
            return !_context.Leads.Any() || _context.Leads.Any(u => (u.WorkPhoneNo == mobile || u.HomePhoneNo == mobile) & u.IsActive);
        }
        public ContractReturn EnrollLead(string cusFirstName, string cusLastName, string cusEmailId, string cusMobile, string cusAddress,
            string cusAptNo, string cusCity, string cusState, string cusZip, string cusCounty, string cusSubsription,
            string dateOfBirth, string cusUtilityAccountNo, bool isAutoEnrollment, string landingPageName)
        {
            ContractReturn returnValue=null;
            //TODO: only email duplication been checked
            if (IsEmailExists(cusEmailId))
                returnValue= new ContractReturn {ReturnCode = 401, LogId = string.Empty, ReturnMsg = "Lead Already Exists"};
            if (returnValue == null)
            {
                if (IsPhoneExists(cusMobile))
                {
                    returnValue = new ContractReturn { ReturnCode = 401, LogId = string.Empty, ReturnMsg = "Lead Already Exists" }; 
                }
            }
            var source = _context.LandingPages.FirstOrDefault(l => l.LandingPageName == landingPageName & l.IsActive) ??
                         _context.LandingPages.Add(new LandingPage() { IsActive = true, LandingPageName = landingPageName });
            BaseReturnWithLogId logVisits;
            var contractReturn = new ContractReturn { ReturnMsg = "Error saving data", ReturnCode = 401, LogId = string.Empty };
            contractReturn = SaveWebLog(isAutoEnrollment, 0, 0, 0, cusAddress, cusCity, cusZip, cusFirstName, cusLastName, cusMobile, "Website/LandingPage", cusEmailId, cusState,
                contractReturn, out logVisits, 0, 0,(returnValue !=null), cusSubsription, null, null, dateOfBirth, cusUtilityAccountNo, cusCounty)
                ? new ContractReturn { ReturnMsg = logVisits.ReturnMsg, ReturnCode = logVisits.ReturnCode, LogId = logVisits.LogId } : new ContractReturn { ReturnMsg = contractReturn.ReturnMsg, ReturnCode = contractReturn.ReturnCode, LogId = contractReturn.LogId };
            var logId = string.IsNullOrEmpty(contractReturn.LogId) ? 0 : Convert.ToInt32(contractReturn.LogId);
            if (logId == 0) return new ContractReturn { ReturnMsg = "Error Saving data/ No data Found", ReturnCode = 401, LogId = "0" };
            var rep = _context.Users.FirstOrDefault(u => u.IsActive);
            var mapping = _context.AddressUserMappings.FirstOrDefault(a => a.AddressUserMappingId == logId & a.IsActive);
            if (_context.Leads.Any(l => l.IsActive & l.PrimaryEmail == cusEmailId & l.TrancientId != null))
                return returnValue ?? new ContractReturn { ReturnMsg = "Contract already sent / signed", ReturnCode = 401 };
            var userLead = _context.Leads.FirstOrDefault(
                s => s.ServiceAddressId == mapping.AddressId & s.IsActive & s.PrimaryEmail == cusEmailId & s.TrancientId == null);
            var userAddress = _context.Address.FirstOrDefault(a => a.AddressId == userLead.BillingAddressId);
            if (userLead == null) return returnValue ?? new ContractReturn { ReturnMsg = "Something went wrong", ReturnCode = 401 };
            if (!isAutoEnrollment) return returnValue ?? new ContractReturn { ReturnMsg = "Lead Saved Successfully", ReturnCode = 200 };
            if (rep == null) return returnValue ?? contractReturn;
            //userLead.AspNetUsersId = rep.Id;
            userLead.LandingPageId = source.LandingPageId;
            _context.SaveChanges();
            return _methods.ContractReturn(userLead, userAddress, rep, mapping);
        }
    }


}
